<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#113681</headerColor>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>Gestione preventivi del portale TiAnticipo</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>IFIS - TiAnticipo</label>
    <navType>Standard</navType>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Api-Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Commercial Lending</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Direzione Commerciale</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Gestione debitori PA</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - International</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Leasing</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Leasing Superuser</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Sviluppo Indiretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Nucleo Contratti</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - TiAnticipo</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Api-Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Commercial Lending</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Direzione Commerciale</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Filo Diretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Finanza Strutturata</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Gestione debitori PA</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Read-Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - International</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Leasing</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Leasing Superuser</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Nucleo Contratti</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Read-Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Segreteria Fidi</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Sviluppo Diretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Sviluppo Indiretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppCommercialLending</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaCommercialLending</recordType>
        <type>Flexipage</type>
        <profile>IFIS - TiAnticipo</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Segreteria Fidi</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Sviluppo Diretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Analytics Cloud Integration User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Analytics Cloud Security User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Chatter External User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Chatter Free User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Chatter Moderator User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Customer Community Login User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Api-Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Guest License User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>SolutionManager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>SalesforceIQ Integration User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>ReadOnly</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>MarketingProfile</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Commercial Lending</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Direzione Commerciale</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Filo Diretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Finanza Strutturata</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Gestione debitori PA</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - International</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Leasing</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Leasing Superuser</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Nucleo Contratti</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Poland</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Read-Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Segreteria Fidi</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Sviluppo Diretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Sviluppo Indiretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>IFIS - TiAnticipo</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Insights Integration User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Partner Community User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Profilo Portal clienti leasing</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>Profilo tianticipo</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>OppPoland</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaPolonia</recordType>
        <type>Flexipage</type>
        <profile>ContractManager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Filo Diretto</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>IFIS - Finanza Strutturata</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Opp_Finanza_Strutturata</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.IFISOpportunitaFinanzaStrutturata</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <tabs>standard-home</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>Cessione__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>AccountRelazioneBusiness__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>IFIS_TiAnticipo_UtilityBar</utilityBar>
</CustomApplication>

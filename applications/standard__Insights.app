<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Account</tabs>
    <tabs>standard-CollaborationGroup</tabs>
    <tabs>standard-UserProfile</tabs>
    <tabs>standard-Product2</tabs>
    <tabs>standard-Macro</tabs>
    <tabs>standard-WaveHome</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-Document</tabs>
    <tabs>standard-Case</tabs>
    <tabs>Ateco__c</tabs>
    <tabs>AssegnazioneAnagrafica__c</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Console</tabs>
    <tabs>standard-ProfileSkill</tabs>
    <tabs>Log__c</tabs>
    <tabs>standard-DuplicateRecordSet</tabs>
    <tabs>FiloDiretto</tabs>
    <tabs>Comune__c</tabs>
    <tabs>standard-ProfilePlatformFeed</tabs>
    <tabs>standard-AppLauncher</tabs>
    <tabs>standard-OtherUserProfile</tabs>
    <tabs>standard-Solution</tabs>
    <tabs>standard-Order</tabs>
    <tabs>standard-StreamingChannel</tabs>
    <tabs>standard-ProfilePlatformOverview</tabs>
    <tabs>standard-WorkBadge</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-Quote</tabs>
    <tabs>standard-ContentSearch</tabs>
    <tabs>MyApprovals</tabs>
    <tabs>standard-Campaign</tabs>
    <tabs>standard-Idea</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Contract</tabs>
    <tabs>standard-File</tabs>
    <tabs>standard-Recognition</tabs>
    <tabs>standard-Forecasting3</tabs>
    <tabs>standard-Workspace</tabs>
    <tabs>AccountRelazioneBusiness__c</tabs>
</CustomApplication>

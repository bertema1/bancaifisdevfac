<apex:page renderAs="pdf"  showHeader="false" applyHtmlTag="false" applyBodyTag="false" standardStylesheets="false" standardController="Opportunity" extensions="ExtPdfAdeguataVerificaClientela">
<html>

<head>
  <apex:stylesheet value="{!$Resource.CssPdfGeneration}"/>
<style>

p {
  margin:0px;
}

p, li, div, span {
  font-family: Arial, Arial Unicode MS;
}

p.MsoNormal, li.MsoNormal, div.MsoNormal {
  margin:0cm;
  margin-bottom:.0001pt;
}

table.CheckListTable {
  margin-left: :5px;
  text-align:justify;
  margin: 0px;
  width: 100%;
}

.checkrow {
  float: right;
  margin-right: 100px;
}

.ElementList1 {
  margin:0cm;
  margin-bottom:.0001pt;
  font-size:7.5pt;
  text-align:justify;
}

.checkboxImgNoMargin{
  width:12px;
  height:12px;
}

.lineInput {
  border-bottom: solid;
  border-bottom-width: 1pt;
  margin-left: 4pt;
  margin-right: 4pt;
  padding-left: 5pt;
  padding-right: 5pt;
}

.normalLine {
  padding-left:80pt;
  border-bottom: solid;
  border-bottom-width: 1pt;
  margin-left: 4pt;
  margin-right: 4pt;
}


p.MsoFootnoteText, li.MsoFootnoteText, div.MsoFootnoteText {
  margin:0cm;
  margin-bottom:.0001pt;
  font-size:9.0pt;
}

body {
  font-size:7.5pt;
  text-align:justify;
  font-family: Arial, Arial Unicode MS;
}

#tabellaConsensi img {
  width: 12px;
  height: 12px;
}
ol {
  margin-bottom:0cm;
}
ul {
  margin-bottom:0cm;
}

</style>

</head>

<body>

<!--footer and header-->
<div class="header_left">
  <img class="header_logo_img" src="{!$Resource.BancaIfisLogo}"/>
</div>

<div class="footer_center">
  <p class="docName">Informaţii Confidenţialitate – Banca IFIS – Mai 2018</p>
  <img class="footerLargeImg" src="{!$Resource.FooterIfisPrivacy}"/>
</div>

<div class="footer_right pageNumberFooter">
  <span class="pagenumber"/>
</div>

<p class="MsoFootnoteText" align="center" style="margin-right:-14.3pt;text-align:
center"><b><span  style="color:black">INFORMAŢII CONFIDENŢIALITATE</span></b></p>

<!-- 1. Premessa-->
<div class="uniqueBlock">
  <h2 class="subSectionName">1. <span class="subSectionSpace"/>Introducere</h2>
  <p>Conform art. 13 şi 14 din Regulament (UE) 2016/679 („Regulamentul”), Banca IFIS S.p.A. („Banca”), în calitate de Titular al prelucrării, doreşte să informeze proprii clienţi, chiar şi potenţiali, precum şi persoanele terţe în general care intră în contact cu aceasta („Persoanele Interesate”), cu privire la faptul că datele personale care se referă la aceştia („Datele”) vor fi prelucrate legal, corect şi transparent, conform modalităţilor şi pentru scopurile ilustrate mai jos.</p>
</div>

<!-- 2. Fonte dei dati personali -->
<div class="uniqueBlock">
  <h2 class="subSectionName">2. <span class="subSectionSpace"/>Sursa de date personale</h2>
  <p>Datele obiect al activităţii de prelucrare din partea Băncii sunt obţinute, direct de la Bancă şi/sau prin intermediul persoanelor terţe desemnate pentru această activitate, la persoanele Interesate şi/sau terţe (inclusiv persoanele care gestionează bănci de date publice, aşa zisele registre publice, etc.), chiar şi prin tehnici de comunizare la distanţă, de care Banca se foloseşte (ex. site-uri web, aplicaţii pentru smartphone şi tabletă, call center, etc.).</p>
</div>

<!-- 3. Finalità del trattamento e base giuridica -->
<div class="uniqueBlock">
  <h2 class="subSectionName">3. <span class="subSectionSpace"/>Scopul prelucrării şi baza juridică</h2>
  <p>Datele sunt prelucrate în cadrul activităţilor normale ale Băncii, în următoarele scopuri:</p>
  <ol class="numericList1">
    <li class="ElementList1">îndeplinirea obligaţiilor legale, atât naţionale, cât şi europene, precum şi a ordinelor/dispoziţiilor Autorităţilor Publice şi/sau a Organismelor de Supraveghere (ex. obligaţii impuse de norme care vizează împiedicarea spălării de bani, terorismul, comercializarea materialelor de pornografie infantilă şi evaziunea fiscală, etc);</li>
    <li class="ElementList1">executarea obligaţiilor ce derivă dintr-un contract încheiat cu persoana interesată şi/sau îndeplinirea, înainte de încheierea contractului, a unor solicitări specifice provenite de la Persoana Interesată, precum şi a altor activităţi legate de executarea şi gestiunea raportului (ex. evaluarea bonităţii; gestiunea încasărilor; verificarea desfăşurării raporturilor, precum şi a riscurilor relative; securitizarea creanţelor; asigurarea creanţelor; operaţiuni de plată chiar şi internaţionale, cum ar fi transferurile peste hotare, pentru care este necesară utilizarea sistemului de mesagerie internaţional SWIFT, etc.);</li>
    <li class="ElementList1">
      <p><u>după aprobarea specifică</u>:</p>
      <p>c.1) dezvăluirea gusturilor, preferinţelor, obişnuinţelor, nevoilor şi alegerilor de consum ale Persoanei Interesate (denumite stabilirea profilului) (doar dacă Persoana interesată este o persoană fizică);</p>
      <p>c.2) promovarea şi oferirea produselor/serviciilor Băncii sau analiza pieţei în vederea descoperirii gradului de satisfacţie a Persoanei Interesate; </p>
      <p>c.3) promovarea şi oferirea de produse/servicii ale terţilor;</p>
      <p>c.4) comunicarea datelor personale către terţi pentru promovarea şi oferirea de produse/servicii ale Băncii sau analiza pieţei în vederea descoperirii gradului de satisfacţie a Persoanei Interesate;</p>
      <p>c.5) comunicarea datelor personale către terţi pentru promovarea şi oferirea de produse/servicii ale acestor terţi.</p>
    </li>
  </ol>
  <p>Potrivit scopurilor de la subpunctele A), B), prelucrarea Datelor de către Bancă, chiar şi pentru comunicarea acestora către subiecţii menţionaţi la paragraful 7 succesiv, în limitele în care această comunicare se dovedeşte funcţională pentru atingerea scopului corespunzător, nu necesită aprobare. În schimb, în ceea ce priveşte scopul de la subpunctul C), Persoana Interesată are dreptul să nu permită, sau să se opună, în orice moment, desfăşurării operaţiunilor prevăzute de prelucrare din partea Băncii. Singura consecinţă ce derivă din acest refuz este imposibilitatea pentru persoana interesată de a beneficia de relativele servicii, fără ca acest fapt să comporte orice consecinţă prejudiciabilă.</p>
  <p>Acordul va putea fi revocat în orice moment, fără a prejudicia legalitatea prelucrărilor precedente.</p>
</div>

<!-- 4. Modalità di trattamento dei dati personali-->
<div class="uniqueBlock">
  <h2 class="subSectionName">4. <span class="subSectionSpace"/>Modalităţile de prelucrare a datelor personale</h2>
  <p>Prelucrarea datelor are loc prin instrumente manuale, informatice şi electronice, cu logici strâns corelate cu scopurile evidenţiate mai sus şi, în orice caz, cu respectarea precauţiilor, a garanţiilor şi a măsurilor necesare prescrise în normativul de referinţă, menite să asigure confidenţialitatea, integritatea şi disponibilitatea Datelor, precum şi să evite daunele, fie ele materiale sau nemateriale (ex. pierderea controlului datelor personale sau limitarea drepturilor, discriminarea, furtul sau uzurparea identităţii, pierderi financiare, descifrarea neautorizată a pseudonimelor, prejudiciul reputaţiei, pierderea confidenţialităţii datelor personale protejate de secretul profesional sau orice altă daună economică sau socială semnificativă).</p>
  <p>Acolo unde este necesar, prelucrările desfăşurate de Bancă se pot baza pe procese decizionale automatizate care produc efecte juridice ce influenţează în mod analog semnificativ persoana interesată: de exemplu, Banca comunică datele personale ale persoanei interesate sistemelor de informaţii de credit la care participă prin trimiterea de fluxuri autorizate prin batch.</p>
</div>


<!-- dati particolari -->
<div class="uniqueBlock">
  <h2 class="subSectionName">5. <span class="subSectionSpace"/>Date speciale</h2>
  <p>În limita necesară atingerii scopului prevăzut la paragraful 3, Băncii i se vor putea aduce la cunoştinţă chiar şi datele pe care Regulamentul le defineşte ca fiind sensibile (ex. datele apte să dezvăluie originea rasială, convingerile religioase sau filozofice, apartenenţa la sindicate, precum şi date genetice, biometrice, date cu privire la sănătate sau la viaţa sexuală sau la orientarea sexuală a persoanei). Aceste date speciale pot fi prelucrate: cu acordul explicit al Persoanei interesate; pentru a absolvi de obligaţii sau pentru exercitarea drepturilor Titularului sau a Persoanelor Interesate în materie de drept al muncii şi siguranţă socială şi protecţie socială; pentru protejarea intereselor vitale ale Persoanelor Interesate sau ale unei alte persoane fizice care se regăseşte în incapacitatea fizică sau juridică de a acorda propriul consens; în cazul în care prelucrarea se referă la date personale făcute publice de Persoana Interesată; dacă prelucrarea este necesară în scopul evaluării capacităţii de muncă a angajatului sub responsabilitatea unui profesionist supus secretului profesional sau de o altă persoană supusă obligaţiei confidenţialităţii; pentru acceptarea, exercitarea sau apărarea unui drept în fază judiciară. În orice caz, datele speciale eventual dobândite vor fi prelucrate respectând cele stabilite în Regulament, precum şi măsurile (generale şi speciale) adoptate în materie de Garante Privacy (Garantarea Confidenţialităţii) şi, în orice caz, în mod confidenţial. Datele sensibile nu vor fi dezvăluite şi vor putea fi comunicate subiecţilor prevăzuţi la paragraful 7, exclusiv pentru atingerea scopurilor şi respectând obligaţiilor de confidenţialitate indicate mai sus.</p>
</div>

<!-- 6. Trasferimento di dati verso Paesi/organizzazioni extra UE -->
<div class="uniqueBlock">
  <h2 class="subSectionName">6. <span class="subSectionSpace"/>Transferul datelor către State/organizaţii extra UE</h2>
  <p>Acolo unde este necesar pentru obţinerea scopurilor de la paragraful 3, Datele Persoanei Interesate pot fi transferate în străinătate, către State/organizaţii extra UE care garantează un nivel de protecţie al datelor personale stabilit de Comisia Europeană prin propria decizie, sau în baza altor garanţii, cum ar fi Clauzele Contractuale Standard adoptate de Comisia Europeană.</p>
  <p>O copie a Datelor eventuale transferate în străinătate, precum şi lista Statelor/organizaţiilor extra UE către care sunt transferate Datele, va putea fi solicitată Titularului folosind adresele indicate în paragrafele succesive 9 şi 10.</p>
</div>

<!-- 7. Categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza -->
<div>
  <h2 class="subSectionName">7. <span class="subSectionSpace"/>Categoriile de subiecţi ale căror date personale pot fi comunicate sau care pot fi aduse la cunoştinţă</h2>
  <p>Pentru atingerea scopurilor descrise la paragraful 3, Banca îşi rezervă dreptul de a comunica Datele către terţi, care aparţin următoarelor categorii:</p>
  <ul style="margin-top:0cm;" type="disc">
    <li>subiecţi care desfăşoară servicii bancare, financiare sau de asigurare;</li>
    <li>Autorităţi şi Organisme de supraveghere şi control şi, în general, subiecţi publici sau privaţi, cu funcţii publice (ex. Unitatea de Informaţii Financiare din Italia; Banca Italia, Fiscul, Centrala de Alarmă Interbancară, Centrala de Riscuri a Băncii Italia, Autoritatea Judiciară din Italia în orice caz doar în limitele condiţiilor stabilite de normativele aplicabile);</li>
    <li>alte societăţi ale grupului din care face parte Banca, sau societăţi mamă, dependente sau legate, potrivit art. 2359 c.c. (situate chiar şi în străinătate);</li>
    <li>subiecţii care prevăd confruntarea datelor furnizate de Persoanele interesate cu cele disponibile în registrele publice, liste, acte sau documentele cunoscute oricăror persoane, în scopul verificării veridicităţii acestora, chiar şi în conformitate cu obligaţiile de verificare corespunzătoare prevăzute de Decretul Anti-Spălare de bani, pe lângă eventuala existenţă a protestelor şi înregistrărilor prejudiciabile; </li>
    <li>subiecţii care efectuează servicii de dobândire, prelucrare şi elaborare a datelor;</li>
    <li>subiecţii care furnizează servicii pentru gestionarea sistemului informativ al Băncii şi a reţelelor de telecomunicaţii (include servicii de mailing);</li>
    <li>subiecţii care desfăşoară activităţi de transmisie, punere în plic, transport şi ghidare a comunicaţiilor;</li>
    <li>subiecţii care desfăşoară activităţi de arhivare a documentaţiei şi introduceri de date;</li>
    <li>subiecţii care desfăşoară activităţi de asistenţă pentru Persoana Interesată;</li>
    <li>societăţi de gestionare a sistemelor naţionale şi internaţionale pentru controlul fraudelor în dauna băncilor şi a intermediarilor financiari;</li>
    <li>birouri profesionale sau societăţi din cadrul raporturilor de asistenţă şi consultanţă;</li>
    <li>sistemele de informare privind creditul, de verificare a coerenţei datelor, de afişare a riscurilor creditului şi insolvenţei;</li>
    <li>agenţi din activitatea financiară, mediatori de credite, alţi operatori intermediari în sectorul de credit, financiar sau bancar;</li>
    <li>persoane care efectuează cercetări de piaţă, menite să arate gradul de satisfacţie al clienţilor asupra calităţii serviciilor şi asupra activităţii desfăşurate de Bancă;</li>
    <li>subiecţi care desfăşoară activităţi de control, revizie şi certificare a activităţii existente a Băncii.</li>
  </ul>
  <p>Persoanele care aparţin categoriilor de mai sus acţionează autonom drept titulari ai prelucrării, sau ca responsabili numiţi de Bancă, a căror listă, actualizată constant, este publicată pe site-ul www.bancaifis.it.</p>
  <p>Datele personale pot fi divulgate, în raport cu desfăşurarea sarcinilor atribuite, de către personalul Băncii, care include stagiarii, lucrătorii temporari, consultanţi, angajaţii societăţilor externe Băncii, dar legate, toţi numiţi în mod corespunzător responsabili cu prelucrarea.</p>
</div>

<!-- 8. Conservazione e cancellazione dei dati personali -->
<div class="uniqueBlock">
  <h2 class="subSectionName">8. <span class="subSectionSpace"/>Păstrarea şi anularea datelor personale</h2>
  <p>Conform prevederilor art. 5 al. 1 lit. e) din Regulament, Datele sunt păstrate într-o formă care să permită identificarea Persoanei Interesate pentru un arc de timp nu mai mare decât cel necesar obţinerii scopurilor pentru care datele sunt prelucrate. După acest termen, Datele vor fi şterse sau transformate în formă anonimă, cu excepţia cazului în care păstrarea lor este necesară pentru absolvirea de obligaţii legale sau pentru îndeplinirea ordinelor emanate de Autorităţile Publice şi/sau de Organismele de Supraveghere.</p>
</div>

<!-- 9. Diritti dell'Interessato -->
<div class="uniqueBlock">
  <h2 class="subSectionName">9. <span class="subSectionSpace"/>Drepturi ale Persoanei Interesate</h2>
  <p>Conform articolelor de la 15 la 22, Regulamentul oferă persoanelor interesate posibilitatea de a exercita drepturi specifice.</p>
  <p>În mod special, Persoana Interesată poate obţine: a) confirmarea existenţei prelucrărilor datelor personale care îl privesc şi, în acest caz, accesul la acele date; b) corectarea datelor personale inexacte şi completarea datelor personale incomplete; c) ştergerea datelor personale care îl privesc, în cazul în care acest lucru este permis de Regulament; d) limitarea prelucrării, în ipotezele prevăzute de Regulament; e) comunicarea, către destinatarii cărora le sunt transmise datele personale, a cererilor de rectificare/ştergere a datelor personale şi de limitare a prelucrării, venite de la Persoana Interesată, cu excepţia cazului în care acest lucru este imposibil sau necesită un efort prea mare; f) primirea, într-un format structurat, de uz comun şi care poate fi citit de un dispozitiv automatic, a datelor personale oferite de Titular, precum şi transmiterea acestora către un alt titular al prelucrării, în orice moment, inclusiv încetarea raporturilor eventual întreţinute cu Titularul.</p>
  <p>Persoana Interesată are dreptul de a se opune în orice moment prelucrării datelor personale care îl privesc: în aceste cazuri, Titularul trebuie să se abţină de la orice prelucrare ulterioară, cu excepţia ipotezelor permise de Regulament. </p>
  <p>Persoana Interesată are de asemenea dreptul de a nu fi supus unei decizii bazate doar pe prelucrarea automatizată, inclusiv profilarea, care să producă efecte juridice care îl privesc sau care să influenţeze în mod analog şi semnificativ persoana, cu excepţia cazului în care această situaţie: a) este necesară pentru încheierea executării unui contract între Persoana Interesată şi Titular; b) este autorizată de dreptul Uniunii sau al Statului membru căruia Titularul îi este supus; c) se bazează pe un acord explicit al Persoanei Interesate. În cazurile prezentate la literele a) şi c), Persoana Interesată are dreptul de a obţine intervenţia umană din partea Titularului, de a exprima propria opinie şi de a contesta decizia.</p>
  <p>Solicitările pot fi prezentate unităţii organizaţionale propuse de Persoana Interesată, pentru înscrierea la sediul Titularului, sau prin mesaj prin poştă electronică la adresa: privacy@bancaifis.it. </p>
  <p>Persoana Interesată are de asemenea dreptul de a reclama Garante Privacy.</p>
</div>

<!-- 10. Titolare e Responsabili del trattamento -->
<div class="uniqueBlock">
  <h2 class="subSectionName">10. <span class="subSectionSpace"/>Titularul prelucrării şi Responsabilul cu Protecţia Datelor</h2>
  <p>Titularul prelucrării datelor personale este Banca IFIS S.p.A., cu sediul în Veneţia-Mestre, via Terraglio, nr. 63. Titularul a numit un Responsabil pentru Protecţia Datelor, care poate fi găsit la adresa <u>rpd@bancaifis.it</u>.</p>
</div>

<!-- INFORMATIVA PRIVACY RELATIVA ALLE ATTIVITÀ DI TRASFERIMENTO DEI FONDI SVOLTA DALLA SWIFT -->
<div class="uniqueBlock">
  <p class="sectionName sectionNameTopLine"> INFORMAŢII DE CONFIDENŢIALITATE CU PRIVIRE LA ACTIVITATEA DE TRANSFER DE FONDURI DESFĂȘURATĂ DE SWIFT </p>
  <p>Pentru a da curs operaţiunilor financiare internaţionale (ex. un transfer internaţional) şi altor operaţiuni specifice naţionale solicitate de clientelă, este necesară utilizarea serviciului de mesagerie internaţional. Acest serviciu este gestionat de „Society for Worldwide Interbank Financial Telecommunication” (SWIFT) cu sediul în Belgia.</p>
  <p>Banca comunică entităţii SWIFT (titularul sistemului SwiftNetFin) datele referitoare la persoana care efectuează tranzacţii (ex. numele persoanei care solicită, al beneficiarului şi al respectivelor bănci, datele bancare şi suma). Altfel, Banca nu ar putea efectua operaţiunile solicitate de clientelă.</p>
  <p>Se precizează, de asemenea, faptul că:</p>
  <ul style="margin-top:0cm;" type="disc">
    <li>toate datele clientelei utilizate pentru efectuarea acestor tranzacţii financiare sunt în momentul de faţă – din motive de securitate operativă – duplicate, transmise şi păstrate temporar în copie de SWIFT într-un server al societăţii situat în Statele Unite ale Americii;</li>
    <li>datele memorate în acest server se pot accesa şi utiliza în SUA, în conformitate cu normativul local de combatere a terorismului, din partea autorităţilor competente americane (ex. Departamentul Trezoreriei).</li>
  </ul>
  <p>Persoana Interesată păstrează toate drepturile prevăzute de normativul în materie de protecţie a datelor personale.</p>
</div>

<!-- INFORMATIVA PRIVACY RELATIVA ALLA REGISTRAZIONE DELLE TELEFONATE -->
<div class="uniqueBlock">
  <p class="sectionName sectionNameTopLine"> INFORMAŢII DE CONFIDENŢIALITATE CU PRIVIRE LA ÎNREGISTRAREA TELEFOANELOR </p>
  <p>Banca doreşte să informeze Persoanele Interesate care au în desfăşurare un contract de locaţiune financiară care este activ un sistem de înregistrare a telefoanelor în domeniul activităţii de propunere telefonică de contracte de asigurare cu privire la produsul leasing deja acordat, desfăşurată de o societate terţă desemnată ca responsabil extern pentru prelucrarea datelor personale. Acest sistem de înregistrare are ca scop instaurarea raporturilor, gestionarea acestora şi monitorizarea calităţii serviciului oferit. Aceste înregistrări vor fi arhivate cu acces rezervat şi păstrate conform legii la sediul societăţii terţe timp de 10 ani începând din momentul în care relativele efecte expiră.</p>
  <p>Înregistrarea apelurilor are loc prin sisteme automatizate care înregistrează apelurile în ieşire, adoptând măsuri care să garanteze siguranţa şi confidenţialitatea datelor dobândite, aşa cum prevede normativul pentru protecţia datelor personale, şi vor putea fi accesate doar de persoanele autorizate în mod explicit în calitate de desemnaţi sau responsabili cu prelucrarea datelor.</p>
  <p>Banca doreşte să informeze Persoanele Interesate care au în desfăşurare un contract de cont care este activ un serviciu de înregistrare a telefoanelor cu privire la apelurile inbound şi outbound gestionate de Serviciul Clienţi pentru analizarea şi monitorizarea calităţii proceselor şi serviciilor oferite clientelei, precum şi pentru apărarea în cazul contestaţiilor înaintate de clientelă.</p>
  <p>Înregistrarea telefoanelor are loc prin intermediul unui sistem automatizat care înregistrează apelurile în intrare şi în ieşire, adoptând măsuri care să garanteze siguranţa şi confidenţialitatea datelor colectate, aşa cum prevede normativul cu privire la protecţia datelor personale. Înregistrările vor fi arhivate cu acces rezervat şi păstrate pentru un interval necesar obţinerii scopurilor pentru care au fost colectate. În orice caz, intervalul de păstrare nu va putea depăşi 10 ani de la data obţinerii înregistrărilor, cu excepţia cazului în care intervin circumstanţe care impun păstrarea ulterioară conform normativului de timp în vigoare. Înregistrările apelurilor pot fi accesate doar de persoanele autorizate în calitate de desemnaţi pentru protecţia datelor personale.</p>
</div>

<!-- INFORMATIVA AI SENSI DELL’ART. 5 DEL CODICE DEONTOLOGICO SUI SISTEMI DI INFORMAZIONI CREDITIZIE -->
<p class="sectionName sectionNameTopLine"> INFORMAŢII CONFORM ART. 5 DIN CODUL DEONTOLOGIC AL SISTEMELOR DE INFORMAŢII DE CREDITE </p>
<p>Conform art. 5 din Codul deontologic al sistemelor de informaţii de credite, Banca IFIS S.p.A. foloseşte anumite date cu privire la clienţi şi debitori. Este vorba despre informaţii deja dobândite sau obţinute consultând anumite bănci de date. Fără aceste date, care ne sunt necesare pentru evaluarea fiabilităţii, nu ar putea fi posibilă acordarea finanţărilor solicitate.</p>
<p>Aceste informaţii vor fi păstrate la Bancă; unele dintre ele vor fi comunicate marilor bănci de date care evaluează riscul de creditare, gestionate de privaţi şi care pot fi consultate de multe persoane. Acest lucru înseamnă că alte bănci sau financiare cărora clienţii/debitorii săi vor solicita un alt împrumut, o finanţare, un card de credit etc., chiar şi pentru a cumpăra în rate un bun de consum, vor putea şti dacă a fost prezentată altei Bănci o cerere recentă de finanţare, dacă sunt în curs alte împrumuturi sau finanţări şi dacă relativele date au fost plătite regulamentar.</p>
<p>În cazul în care plăţile sunt punctuale, păstrarea acestor informaţii din partea băncilor de date necesită acordul Persoanelor Interesate. În cazul plăţilor cu întârziere sau în cazul neplăţii, sau în cazul în care finanţarea se referă la activitatea companiei sau activitatea profesională a Persoanelor Interesate, acest consens nu este necesar.</p>
<p>Persoanele Interesate au dreptul de a cunoaşte datele lor şi de a exercita diversele drepturile relative utilizării lor (corectarea, actualizarea, ştergerea, etc.).</p>
<p>Cererile cu privire la date trebuie transmise Băncii şi/sau societăţilor indicate mai jos, cărora le comunicăm datele: ASSOCIAZIONE ITALIANA LEASING – ASSILEA, CRIF S.P.A., EXPERIAN-CERVED INFORMATION SERVICES S.P.A.</p>
<p>Păstrăm datele la Bancă pentru tot ce este necesar pentru gestionarea finanţării şi pentru îndeplinirea obligaţiilor legale.</p>
<p>Pentru a evalua mai bine riscul de creditare, comunicăm anumite date (date de identificare, chiar şi ale codebitorului, tipologia contractului, suma creanţei, modalitatea de rambursare) sistemelor de informaţii de credite, care sunt reglementate de relativul Cod deontologic şi de bună conduită (publicat în Monitorul Oficial Italian Seria Generală din 23 decembrie 2004, nr. 300; disponibil pe site-ul www.garanteprivacy.it). Datele sunt accesibile şi diverşilor operatori bancari şi financiari participanţi ale căror categorii le indicăm în cele ce urmează.</p>
<p>Datele sunt actualizate periodic cu informaţii dobândite în timpul raportului (situaţia plăţilor, expunerea debitului rămas, statusul raportului).</p>
<p>În domeniul sistemelor de informaţii de credit, datele vor fi prelucrate conform modalităţilor de organizare, comparare şi elaborare indispensabile pentru obţinerea scopurilor mai sus descrise, şi în mod special extragerea în mod echivoc din sistemul de informaţii de credit a informaţiilor înscrise. Aceste elaborări vor fi efectuate prin intermediul instrumentelor informatice, electronice şi manuale care asigură siguranţa şi confidenţialitatea acestora, chiar şi în cazul utilizării tehnicilor de comunicare la distanţă.</p>
<p>Datele sunt obiect al elaborărilor speciale de statistici pentru atribuirea unei opinii sintetice sau a unui punctaj cu privire la gradul de fiabilitate şi solvabilitate (aşa zisul credit scoring), ţinând cont de următoarele tipologii de factori: numărul şi caracteristicile raporturilor de credit în desfăşurare, tendinţa şi istoricul plăţilor raporturilor în curs sau stinse, eventuala prezenţă şi caracteristicile noilor solicitări de credit, istoricul raporturilor de credit stinse. Anumite informaţii suplimentare pot fi oferite în cazul neprimirii unei cereri de credit.</p>
<p>Sistemele de informaţii de credit la care aderă Banca sunt gestionate de:</p>
<ol style="margin-top:0cm;">
  <li>ASSOCIAZIONE ITALIANA LEASING – ASSILEA, cu sediul legal pe Via Massimo d'Azeglio n. 33, 00184 Roma (tel 06/9970361 – fax 06/45440739); site web www.assilea.it; RESPONSABILUL PENTRU ÎNTÂMPINAREA PERSOANELOR INTERESATE: ASSILEA SERVIZI S.R.L. (e-mail: bdcr@assilea.it); TIPUL SISTEMULUI: pozitiv şi negativ; PARTICIPANŢI: bănci şi intermediari financiari care desfăşoară activităţi de locaţiune financiară – leasing; INTERVAL DE PĂSTRARE A DATELOR: cel prevăzut de Codul de deontologie indicat în tabelul de mai jos; UTILIZAREA SISTEMELOR AUTOMATIZATE DE CREDIT SCORING: da.</li>
  <li>CRIF S.P.A., cu sediul pe Via Francesco Zanardi n. 41, 40131 Bologna (tel 051/6458900 – fax 051/6458940); site web www.crif.com; TIPUL SISTEMULUI: pozitiv şi negativ; PARTICIPANŢI: bănci, intermediari financiari şi persoane private care exercită o activitate comercială sau profesională oferind eşalonarea plăţilor pentru furnizarea bunurilor şi a serviciilor; INTERVAL DE PĂSTRARE A DATELOR: cel prevăzut de Codul de deontologie indicat în tabelul de mai jos; UTILIZAREA SISTEMELOR AUTOMATIZATE DE CREDIT SCORING: da.</li>
  <li>EXPERIAN-CERVED INFORMATION SERVICES S.P.A., cu sediul legal pe Via C. Pesenti n. 121/123, 00156 Roma; RESPONSABIL PENTRU ÎNTÂMPINAREA PERSOANELOR INTERESATE: Serviciul pentru Protecţia Consumatorilor, sediul legal (fax 199.101.850 – tel 199.183.538); site web www.experian.it (Zona Consumatori); TIPUL SISTEMULUI: pozitiv şi negativ; PARTICIPANŢI: bănci, intermediari financiari şi persoane private care exercită o activitate comercială sau profesională oferind eşalonarea plăţilor pentru furnizarea bunurilor şi a serviciilor (cu excepţia persoanelor care desfăşoară activităţi de recuperare credite); INTERVAL DE PĂSTRARE A DATELOR: cel prevăzut de Codul de deontologie indicat în tabelul de mai jos; UTILIZAREA SISTEMELOR AUTOMATIZATE DE CREDIT SCORING: da.</li>
</ol>
<p>Persoana Interesată are dreptul de a accesa în orice moment datele care îl privesc, adresând propriile cereri Băncii sau gestionarilor de sisteme de informaţii de credite, folosind modalităţile indicate mai sus.</p>
<p>În acelaşi fel poate solicita corectarea, actualizarea sau completarea datelor inexacte sau incomplete, sau ştergerea sau blocarea celor prelucrate cu încălcarea legii, sau să se opună utilizării acestora din motive legitime care sunt evidenţiate în cerere (art. 8 din Codul deontologic).</p>

<div class="uniqueBlock">
  <p style="margin-top:15px">Intervalul de păstrare a datelor în sistemele de informaţii de credite:</p>
  <table class="borderedTable" style="margin-top: 15px; margin-bottom:15px">
    <col width="30%"/>
    <col width="70%"/>
    <tr>
      <td><b>cereri de finanţare</b></td>
      <td><b>6 luni</b>, în cazul în care se solicită, sau <b>1 lună</b> în cazul respingerii cererii sau renunţării la aceasta</td>
    </tr>
    <tr>
      <td><b>cu întârziere de două rate sau de două luni</b> ulterior <b>rectificate</b></td>
      <td><b>12 luni</b> de la regularizare</td>
    </tr>
    <tr>
      <td><b>întârzieri mai mari rectificate</b> chiar şi la tranzacţie</td>
      <td><b>24 luni</b> de la regularizare</td>
    </tr>
    <tr>
      <td><b>evenimente negative</b> (întârzieri, grave încălcări, pierderi) <b>nerectificate</b></td>
      <td><b>36 luni</b> de la data scadenţei contractuale a raportului sau de la data la care a fost necesară ultima actualizare (în cazul acordurilor succesive sau a altor evenimente importante în raport cu rambursarea)</td>
    </tr>
    <tr>
      <td><b>raporturi</b> care s-au desfăşurat <b>pozitiv</b> (fără întârzieri sau alte evenimente negative)</td>
      <td><b>36 luni</b> în prezenţa altor raporturi cu evenimente negative neregularizate. În cazurile rămase, în prima fază de aplicare a codului de deontologie, termenul va fi de 36 luni de la data încetării raportului sau de scadenţă a contractului, sau de la prima actualizare efectuată în luna succesivă acelei date</td>
    </tr>
  </table>
</div>

<!-- CONSENSO -->
<div class="uniqueBlock">
  <p class="sectionName"> ACORDUL </p>
  <p>Declar că am primit informaţiile despre confidenţialitate, conform art. 13 şi 14 din Regulament (UE) 2016/679 şi 5 din Codul deontologic cu privire la sistemele de informaţii de credite, că am fost informat cu privire la prelucrările desfăşurate, în scopurile şi modalităţile acestora, precum şi la persoanele cărora datele pot fi comunicate respectând aceste scopuri.</p>
  <p>Autorizez Banca IFIS S.p.A. pentru prelucrarea datelor mele personale, chiar şi a celor speciale, şi pentru comunicarea acestora terţilor, inclusiv a sistemelor de informaţii de credit şi bazată pe procese decizionale automatizate, pentru obţinerea scopurilor declarate.</p>

  <p style="margin-top:15px">În particular, în ceea ce priveşte scopurile prevăzute la litera C) a paragrafului 3, declar după cum urmează:</p>

  <table id="tabellaConsensi" style="margin-bottom: 15px">
      <col width="74%"/>
      <col width="13%" align="center"/>
      <col width="13%" align="center"/>
    <tr>
      <th></th>
      <th align="center">AUTORIZEZ</th>
      <th align="center">NU AUTORIZEZ</th>
    </tr>
    <tr>
      <td>1)&nbsp;&nbsp;identificarea gusturilor, preferinţelor, obişnuinţelor, nevoilor şi alegerilor de consum ale Persoanei Interesate (aşa zisa stabilire a profilului) (doar dacă Persoana Interesată este o persoană fizică)</td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoAllaProfilazione__c = TRUE, $Resource.PrivacyPFCheckbox_selected, $Resource.PrivacyPFCheckbox)}"/></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoAllaProfilazione__c = TRUE, $Resource.PrivacyPFCheckbox, $Resource.PrivacyPFCheckbox_selected)}"/></td>
    </tr>

    <tr>
      <td><p>2)&nbsp;&nbsp;promovarea şi oferirea de produse/servicii ale Băncii sau analiza pieţei în vederea descoperirii gradului de satisfacţie a Persoanei Interesate prin modalităţi automatizate şi tradiţionale</p></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoProdottiBancaRicercheMercato__c = TRUE, $Resource.PrivacyPFCheckbox_selected, $Resource.PrivacyPFCheckbox)}"/></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoProdottiBancaRicercheMercato__c = TRUE, $Resource.PrivacyPFCheckbox, $Resource.PrivacyPFCheckbox_selected)}"/></td>
    </tr>

    <tr>
      <td ><p>3)&nbsp;&nbsp;promovarea şi oferirea de produse/servicii ale terţilor, prin modalităţi denumite automatizate şi tradiţionale</p></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoProdottiSocietaTerze__c = TRUE, $Resource.PrivacyPFCheckbox_selected, $Resource.PrivacyPFCheckbox)}"/></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoProdottiSocietaTerze__c = TRUE, $Resource.PrivacyPFCheckbox, $Resource.PrivacyPFCheckbox_selected)}"/></td>
    </tr>

    <tr>
      <td><p>4)&nbsp;&nbsp;comunicarea datelor personale către terţi pentru promovarea şi oferirea de produse/servicii ale Băncii sau analiza pieţei în vederea descoperirii gradului de satisfacţie a persoanei interesate prin modalităţi automatizate şi tradiţionale</p></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoAttivitaPromozionaleTerzi__c = TRUE, $Resource.PrivacyPFCheckbox_selected, $Resource.PrivacyPFCheckbox)}"/></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoAttivitaPromozionaleTerzi__c = TRUE, $Resource.PrivacyPFCheckbox, $Resource.PrivacyPFCheckbox_selected)}"/></td>
    </tr>

    <tr>
      <td><p>5)&nbsp;&nbsp;comunicarea datelor personale către terţi pentru promovarea şi oferirea de produse/servicii ale acestor terţi, prin modalităţi automatizate şi tradiţionale</p></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoAttivitaPromRicercheMercato__c = TRUE, $Resource.PrivacyPFCheckbox_selected, $Resource.PrivacyPFCheckbox)}"/></td>
      <td align="center"><img src="{!IF(Opportunity.Account.ConsensoAttivitaPromRicercheMercato__c = TRUE, $Resource.PrivacyPFCheckbox, $Resource.PrivacyPFCheckbox_selected)}"/></td>
    </tr>
  </table>

  <p>Subsemnatul declar faptul că doresc să primesc înştiinţările promoţionale (prevăzute la punctele 2-5 de mai sus) exclusiv prin modalităţi tradiţionale (ex. poştă comună, telefoane cu operatorul) şi nu prin cele automatizate (ex. e-mail, SMS, MMS, fax, telefoane preînregistrate):</p>
  <p style="margin-left:70px; margin-top: 10px">
    <img style="vertical-align: -3px; width:12px; height:12px; margin-right: 10px" src="{!IF(Opportunity.Account.ConsensoSoloModalitaTradizionali__c = TRUE, $Resource.PrivacyPFCheckbox_selected, $Resource.PrivacyPFCheckbox)}"/> doar modalităţi tradiţionale
  </p>
</div>

<!-- form -->
<div style="margin-top: 30px; text-align: right">
  <!-- NOME e COGNOME -->
  <div style="padding: 13px 0px;">
    <span>NUME şi PRENUME</span>
    <span class="{!IF(OR(ISBLANK(esecutore), ISBLANK(esecutore.Name)), 'hugeLine', 'hiddenInput')}"/>
    <span class="{!IF(AND(!ISBLANK(esecutore), !ISBLANK(esecutore.Name)), 'linedInput', 'hiddenInput')}"> {!esecutore.Name} </span>
  </div>
  <!-- AZIENDA -->
  <div style="padding: 13px 0px;">
    <span>SOCIETATEA</span>
    <span class="{!IF(OR(ISBLANK(Opportunity.Account), ISBLANK(Opportunity.Account.Name)), 'hugeLine', 'hiddenInput')}"/>
    <span class="{!IF(AND(!ISBLANK(Opportunity.Account), !ISBLANK(Opportunity.Account.Name)), 'linedInput', 'hiddenInput')}"> {!Opportunity.Account.Name} </span>
  </div>
</div>
<div style="padding: 13px 0px;">
  <!-- LUOGO e DATA -->
  <div style="display: inline-block">
    <span class="{!IF( ISBLANK(Opportunity.Account.BillingCity), 'normalLine', 'hiddenInput')}" />
    <span class="{!IF( !ISBLANK(Opportunity.Account.BillingCity), 'linedInput', 'hiddenInput')}" > {!Opportunity.Account.BillingCity} </span>
    <span style="font-size:9.0pt;">, data
      <span class= "{!IF( ISBLANK(Opportunity.Account.DataInserimentoConsensi__c), 'normalLine', 'hiddenInput')}"/>
      <span class= "{!IF( !ISBLANK(Opportunity.Account.DataInserimentoConsensi__c), 'linedInput', 'hiddenInput')}">
        <apex:outputText value="{0,date,dd/MM/yyyy}">
          <apex:param value="{!Opportunity.Account.DataInserimentoConsensi__c}" />
        </apex:outputText>
      </span>
    </span>
  </div>
  <!-- FIRMA -->
  <div style="display: inline-block; position: absolute; right: 8px; text-align:right;">
    <span>SEMNĂTURA</span>
    <span class="hugeLine"/>
  </div>
</div>

</body>

</html>


</apex:page>
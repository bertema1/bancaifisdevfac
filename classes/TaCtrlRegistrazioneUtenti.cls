//IMPORTANTE: lasciare without sharing
public without sharing class TaCtrlRegistrazioneUtenti {

  /**
   * Recupera informazioni riguardo ai contatti associati all'account dell'utente Community loggato
   * @return   lista dei contatti
   */
  public static Contact[] fetchContacts() {
    //profili certificati vedono tutti i referenti dell'azienda, gli altri sono quelli che hanno creato
    //nascondo il contatto dell'utente community
    //nascondo il centralino (sigh)
    Contact[] contacts = [SELECT Id,
                          CreatedById,
                          AccountId,
                          Name,
                          FirstName,
                          LastName,
                          CF__c,
                          Ruolo__c,
                          Birthdate,
                          LuogoNascita__c,
                          CodCatastaleLocNascita__c,
                          MobilePhone,
                          Email,
                          Sesso__c,
                          ProvinciaNascita__c,
                          SAE__c,
                          TAECode__c,
                          MailingCountry,
                          MailingState,
                          MailingStreetType__c,
                          MailingStreetName__c,
                          MailingStreetNumber__c,
                          MailingCity,
                          MailingPostalCode,
                          TipoDocumentoId__c,
                          NumeroDoc__c,
                          LuogoEmissioneDoc__c,
                          DataEmissioneDoc__c,
                          DataScadenzaDoc__c,
                          EnteEmitettenteDocumento__c,
                          TitolareDefault__c,
                          EsecutoreDefault__c,
                          TipologiaTitolareEffettivo__c,
                          TipoDocumentoIdentita__c,
                          TelefonoPrefisso__c,
                          TelefonoNumero__c,
                          PEP__c,
                          Cittadinanza__c,
                          PaeseNascita__c,
                          Professione__c,
                          PresaVisionePrivacy__c,
                          (SELECT Id, Name, LatestContentVersionId__c FROM Allegati__r)
                          FROM Contact
                          WHERE AccountId = :TiAnticipoUtils.getUserAccountId()
                          AND Id != :TiAnticipoUtils.getUserContactId()
                          AND FirstName != 'Centralino'];

    if (!TiAnticipoUtils.isUtenteTitolare()) {
      contacts = U.filter(contacts, 'CreatedById', UserInfo.getUserId());
    }

    return contacts;
  }

  /**
   * addContactAllegato description
   * @param  contatto     Oggetto JSON del contatto
   * @param  idAttachment idAttachment del documento di identità
   * @return              successo dell'operazione
   */
  public static Boolean addContactAllegato(String contatto, String idAttachment) {
    Contact con = (Contact)JSON.deserialize(contatto, Contact.class);
    con.AccountId = TiAnticipoUtils.getUserAccountId();
    insert con;
    if (String.isNotBlank(idAttachment)) salvaDocumentoIdentita(con, idAttachment);
    return true;
  }

  /**
  * Modifica un contatto
  * @param  contatto dati del contatto da modificare
  * @return       Booleano per successo dell'operazione
  */
  @RemoteAction
  public static Boolean modifyContactAllegato(String contatto, String idAttachment, String idCessione) {
    Contact con = (Contact)JSON.deserialize(contatto, Contact.class);
    con.AccountId = TiAnticipoUtils.getUserAccountId();
    update con;
    if (String.isNotBlank(idCessione)) {
      NDGLinea__c[] titEffL = [SELECT Id
        FROM NDGLinea__c
        WHERE Contact__c = :con.Id
        AND Opportunita__r.Cessione__c = :idCessione
        AND Tipo__c = 'Titolare effettivo adeguata verifica'
      ];
      if (!titEffL.isEmpty()) {
        update U.massSet(titEffL, 'TipologiaTitolareEffettivo__c', con.TipologiaTitolareEffettivo__c);
      }
    }

    if (String.isNotBlank(idAttachment)) salvaDocumentoIdentita(con, idAttachment);
    return true;
  }

  private static void salvaDocumentoIdentita(Contact con, String idAttachment) {
    ContentVersion v = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :idAttachment];
    Allegato__c allegato = new Allegato__c(
      Contact__c = con.Id,
      Tipo__c = con.TipoDocumentoIdentita__c,
      FamigliaDocumento__c = 'Documenti identità'
    );
    allegato.LatestContentVersionId__c = v.Id;
    upsert allegato;

    ContentDocumentLink cl = new ContentDocumentLink(
      ContentDocumentId = v.ContentDocumentId,
      LinkedEntityId = allegato.Id,
      ShareType = 'I',
      Visibility = 'AllUsers'
    );
    insert cl;

    if (con.TipoDocumentoIdentita__c == 'CartaIdentita') con.TipoDocumentoId__c = '1';
    else if (con.TipoDocumentoIdentita__c == 'Patente') con.TipoDocumentoId__c = '2';
    else if (con.TipoDocumentoIdentita__c == 'Passaporto') con.TipoDocumentoId__c = '3';
    else if (con.TipoDocumentoIdentita__c == 'PortoArmi') con.TipoDocumentoId__c = '4';
    else if (con.TipoDocumentoIdentita__c == 'TesseraPostale') con.TipoDocumentoId__c = '5';
    else if (con.TipoDocumentoIdentita__c == 'Altro10') con.TipoDocumentoId__c = '6';
    update con;

    TaCtrlDocumentazione.chiudiAnomaliaAttore(con.Id);
  }

  public static Boolean aggiungiAttore(Id idCessione, Id idReferente, String tipo) {
    Cessione__c cessione = [SELECT Id, Account__c FROM Cessione__c WHERE Id = :idCessione];
    Opportunity opty = [SELECT Id, Cessione__c FROM Opportunity WHERE Cessione__c = :idCessione];
    Contact referente = [SELECT Id, TipologiaTitolareEffettivo__c FROM Contact WHERE Id = :idReferente];
    Id rt = U.getRecordTypes('NDGLinea__c').get('Referente').Id;

    NDGLinea__c attore = new NDGLinea__c(
      Cessione__c = idCessione,
      Contact__c = idReferente,
      Opportunita__c = opty.Id,
      Tipo__c = tipo, //Titolare effettivo adeguata verifica || Esecutore adeguata verifica
      RecordTypeId = rt
    );
    if (tipo == 'Titolare effettivo adeguata verifica') {
      attore.TipologiaTitolareEffettivo__c = referente.TipologiaTitolareEffettivo__c;
    }
    insert attore;
    return true;
  }

  public static Boolean rimuoviAttore(Id idCessione, Id idReferente, String tipo) {
    delete [SELECT Id FROM NDGLinea__c
            WHERE Cessione__c = :idCessione
                                AND Contact__c = :idReferente
                                    AND Tipo__c = :tipo];
    return true;
  }

  public static List<NDGLinea__c> fetchAttoriCessione(Id idCessione) {
    List<NDGLinea__c> attori = [SELECT
                               Tipo__c,
                               Contact__r.TipologiaTitolareEffettivo__c,
                               Contact__r.Id,
                               Contact__r.Name,
                               Contact__r.FirstName,
                               Contact__r.LastName,
                               Contact__r.CF__c,
                               Contact__r.Ruolo__c,
                               Contact__r.Birthdate,
                               Contact__r.LuogoNascita__c,
                               Contact__r.CodCatastaleLocNascita__c,
                               Contact__r.MobilePhone,
                               Contact__r.Email,
                               Contact__r.Sesso__c,
                               Contact__r.ProvinciaNascita__c,
                               Contact__r.SAE__c,
                               Contact__r.TAECode__c,
                               Contact__r.MailingCountry,
                               Contact__r.MailingState,
                               Contact__r.MailingStreetType__c,
                               Contact__r.MailingStreetName__c,
                               Contact__r.MailingStreetNumber__c,
                               Contact__r.MailingCity,
                               Contact__r.MailingPostalCode,
                               Contact__r.TipoDocumentoId__c,
                               Contact__r.NumeroDoc__c,
                               Contact__r.LuogoEmissioneDoc__c,
                               Contact__r.DataEmissioneDoc__c,
                               Contact__r.DataScadenzaDoc__c,
                               Contact__r.EnteEmitettenteDocumento__c,
                               Contact__r.TitolareDefault__c,
                               Contact__r.EsecutoreDefault__c,
                               Contact__r.Professione__c,
                               Contact__r.PaeseNascita__c,
                               Contact__r.Cittadinanza__c,
                               Contact__r.TelefonoPrefisso__c,
                               Contact__r.TelefonoNumero__c,
                               Contact__r.PEP__c,
                               Contact__r.TipoDocumentoIdentita__c,
                               Contact__r.PresaVisionePrivacy__c,
                               Contact__r.ConsensoAllaProfilazione__c,
                               Contact__r.ConsensoAttivitaPromRicercheMercato__c,
                               Contact__r.ConsensoAttivitaPromozionaleTerzi__c,
                               Contact__r.ConsensoProdottiBancaRicercheMercato__c,
                               Contact__r.ConsensoProdottiSocietaTerze__c,
                               Contact__r.ConsensoSoloModalitaTradizionali__c,
                               Contact__r.ConsensiPrivacyModificati__c
                               FROM NDGLinea__c
                               WHERE Cessione__c = : idCessione];
    return attori;
  }

  public static Boolean removeContact(Id idContact) {
    delete new Contact(Id = idContact);
    return true;
  }
}
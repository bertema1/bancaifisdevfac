/*
Questo file è stato generato e non è il codice sorgente effettivo di questa
classe globale gestita.
Questo file di sola lettura mostra i costruttori, i metodi,
le variabili e le proprietà globali della classe.
Per abilitare il codice per la compilazione, tutti i metodi restituiscono null.
*/
global class trackingWebServices {
    global trackingWebServices() {

    }
    webService static String buttonResponse(String sendDefId) {
        return null;
    }
    webService static void cancelSendDef(String sendDefId) {

    }
    webService static void cancelSmsDef(String smsDefId) {

    }
    webService static void immediateTracking(String sendDefId, String evalString) {

    }
    webService static String requestImmediateTracking(String sendDefId) {
        return null;
    }
    webService static Boolean requestTracking(String sendDefId, String evalString) {
        return null;
    }
}

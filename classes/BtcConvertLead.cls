/**
* Progetto:         Banca IFIS
* Developer:        Michele Triaca
* Description:      Batch che converte i lead provenienti da web o da importazione liste
*                   in prospect. Nel caso di lead web, i prospect convertiti vengono collegati alla campagna web,
*                   e viene creato un task di contatto telefonico assegnato all'operatore di filo diretto interessato
*/
global class BtcConvertLead implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
  @TestVisible private static final String CANALE_WEB = '5';
  public static final String ID_CAMPAGNA_WEB = Impostazioni__c.getInstance().IdCampagnaWeb__c;
  public static Boolean disableTrigger = false;

  Set<Id> leadIds;
  Set<Id> idsToRetry = new Set<Id>();

  global BtcConvertLead() {}
  global BtcConvertLead(Set<Id> leadIds) {
    this.leadIds = leadIds;
  }

  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator(QueryBuilder.newInstance('Lead', new String[]{
      'Id',
      'IFISIdUnicoForm__c',
      'FirstName',
      'LastName',
      'RagioneSociale__c',
      'Phone',
      'Email',
      'PIVA__c',
      'TipoAccount__c',
      'State',
      'PostalCode',
      'Tag__c',
      'CodiceOwner__c',
      'WebID__c',
      'IFISForm__c',
      'Messaggio__c',
      'NumeroTentativiConversione__c'
    })
      .beginFilter()
        .add('Status', QBOp.QEQUAL, 'Da qualificare')
        .add('Id', QBOp.QIN, leadIds)
      .endFilter()
      .getQuery()
    );
  }

  global void execute(Database.BatchableContext BC, List<SObject> scope) {
    TrgAccount.disabilitaCreazioneCTDopoInsertAccount = true;

    System.SavePoint sp;

    Lead l = (Lead) scope[0];
    String piva = l.PIVA__c;

    //VALIDAZIONI
    Boolean isLeadWeb = l.WebID__c != null;

    try {
      if (scope.size() > 1) throw new Ex.BtcConvertLeadException('Il batch di conversione va eseguito con un batchsize = 1');
      if (String.isBlank(piva)) throw new Ex.BtcConvertLeadException('Non è stato possibile qualificare il lead in quanto manca la partita IVA');
      Account a = searchProspect(piva, l.State);
      if(a == null) a = initProspectFromLead(l);
      /**
       * Banca IFIS
       * @author: Alessio Dione
       * Pre-valorizziamo il TAG Controllante per i nuovi Account generati da Lead Web
       * */
      if (a.Id == null && isLeadWeb ) {
          a.TAG_Controllante__c = 'NB Inbound';
      }

      sp = Database.setSavePoint();

      if(isLeadWeb && String.isNotBlank(ID_CAMPAGNA_WEB)) {
        a.DataUltimaImportazioneDaWeb__c = Datetime.now();
        a.CanaleSviluppo__c = CANALE_WEB;
        a.Sottocanale__c = l.IFISForm__c;
        //Se è nuovo prospect, lo assegno all'utenza specificata nel lead se c'è, altrimenti a utenza default
        if(a.Id == null) a.OwnerId = getOwner(l, null);
        if(String.isNotBlank(l.IFISIdUnicoForm__c)) a.IFISIdUnicoForm__c = l.IFISIdUnicoForm__c;
        if(String.isNotBlank(l.Phone)) a.Phone = l.Phone;
        if(String.isNotBlank(l.Email)) a.Email__c = l.Email;
        if(String.isNotBlank(l.State)) a.ShippingState = l.State;
        if(String.isNotBlank(l.PostalCode)) a.ShippingPostalCode = l.PostalCode;
        if(String.isNotBlank(l.TipoAccount__c)) a.TipologiaCliente__c = l.TipoAccount__c;
        if(String.isNotBlank(l.Tag__c)) a.Tag__c = l.Tag__c;

        upsert TrgAccount.gestisciOwner(new Account[]{a});

        Contact contattoDefault = initContactFromLead(l, a);
        upsert contattoDefault;

        CampaignMember[] cmL = [SELECT
          Id,
          ContactId
          FROM CampaignMember
          WHERE Contact.AccountId = :a.Id
          AND CampaignId = :ID_CAMPAGNA_WEB
          AND ContactId = :contattoDefault.Id
          LIMIT 1
          FOR UPDATE
        ];

        //Aggiornando la data ultima importazione da web, si riscatena il sistema che crea i task di contatto telefonico
        CampaignMember cmTarget = !cmL.isEmpty() ? cmL[0] : new CampaignMember(ContactId = contattoDefault.Id, CampaignId = ID_CAMPAGNA_WEB, Status = 'Target');
        BtcConvertLead.disableTrigger = true;
        upsert cmTarget; // workaround: ricalcolo le formule sacrificando 1 DML
        cmTarget.DataUltimaImportazioneDaWeb__c = Datetime.now();
        cmTarget.MessaggioFormWeb__c = l.Messaggio__c;
        BtcConvertLead.disableTrigger = false;
        upsert cmTarget;
      } else {
        // Nel caso di un import manuale, se è stato specificato il codiceOwner sul lead, lo uso come owner preferenziale, ma solo
        // se l'account non è già assegnato a qualcun altro. In questo caso, inoltre, non devo creare nessun contatto telefonico.
        // Questi verranno creati in un momento successivo, quando filo diretto segmenterà la lista importata in n campagne e le attiverà
        a.OwnerId = getOwner(l, a.OwnerId);
        if(String.isNotBlank(l.Phone)) a.Phone = l.Phone;
        if(String.isNotBlank(l.Tag__c)) a.Tag__c = l.Tag__c;
        upsert a;
      }

      l.Status = 'Qualificato';
      l.ConversionMessage__c = '';
      l.AccountCollegato__c = a.Id;
      update l;
    } catch(Ex.BtcConvertLeadException e) {
      if(sp != null) Database.rollback(sp);
      l.Status = 'Non qualificato';
      l.ConversionMessage__c = e.getMessage();
      update l;
    } catch(CalloutException e) {
      if(sp != null) Database.rollback(sp);
      if(l.NumeroTentativiConversione__c == null) l.NumeroTentativiConversione__c = 0;
      l.NumeroTentativiConversione__c++;
      l.Status = l.NumeroTentativiConversione__c >= 3 ? 'Non qualificato' : 'Da qualificare';
      l.ConversionMessage__c = l.NumeroTentativiConversione__c >= 3 ? e.getMessage() + ' - ' + e.getStackTraceString() : '';
      update l;
      idsToRetry.add(l.Id);
    } catch(Exception e) {
      if(sp != null) Database.rollback(sp);
      l.Status = 'Non qualificato';
      l.ConversionMessage__c = e.getMessage() + ' - ' + e.getStackTraceString();
      update l;
    }
  }

  @testVisible
  private Id getOwner(Lead l, Id oldOwnerId) {
    User oldOwner = oldOwnerId != null ? [SELECT Id, IsActive FROM User WHERE Id = :oldOwnerId] : null;

    if (String.isNotBlank(l.CodiceOwner__c)) {
      User[] uL = [SELECT Id FROM User WHERE LoginSiebel__c = :l.CodiceOwner__c AND IsActive = TRUE];
      return uL.isEmpty() ? UtenzeDefault__c.getInstance().IdUtente__c : uL[0].Id;
    } else if (oldOwner != null && oldOwner.IsActive) {
      return oldOwner.Id;
    } else {
      return UtenzeDefault__c.getInstance().IdUtente__c;
    }
  }

  @TestVisible
  private Account initProspectFromLead(Lead l) {
    return new Account(
      Name = l.RagioneSociale__c,
      OwnerId = UtenzeDefault__c.getInstance().IdUtente__c,
      Phone = l.Phone,
      Email__c = l.Email,
      PIVA__c = l.PIVA__c,
      CF__c = l.PIVA__c,
      TipologiaCliente__c = l.TipoAccount__c,
      ShippingState = l.State,
      ShippingPostalCode = l.PostalCode,
      BillingState = l.State,
      BillingPostalCode = l.PostalCode,
      Tag__c = l.Tag__c
    );
  }

  private Contact initContactFromLead(Lead l, Account a) {
    Contact c = new Contact(
      FirstName = l.FirstName,
      LastName = l.LastName,
      Phone = l.Phone,
      Email = l.Email,
      MailingState = l.State,
      MailingPostalCode = l.PostalCode,
      AccountId = a.Id
    );

    Contact[] contattoDaLead = [SELECT
      Id,
      FirstName,
      LastName
      FROM Contact
      WHERE Id IN (SELECT ContactId FROM AccountContactRelation WHERE AccountId = :a.Id)
      AND FirstName = :l.FirstName
      AND LastName = :l.LastName
      AND Email = :l.Email
      AND Phone = :l.Phone
    ];

    if(!contattoDaLead.isEmpty()) {
      c.Id = contattoDaLead[0].Id;
    }

    return c;
  }

  private Account searchProspect(String piva, String provincia) {
    Account[] accsSuCrm = [
      SELECT Id, OwnerId, RecordType.DeveloperName
      FROM Account
      WHERE (PIVA__c = :piva OR CF__c = :piva)
      AND CodiceIstituto3N__c = :CtrlWizardAccount.CI3N_FACTORING
    ];
    Account[] accsCervedAttivi = new Account[]{};

    for(Account a : CtrlWizardAccount.searchAnagraficaRibes(null, piva, null)) {
      if(a.StatoAnagrafica__c == 'ATTIVA') accsCervedAttivi.add(a);
    }

    if(accsCervedAttivi.size() > 1) {
      Account[] accsTmp = new Account[]{};
      for(Account a : accsCervedAttivi) {
        if(a.BillingState == provincia) accsTmp.add(a);
      }
      accsCervedAttivi = accsTmp;
    }

    if(accsSuCrm.size() > 1) throw new Ex.BtcConvertLeadException('Non è stato possibile qualificare il lead perchè esiste più di un account con la stessa partita iva su CRM');

    if(accsSuCrm.isEmpty()) {
      if(accsCervedAttivi.size() == 1) return accsCervedAttivi[0];
      else return null;
    }

    if(accsSuCrm.size() == 1) {
      if(accsCervedAttivi.size() != 1 || accsSuCrm[0].RecordType.DeveloperName != 'Prospect') return accsSuCrm[0];
      else {
        //In questo caso ho trovato un prospect su crm e c'è un cliente che arriva da cerved. Faccio il merge dei dati tra i 2
        Account cervedAcc = accsCervedAttivi[0];
        Account res = accsSuCrm[0];
        res.Name = cervedAcc.Name;
        res.PIVA__c = cervedAcc.PIVA__c;
        res.CF__c = cervedAcc.CF__c;
        if(res.BillingCity != cervedAcc.BillingCity) res.BillingPostalCode = null;
        res.BillingStreet = cervedAcc.BillingStreet;
        res.BillingCity = cervedAcc.BillingCity;
        res.BillingState = cervedAcc.BillingState;
        return res;
      }
    }

    return null;
  }

  global void finish(Database.BatchableContext BC) {
    if(!idsToRetry.isEmpty()) {
      BtcConvertLead b = new BtcConvertLead(idsToRetry);
      Database.executeBatch(b, 1);
    }
  }
}
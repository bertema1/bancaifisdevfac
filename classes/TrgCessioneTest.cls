@isTest
private class TrgCessioneTest {

  @testSetup
  static void setupData () {
    Funzionalita__c f = Funzionalita__c.getInstance();
    f.DisabilitaVRNonIndispensabili__c = true;
    f.DisabilitaControlloCambioFaseOpp__c = true;
    upsert f;

    insert new Prodotto__c(CodiceUnivoco__c = 'ATDTiAnticipo');

    Account a = TestUtils.creaAccount('AccountTest');
    Opportunity opp = TestUtils.creaOpportunity(a);
    Cessione__c c = new Cessione__c(Name = 'test', Account__c = a.Id);
    insert c;
  }

  @isTest
  static void testSetAttoriDefault() {
    Account a = [SELECT Id FROM Account LIMIT 1];
    Contact c = new Contact(LastName = 'test', EsecutoreDefault__c = true);
    insert c;

    Cessione__c cessione = new Cessione__c(Account__c = a.Id);
    insert cessione;
  }

  @isTest
  static void testRinomina() {
    Account a = [SELECT Id FROM Account LIMIT 1];
    String cessioneName = TrgCessione.getCessioneName();
    Cessione__c c = new Cessione__c(Name = 'TestRinomina', Account__c = a.Id);
    insert c;

    c = [SELECT Name FROM Cessione__c WHERE Id = :c.Id];
    System.assert(c.Name == cessioneName);
  }

  @isTest
  static void testRicalcolo() {
    //Cessione__c c = [SELECT Name FROM Cessione__c];
    //Fattura__c f = TestUtils.creaFattura(c);
    //f.recalculateFormulas();

    //c = [SELECT Name, OffertaTotalePreventivo__c
    //     FROM Cessione__c];

    //System.debug(c.OffertaTotalePreventivo__c);
    //System.debug(f.IncassoATD__c);
    //System.assert(c.OffertaTotalePreventivo__c == f.IncassoATD__c);
  }

  @isTest
  static void popolaTimestamp() {
    Test.startTest();
    Cessione__c c = [SELECT Name FROM Cessione__c];
    c.Stato__c = '1';
    update c;

    c.Stato__c = '2';
    update c;

    c.Stato__c = '3';
    update c;

    c.Stato__c = '4';
    update c;

    c.Stato__c = '5';
    update c;
    Test.stopTest();
  }

  @isTest
  static void testDeleteCessione() {
    delete [SELECT Id FROM Cessione__c];
  }

 /* @isTest static void testInviaMail(){
    Test.startTest();
    List<Cessione__c> cessioni = [SELECT Id, Name, Stato__c, ImportoTotaleCertificazioni__c, ImportoTotaleCertificazioniOriginario__c, FirmaDigitale__c FROM Cessione__c];
    Profile p = [select id from profile where name='Utente Amministratore'];
    System.runAs(p){
      cessioni[0].Stato__c = '3';
      cessioni[0].ImportoTotaleCertificazioni__c = 1.0;
      cessioni[0].ImportoTotaleCertificazioniOriginario__c = 1.0;
      cessioni[0].FirmaDigitale__c = true;
      cessioni[1].Stato__c = '3';
      cessioni[1].ImportoTotaleCertificazioni__c = 1.0;
      cessioni[1].ImportoTotaleCertificazioniOriginario__c = 2.0;
      cessioni[1].FirmaDigitale__c = true;
      cessioni[2].Stato__c = '3';
      cessioni[2].ImportoTotaleCertificazioni__c = 1.0;
      cessioni[2].ImportoTotaleCertificazioniOriginario__c = 2.0;
      cessioni[2].FirmaDigitale__c = false;
      cessioni[3].Stato__c = '4';
      cessioni[3].ImportoTotaleCertificazioni__c = 1.0;
      cessioni[3].ImportoTotaleCertificazioniOriginario__c = 2.0;
      update cessioni;
    }
    Test.stoptest();

  }*/
}
/**
* Progetto:         Banca IFIS
* Sviluppata il:    21/10/2016
* Developer:        Zerbinati Francesco
*/

@isTest
public with sharing class TrgCampaignMemberTest {

  @testSetup
  static void dataSetup() {
    Campaign camp = TestUtils.creaCampagna();
    User dora = TestUtils.creaUtente('dora');

    Account a = TestUtils.creaAccount('A',dora);
    Contact c = TestUtils.creaReferente(a);
  }

  @isTest
  static void testCreaTaskContatto() {
    // ottengo campagna, l'account e il contatto che diventerà membro
    Account a = [SELECT Id,OwnerId FROM Account WHERE Name = 'A'];
    Campaign camp = [SELECT Id FROM Campaign WHERE Name = 'Campagna'];
    Contact c = [SELECT Id FROM Contact LIMIT 1];
    // attivo campagna
    camp.DataContatto__c = Date.today();
    camp.IsActive = true;
        update camp;

    Test.startTest();

    TestUtils.creaMembroCampagna(camp, c);

    Test.stopTest();

    Task t = [SELECT Id, Subject, ActivityDate, OwnerId, WhatId, Campagna__c FROM Task WHERE Subject = 'Contatto telefonico campagna' LIMIT 1];

    System.assertEquals('Contatto telefonico campagna',t.Subject);
    System.assertEquals(camp.DataContatto__c,t.ActivityDate);
    System.assertEquals(a.Id,t.WhatId);
    System.assertEquals(a.OwnerId,t.OwnerId);
    System.assertEquals(camp.Id,t.Campagna__c);


  }

}
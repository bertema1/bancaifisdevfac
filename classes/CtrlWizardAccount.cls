/**
* Progetto:         Banca IFIS
* Sviluppata il:    22/12/2016
* Developer:        Zerbinati Francesco, Michele Triaca
*/
public with sharing class CtrlWizardAccount {
  private static final String ORIGINE_CRM = 'CRM';
  private static final String CANALE_DIRETTO = '2';
  private static final String CANALE_INDIRETTO = '4';
  private static final String CANALE_WEB = '5';
  private static final String SOTTOCANALE_NVERDE = '101';
  public static final String CI3N_FACTORING = '881';
  public static final String CI3N_POLONIA = '889';

  public CtrlWizardAccount() {}
  public CtrlWizardAccount(ApexPages.StandardController ctrl) {}
  public CtrlWizardAccount(CtrlFiloDiretto ctrl) {}

  public class InputObj {
    String pivaOrCf;
    String ragioneSociale;
    String provincia;
    String tipoDiRicerca;
    Boolean estero;

    public InputObj(String pivaOrCf, String ragioneSociale, String provincia, String tipoDiRicerca, Boolean estero) {
      this.pivaOrCf = pivaOrCf;
      this.ragioneSociale = ragioneSociale;
      this.provincia = provincia;
      this.tipoDiRicerca = tipoDiRicerca;
      this.estero = estero;
    }
  }

  /**
   * Restituisce true nel caso in cui la ricerca Account venga eseguita sull'istanza di Cedacri polacca
   */
  @RemoteAction
  public static Boolean isPolandSearch() {
    return ImpostazioniServizi__c.getInstance().CodiceIstituto3N__c == CI3N_POLONIA;
  }

  @RemoteAction
  public static List<Account> searchAnagrafica(InputObj inputObj, String platform) {
    Boolean searchByName = inputObj.tipoDiRicerca != 'piva';
      System.debug('SV searchByName: ' + searchByName);
    String paramSearch = searchByName ? inputObj.ragioneSociale : inputObj.pivaOrCf;
    paramSearch = paramSearch.trim();
    String provinceCode;
    if(isPolandSearch()) provinceCode = '';
    else if(inputObj.estero) provinceCode = 'EE';
    else provinceCode = inputObj.provincia;
    //Validazioni
    if (!searchByName && (String.isBlank(paramSearch) || paramSearch.length() < 3) ||
        searchByName && ((String.isBlank(paramSearch) || paramSearch.length() < 3 ))) {
      throw new Ex.WSException(Label.ErroreStringa3Caratteri);
    }

    try {
      List<Account> account = new List<Account>();
      if (platform == 'SFDC') account = searchAnagraficaSFDC(paramSearch, provinceCode, searchByName);
      if (platform == 'RIBES' && !inputObj.estero && !isPolandSearch()) account = searchAnagraficaRibes(
        inputObj.tipoDiRicerca == 'piva' ? '' : inputObj.ragioneSociale,
        inputObj.tipoDiRicerca == 'piva' ? inputObj.pivaOrCf : '',
        provinceCode
      );
      if (platform == 'ANDG') account = WsAnagrafeUtils.searchPG(
        inputObj.tipoDiRicerca == 'piva' ? '' : inputObj.ragioneSociale,
        inputObj.tipoDiRicerca == 'piva' ? inputObj.pivaOrCf : '',
        inputObj.tipoDiRicerca == 'piva' ? inputObj.pivaOrCf : '',
        provinceCode
      );
      return account;
    } catch (DmlException err) {
      System.debug(err);
      throw new Ex.WSException(Label.ErroreRecuperoAnagrafiche, err);
    }
  }

  /**
   * Cerca su Cedacri i referenti corrispondenti
   * @param  cf      codice fiscale del referente da cercare
   * @param  cognome cognome del referente da cercare
   * @param  nome    nome del referente da cercare
   * @return         lista di referenti trovati
   */
  @RemoteAction
  public static List<Contact> searchContattiANDG(String cf, String cognome, String nome) {
    return WsAnagrafeUtils.searchPF(cf, cognome, nome);
  }

  /**
   * Cerca su Salesforce i referenti corrispondenti
   * @param  cf      codice fiscale del referente da cercare
   * @param  cognome cognome del referente da cercare
   * @param  nome    nome del referente da cercare
   * @return         lista di referenti trovati
   */
  @RemoteAction
  public static List<Contact> searchContattiSFDC(String cf, String cognome, String nome) {
    String ci3n = ImpostazioniServizi__c.getInstance().CodiceIstituto3N__c;
    //TODO QUERY RICERCA MIGLIORE
    String query = QueryBuilder.newInstance('Contact', WsAnagraficaBean.REQUIRED_CONTACT_FIELDS, new String[]{
      'AccountId',
      'Account.Name',
      'CodiceIstituto3N__c',
      'StatoFastFinance__c'
    }).beginFilter()
      .beginOr()
        .beginAnd()
          .add('FirstName', QBOp.QLIKE, '%' + nome + '%', String.isNotBlank(nome))
          .add('LastName', QBOp.QLIKE, '%' + cognome + '%', String.isNotBlank(cognome))
        .end()
        .add('CF__c', QBOp.QEQUAL, cf)
      .end()
      .add('IsReferenteCampagna__c', QBOp.QEQUAL, false)
      .add('CodiceIstituto3N__c', QBOp.QEQUAL, ci3n, ci3n == CI3N_FACTORING)
    .endFilter()
    .getQuery();

    List<Contact> cL = Database.query(query);
    U.massSet(cL, 'Origine__c', ORIGINE_CRM);
    return cL;
  }

  public static void truncateAccount (Account inputAccount) {
    UtilAnagrafiche.truncateInSObject(inputAccount, new String[]{
      'Name',
      'RagioneSociale__c'
    }, 80);

    UtilAnagrafiche.truncateInSObject(inputAccount, new String[]{
      'BillingStreetName__c',
      'BillingState',
      'BillingCountry',
      'ShippingStreetName__c',
      'ShippingState',
      'ShippingCountry'
    }, 24);
  }

  @RemoteAction
  public static Account setupAccount(Account inputAccount) {
    Account res = inputAccount;
    Logger.setTransactionContext(null, inputAccount.Id);
    if (
      (String.isBlank(res.NDGGruppo__c) || String.isBlank(res.Id)) &&
      (String.isNotBlank(res.CF__c) || String.isNotBlank(res.PIVA__c))
    ) {
      res = (Account) WsAnagrafeUtils.mergeWithCedacri(res);
      if(String.isNotBlank(res.NDGGruppo__c)) {
        res.Origine__c = res.Id != null ? ORIGINE_CRM : WsAnagrafe.CEDACRI;
      }
    }

    if (res.Origine__c == WsRestRibes.RIBES) {
      WsRestRibesUtils ribesUtils = new WsRestRibesUtils();
      res = ribesUtils.updateDatiAnagraficiRibesPg(res);
    }

    return res;
  }

  //zIgnoreErrors: mantenere z per ordinamento parametri
  @RemoteAction
  public static Id insertAccount(Account inputAccount, String source, Boolean zIgnoreErrors) {
    Logger.setTransactionContext(null, inputAccount.Id);

    // troncamenti a 24 caratteri
    truncateAccount(inputAccount);

    // se inserimento nuovo account da modale, setto owner a utente default
    Id defaultOwner = UtenzeDefault__c.getInstance().IdUtente__c;
    if(inputAccount.Id == null && (source == 'woInserimentoDebitori' || source == 'woInserimentoGaranti')) {
      inputAccount.OwnerId = defaultOwner;
    }

    String canaleSviluppo = (inputAccount.Id != null) ? [SELECT CanaleSviluppo__c FROM Account WHERE Id = :inputAccount.Id].CanaleSviluppo__c : '';

    System.SavePoint sp;

    // Per i prospect il canale è 2 => Diretto, a meno che source non valga fdInbound.
    // In quel caso è web / numero verde e valorizzo anche la data ultima importazione da web.
    if (source == 'fdInbound') {
      inputAccount.CanaleSviluppo__c = CANALE_WEB;
      inputAccount.Sottocanale__c = SOTTOCANALE_NVERDE;
      inputAccount.DataUltimaImportazioneDaWeb__c = Datetime.now();
    } else if (canaleSviluppo != CANALE_WEB && canaleSviluppo != CANALE_INDIRETTO) {
    // Se il canale = web o indiretto, non lo modifico.
      inputAccount.CanaleSviluppo__c = CANALE_DIRETTO;
    }

    // TODO QUESTA PARTE VA RIVISTA (inizio)
    Contact[] referentiRibes = new Contact[]{};
    if (inputAccount.Origine__c == WsRestRibes.RIBES) {
      //se c'è un errore nel recupero dei referenti Ribes, l'account deve comunque essere inserito,
      //per cui non lancio eccezioni, ma creo solo un Log__c
      try {
        referentiRibes = new WsRestRibesUtils().getContacts(inputAccount.Id, inputAccount.REA__c, inputAccount.ProvinciaCCIAA__c);
      } catch(Exception e) {}
    }

    Contact diContact;
    if (inputAccount.NaturaGiuridica__c == 'DI') {
      if (inputAccount.Origine__c == WsRestRibes.RIBES) {
        Integer diIndex = null;
        for (Integer i = 0; i < referentiRibes.size(); i++) {
          if (referentiRibes[i].CF__c == inputAccount.CF__c) {
            diIndex = i;
            break;
          }
        }
        if (diIndex != null) {
          diContact = referentiRibes.remove(diIndex);
        } else {
          throw new Ex.WSException(Label.ErroreRecuperoDIRibes);
        }
      } else if (inputAccount.Origine__c != WsAnagrafe.CEDACRI && inputAccount.Area__c != 'EE') {
        diContact = setupDIContact(inputAccount);
      }
    }
    // TODO QUESTA PARTE VA RIVISTA (fine)

    //censimento light account sincrono
    if (source != 'fdInbound' && String.isBlank(inputAccount.NDGGruppo__c) && !isPolandSearch()) {
      try {
        inputAccount.NDGGruppo__c = WsAnagrafeUtils.censimentoLight(inputAccount, diContact);
      } catch (Exception e) {
        // Se mi sto registrando a TiAnticipo e il censimento non ha buon fine, proseguo
        if (!zIgnoreErrors) throw e;
      }
    }

    //inserimento account in CRM
    sp = Database.setSavePoint();
    try {
      upsert inputAccount;
      //inserimento DI
      if (diContact != null) {
        diContact.AccountId = inputAccount.Id;
        TrgContact.skipCheckCodiceFiscale = true;
        upsert diContact CI3N_CF__c; // TODO POLONIA: sistemare upsert
      }
      Logger.setTransactionContext(null, inputAccount.Id);
    } catch (DmlException e) {
      Database.rollback(sp);
      Logger.log('ANAG_CensimentoLight', e.getDmlMessage(0) + ' - ' + e.getStackTraceString());
      throw new Ex.WSException(Label.ErroreInserimentoAccCont + ' ' + e.getDmlMessage(0), e);
    }

    //inserimento referenti ribes
    if (!referentiRibes.isEmpty()) {
      try {
        U.massSet(referentiRibes, 'AccountId', inputAccount.Id);
        insert referentiRibes;
      } catch (DMLException e) {
        Logger.log('RIBES_ReferentiRibes', e.getDmlMessage(0) + ' - ' + e.getStackTraceString());
      }
    }

    //FUTURE
    if (String.isNotEmpty(inputAccount.NDGGruppo__c) && inputAccount.Origine__c == WsAnagrafe.CEDACRI) {
      WsAnagrafeUtils.syncCollegamentiAsync(inputAccount.NDGGruppo__c, inputAccount.Id);
    }
    //END FUTURE

    return inputAccount.Id;
  }

  @TestVisible
  private static Contact setupDIContact(Account acc) {
    if (String.isNotBlank(acc.BillingCity)) {
      Comune__c[] cL = [SELECT Id, Name, Cap__c FROM Comune__c WHERE Name = :acc.BillingCity ORDER BY FlagStorico__c];
      if (!cl.isEmpty()) {
        acc.BillingPostalCode = cL[0].Cap__c;
        acc.ShippingPostalCode = cL[0].Cap__c;
        acc.BillingCountry = 'ITALIA';
        acc.ShippingCountry = 'ITALIA';
      }
    }

    Contact c = new Contact();
    UtilCodiceFiscale u = new UtilCodiceFiscale();
    UtilCodiceFiscale.ContactInfo reversedInfo = u.reverseCodiceFiscale(acc.CF__c, acc.Name); // TODO INTERNATIONAL: modificare
    c.FirstName = reversedInfo.firstName;
    c.LastName = String.isNotBlank(reversedInfo.lastName) ? reversedInfo.lastName : acc.Name;
    c.Birthdate = reversedInfo.birthDate;
    c.Sesso__c = reversedInfo.sesso;
    c.CodCatastaleLocNascita__c = reversedInfo.codiceCatastale;
    Comune__c[] comuniCompatibili = [SELECT
      Id,
      Name,
      Cap__c
      FROM Comune__c
      WHERE CodiceCatastale__c = :c.CodCatastaleLocNascita__c
      ORDER BY FlagStorico__c
    ];
    if (!comuniCompatibili.isEmpty()) c.LuogoNascita__c = comuniCompatibili[0].Name;
    c.CF__c = acc.CF__c;
    c.CodiceIstituto3N__c = ImpostazioniServizi__c.getInstance().CodiceIstituto3N__c;
    c.CI3N_CF__c = c.CodiceIstituto3N__c + '-' + c.CF__c;
    c.Email = acc.Email__c;

    c.MailingStreet = acc.BillingStreet;
    c.MailingStreetName__c = acc.BillingStreetName__c;
    c.MailingStreetNumber__c = acc.BillingStreetNumber__c;
    c.MailingStreetType__c = acc.BillingStreetType__c;
    c.MailingCity = acc.BillingCity;
    c.MailingState = acc.BillingState;
    c.MailingCountry = 'ITALIA';
    c.MailingPostalCode = acc.BillingPostalCode;

    return c;
  }

  @RemoteAction
  public static Boolean insertContacts(List<Contact> inputContacts) {
    try {
      insert inputContacts;
      return true;
    } catch (DmlException err) {
      throw new Ex.WSException(err.getDmlMessage(0), err);
    }
  }

  @RemoteAction
  public static Id upsertContact(Contact c, AccountContactRelation[] r, String source) {
    System.debug(c);
    System.SavePoint sp = Database.setSavePoint();

    try {
      upsert c;
      Set<Id> accIds = U.getIdSet(r, 'AccountId');

      delete [SELECT Id FROM AccountContactRelation WHERE ContactId = :c.Id AND IsDirect = FALSE AND AccountId NOT IN :accIds];
      Map<String, SObject> accId2acr = U.keyBy([SELECT Id, AccountId, Roles FROM AccountContactRelation WHERE ContactId = :c.Id], 'AccountId');
      Integer i = 0;
      while (i < r.size()) {
        AccountContactRelation acr = r[i];
        if (String.isEmpty(acr.AccountId)) {
          r.remove(i);
        } else {
          acr.ContactId = c.Id;
          if (accId2acr.containsKey(acr.AccountId)) {
            acr.Id = accId2acr.get(acr.AccountId).Id;
          }
          i++;
        }
      }
      if (!r.isEmpty()) upsert r;

      // Nel caso di filo diretto chiamata inbound, il canale dell'account principale va impostato a web
      // con sottocanale 101 (numero verde), se questo non è già web (5) o indiretto (4)
      if (source == 'fdInbound') {
        Account a = [SELECT Id, CanaleSviluppo__c, Sottocanale__c FROM Account WHERE Id = :c.AccountId];
        if (a.CanaleSviluppo__c != CANALE_WEB && a.CanaleSviluppo__c != CANALE_INDIRETTO) {
          a.CanaleSviluppo__c = CANALE_WEB;
          a.Sottocanale__c = SOTTOCANALE_NVERDE;
          update a;
        }
      }
    } catch (DmlException e) {
      Database.rollback(sp);
      throw new Ex.WSException(e.getDmlMessage(0), e);
    }

    // se l'upsert e' andata a buon fine allineo i dati su Cedacri (censendo o aggiornando l'anagrafica)
    if (source != 'fdInbound' && !isPolandSearch()) {
      if (String.isBlank(c.NDGGruppo__c))
        WsAnagrafeUtils.censimentoLightAsync(c.id);
      else
        WsAnagrafeUtils.variazioneAsync(c.id);
    }
    return c.Id;
  }

  /**
   * Cerca Account collegati al Referente
   * @param c: id del Referente
   * @param ndg: ndg del Referente, se presente
   * @return AccountContactRelation[]: lista di AccountContactRelation già presenti su Salesforce
   * o su Cedacri (di cui ho l'accountId su Salesforce)
   */
  @RemoteAction
  public static AccountContactRelation[] getRelations(String contactId, String contactNdg) {

    AccountContactRelation[] acrInSalesforceList = new List<AccountContactRelation>();
    Map<String, SObject> acrInSalesforceMap = new Map<String, SObject>();
    Id cId = String.isBlank(contactId) ? null : (Id) contactId;

    // se ho l'ID del Referente cerco su Salesforce relazioni Account-Referente gia' esistenti..
    if (cId != null) {
      acrInSalesforceList = [SELECT
        AccountId,
        ContactId,
        Roles,
        IsDirect
        FROM AccountContactRelation
        WHERE ContactId = :cId
        ORDER BY CreatedDate ASC
      ];
      acrInSalesforceMap = U.keyBy(acrInSalesforceList, 'AccountId');
    }

    // ..se ho l'NDG cerco su Cedacri eventuali account collegati al Referente..
    if (!String.isBlank(contactNdg)) {
      try {
        WsAnagrafe.GestioneCollNdgResponse response = WsAnagrafe.GestioneCollNdg(contactNdg);
        String[] ndgCollegati = new List<String>();
        List<Account> accountIds = new List<Account>();

        for (WsAnagrafe.ElementoNdg elemento : response.payload.elementi) {
          if (elemento.Ndg != Long.valueOf(contactNdg)) ndgCollegati.add(String.valueOf(elemento.Ndg));
        }

        if (!ndgCollegati.isEmpty()) accountIds = [
          SELECT Id
          FROM Account
          WHERE NDGGruppo__c IN :ndgCollegati
          AND CodiceIstituto3N__c = :ImpostazioniServizi__c.getInstance().CodiceIstituto3N__c
        ];

        // ..che aggiungo alla lista di AccountContactRelation, se presenti su Salesforce
        for (Account acc : accountIds) {
          if (!acrInSalesforceMap.containsKey(acc.Id)) {
            AccountContactRelation acr = new AccountContactRelation(AccountId = acc.Id);
            acrInSalesforceList.add(acr);
          }
        }
      } catch (Exception e) {
        throw new Ex.AnagrafeDiGruppoException(Label.ErroreRichiestaCedacri +  ': ' + e + e.getStackTraceString());
      }
    }
    return acrInSalesforceList;
  }

  @RemoteAction
  public static Contact getContatto(String contactId) {
    return (Contact) Database.query(
      QueryBuilder.newInstance(
        'Contact',
        WsAnagraficaBean.REQUIRED_CONTACT_FIELDS,
        new String[]{'AccountId', 'StatoFastFinance__c'}
      ).withId(contactId).getQuery()
    );
  }

  /*** METODI DI UTILITY PER IL RECUPERO ANAGRAFICHE **/
  private static List<Account> searchAnagraficaSFDC(String paramSearch, String provinceCode, Boolean searchByName) {
    String ci3n = ImpostazioniServizi__c.getInstance().CodiceIstituto3N__c;
    List<Account> accountSFDC = Database.query(
      QueryBuilder.newInstance('Account', WsAnagraficaBean.REQUIRED_ACCOUNT_FIELDS, new String[]{
        'RecordType.Name',
        'Origine__c',
        'CodiceIstituto3N__c'
      }).beginFilter()
        .beginOr()
          .add('PIVA__c', QBOp.QLIKE, '%' + paramSearch + '%', !searchByName)
          .add('CF__c', QBOp.QLIKE, '%' + paramSearch + '%', !searchByName)
        .end()
        .add('Name', QBOp.QLIKE, '%' + paramSearch + '%', searchByName)
        .add('CodiceIstituto3N__c', QBOp.QEQUAL, ci3n, ci3n == CI3N_FACTORING)
        .add('BillingState', QBOp.QEQUAL, provinceCode, String.isNotBlank(provinceCode))
      .endFilter()
      .setLimit(100)
      .getQuery()
    );

    // ho memorizzato origine vera, ma qua voglio mostrare CRM?
    U.massSet(accountSFDC, 'Origine__c', ORIGINE_CRM);
    return accountSFDC;
  }

  public static List<Account> searchAnagraficaRibes(String denominazione, String cf, String provinceCode) {
    List<Account> foundAccount = new List<Account>();

    WsRestRibes.GetListaAziendeResponse res = WsRestRibes.getListaAziende(denominazione, cf, provinceCode);
    for (WsRestRibes.Impresa r : res.getAziende(provinceCode)) {
      //TODO: check mapping
      //MISSING!
      //a.BillingIstatCode__c = r.ISTATCodeMunicipality;
      //

      Account a = new Account(
        Name = r.denominazione,
        NaturaGiuridica__c = r.getFormaGiuridica(),
        ProvinciaCCIAA__c = r.cciaa,
        REA__c = r.nRea,
        PIVA__c = r.pIva,
        VAT__c = r.pIva,
        CF__c = r.codFisc,
        Origine__c = WsRestRibes.RIBES,
        CodiceIstituto3N__c = ImpostazioniServizi__c.getInstance().CodiceIstituto3N__c
      );

      if (r.ateco07 != null) {
        a.DettaglioSettore__c = r.ateco07.cod;
      }

      if (r.indirizzo != null) {
        a.ShippingStreet = String.join(new String[]{
          r.indirizzo.toponimo,
          r.indirizzo.via,
          r.indirizzo.nCivico
        }, ' ');

        a.ShippingCity = r.indirizzo.comune;
        if (r.indirizzo.provincia != null) {
          a.ShippingState = r.indirizzo.provincia.content;
        }
        a.ShippingPostalCode = r.indirizzo.cap;
        a.ShippingStreetName__c = r.indirizzo.via;
        a.ShippingStreetNumber__c = r.indirizzo.nCivico;
        a.ShippingStreetType__c = r.indirizzo.toponimo;

        a.BillingStreet = a.ShippingStreet;
        a.BillingCity = a.ShippingCity;
        a.BillingPostalCode = a.ShippingPostalCode;
        a.BillingState = a.ShippingState;
        a.BillingStreetName__c = a.ShippingStreetName__c;
        a.BillingStreetNumber__c = a.ShippingStreetNumber__c;
        a.BillingStreetType__c = a.ShippingStreetType__c;
      }

      if (r.statoAttivitaR != null) {
        a.StatoAnagrafica__c = r.statoAttivitaR.content;
      }

      a.RecordTypeId = U.getRecordTypes('Account').get('Prospect').Id;

      foundAccount.add(a);
    }

    return foundAccount;
  }

  @RemoteAction
  public static Contact cwaFetchContact(Contact c) {
    Logger.setTransactionContext(null, c.Id);
    try {
      return (Contact) WsAnagrafeUtils.mergeWithCedacri(c);
    } catch (Exception e) {
      throw new Ex.WSException(Label.ErroreRecuperoAnagCedacri + ' ' + e.getMessage(), e);
    }
  }

  @RemoteAction
  public static Ateco__c[] fetchAteco() {
    return [SELECT
      Cedacri__c,
      Descrizione__c,
      RAE__c
      FROM Ateco__c
      ORDER BY Descrizione__c
    ];
  }

  @RemoteAction
  public static NaturaGiuridicaSAE__mdt[] fetchRelazioneNgSae() {
    return [SELECT
      Label,
      SAE__c,
      AtecoRichiesto__c,
      RaeRichiesto__c,
      SedeLegale__c
      FROM NaturaGiuridicaSAE__mdt
    ];
  }

  @RemoteAction
  public static List<Map<String,Object>> getRiepilogoAccountTeam(Id accountId) {
    List<Map<String,Object>> result = new List<Map<String, Object>>();
    AccountTeamMember[] atmList = [SELECT Id,
      UserId,
      User.Name,
      toLabel(TeamMemberRole)
      FROM AccountTeamMember
      WHERE AccountId = :accountId];

    if (atmList.size() != 0) {
      for (AccountTeamMember atm : atmList) {
        result.add(new Map<String, Object> {
          'icona' => 'action-user',
          'id' => atm.Id,
          'nome' => atm.User.Name,
          'ruolo' => atm.TeamMemberRole
        });
      }
    }
    return result;
  }

  @RemoteAction
  public static Map<String, Map<String, Object>> getFieldSets(String fieldSetList, Id sObjectId, String sObjectName) {
    Boolean considerAllFieldSets = String.isBlank(fieldSetList) || fieldSetList.equals('*');
    Set<String> fsSet = new Set<String>();
    Map<String, Schema.FieldSet> fsMap = Schema.getGlobalDescribe().get(sObjectName).getDescribe().FieldSets.getMap();
    Map<String, Map<String, Object>> result = new Map<String, Map<String, Object>>();
    Set<String> fields = new Set<String>();
    fsSet = (considerAllFieldSets) ? fsMap.keySet() : U.toSet(fieldSetList.split(';'));

    // prendo da CRM i valori dei campi contenuti nei fieldSet specificati per l'account corrente
    for (String fieldSetName : U.toList(fsSet)) {
      for(Schema.FieldSetMember f : fsMap.get(fieldSetName.trim()).getFields()) fields.add(f.getFieldPath());
    }
    String query = 'SELECT ' + String.join(U.toList(fields), ', ') + ' FROM ' + sObjectName + ' WHERE Id = :sObjectId';
    // Account a = (Account) Database.query(query)[0];
    SObject sObj = Database.query(query)[0];

    // costruisco la mappa
    for (String fieldSetName : U.toList(fsSet)) {
      Map<String, Object> fieldValues = new Map<String, Object>();
      for (Schema.FieldSetMember f : fsMap.get(fieldSetName).getFields()) {
        fieldValues.put(f.getFieldPath(), (sObj.get(f.getFieldPath()) == null) ? '' : sObj.get(f.getFieldPath()));
      }
      result.put(fsMap.get(fieldSetName).getLabel(), fieldValues);
    }
    return result;
  }

  @RemoteAction
  public static Id upsertObject(String obj, String objName) {
    SObject objToUpsert = (SObject) JSON.deserialize(obj, Type.forName(objName));
    upsert objToUpsert;
    return (Id) objToUpsert.get('Id');
  }

  /**
   * Metodo che verifica che l'utente corrente abbia un profilo tra quelli specificati dal parametro 'profileList'.
   * @param  profileList  lista dei profili accettati, separati da ';',
                          se popolato con valore '*' vengono considerati tutti i profili,
                          se popolato con valore 'AdminsOnly' o vuoto vengono considerati solo i profili degli amministratori
   * @return              true, se l'utente corrente è tra i profili specificati; false altrimenti
   */
  @RemoteAction
  public static Boolean renderModifica(String profileList) {
    Boolean considerAllProfiles = profileList.equals('*');
    Boolean considerAdminsOnly = String.isBlank(profileList) || profileList.equals('AdminsOnly');
    Set<String> profileSet = new Set<String>{'System Administrator', 'Amministratore del sistema'};
    if (!considerAllProfiles && !considerAdminsOnly) profileSet.addAll(U.toSet(profileList.split(';')));

    String profileQuery = QueryBuilder.newInstance('Profile', new String[] {'Id'})
      .beginFilter()
        .add('Name', QBOp.QIN, profileSet, !considerAllProfiles)
        .endFilter()
      .getQuery();

    return U.getIdSet(Database.query(profileQuery), 'Id').contains(UserInfo.getProfileId());
  }
}
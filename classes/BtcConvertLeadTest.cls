@isTest
private class BtcConvertLeadTest {

  @testSetup
  public static void dataSetup() {
    Campaign campagna = TestUtils.creaCampagna();
    TestUtils.CreaImpostazioni(campagna);
    User dora = TestUtils.creaUtente('dora');
    dora.LoginSiebel__c = 'ls';
    update dora;
    TestUtils.impostaCS();
    TestUtils.creaEndpointServizi();
  }

  @isTest
  public static void testConvertLeadTriggered() {
    // Il batch viene eseguito dal trigger 'TrgLead'. Con questo metodo testo l'intero batch ('start' compreso).

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_RIBES_GETLISTAAZIENDE)); //TODO remove?
    TestUtils.creaLead('Da qualificare', 'ifisIdUnicoForm', 'firstName', 'lastName', 'ragioneSociale', '3393537383', 'email@email.com', '02168580120', '1', 'state', 'postalCode', 'tag', 'codiceOwner', 'messaggio', '1', 3);
    Test.stopTest();

    Lead l = [SELECT Status, ConversionMessage__c, AccountCollegato__c, RagioneSociale__c, Phone, Email, PIVA__c, TipoAccount__c, State, PostalCode, Tag__c, IFISForm__c, IFISIdUnicoForm__c
              FROM Lead
              ];

/* TODO -> VERIFICARE PERCHE' IN PROD IL TEST FALLISCE CON UNCOMMITTED WORK PENDING. NON SUCCEDE SULLE ALTRE ORG
    System.assertEquals(null, l.ConversionMessage__c);
    System.assertEquals('Qualificato', l.Status);

    Account a = [SELECT Name, OwnerId, Phone, Email__c, PIVA__c, CF__c, TipologiaCliente__c, ShippingState, ShippingPostalCode, BillingState, BillingPostalCode, Tag__c, CanaleSviluppo__c, Sottocanale__c, IFISIdUnicoForm__c
                 FROM Account
                 ];

    System.assertEquals(a.Id, l.AccountCollegato__c);
    System.assertEquals(l.RagioneSociale__c, a.Name);
    System.assertEquals(l.Phone, a.Phone);
    System.assertEquals(l.Email, a.Email__c);
    System.assertEquals(l.PIVA__c, a.PIVA__c);
    System.assertEquals(l.PIVA__c, a.CF__c);
    System.assertEquals(l.TipoAccount__c, a.TipologiaCliente__c);
    System.assertEquals(l.State, a.ShippingState);
    System.assertEquals(l.PostalCode, a.ShippingPostalCode);
    System.assertEquals(l.State, a.BillingState);
    System.assertEquals(l.PostalCode, a.BillingPostalCode);
    System.assertEquals(l.Tag__c, a.Tag__c);

    // e' un lead web
    System.assertEquals(BtcConvertLead.CANALE_WEB, a.CanaleSviluppo__c);
    System.assertEquals(l.IFISForm__c, a.Sottocanale__c);
    System.assertEquals(l.IFISIdUnicoForm__c, a.IFISIdUnicoForm__c);
*/
  }

  @isTest
  public static void testConvertLead() {
    // Se in futuro il 'TrgLead' verra' rimosso, questo metodo testa comunque l'execute del batch.

    TestUtils.creaLead('Stato che non fa scattare il trigger', 'ifisIdUnicoForm', 'firstName', 'lastName', 'ragioneSociale', '3393537383', 'email@email.com', '02168580120', '1', 'state', 'postalCode', 'tag', 'codiceOwner', 'messaggio', '1', 3);
    TestUtils.creaAccount('nomeAccount', 'MI', '02168580120', null);

    Set<Id> leads = U.getIdSet([SELECT Id FROM Lead], 'Id');

    Test.startTest();
    BtcConvertLead bcl = new BtcConvertLead(leads);
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_RIBES_GETLISTAAZIENDE));//TODO remove?
    bcl.execute(null, [SELECT Id,
                              IFISIdUnicoForm__c,
                              FirstName,
                              LastName,
                              RagioneSociale__c,
                              Phone,
                              Email,
                              PIVA__c,
                              TipoAccount__c,
                              State,
                              PostalCode,
                              Tag__c,
                              CodiceOwner__c,
                              WebID__c,
                              IFISForm__c,
                              Messaggio__c,
                              NumeroTentativiConversione__c FROM lead]);
    Test.stopTest();

    Lead l = [SELECT Status, ConversionMessage__c, AccountCollegato__c, RagioneSociale__c, Phone, Email, PIVA__c, TipoAccount__c, State, PostalCode, Tag__c, IFISForm__c, IFISIdUnicoForm__c
              FROM Lead
              ];

    Account a = [SELECT Name, OwnerId, Phone, Email__c, PIVA__c, CF__c, TipologiaCliente__c, ShippingState, ShippingPostalCode, BillingState, BillingPostalCode, Tag__c, CanaleSviluppo__c, Sottocanale__c, IFISIdUnicoForm__c
                 FROM Account
                 ];

    System.assertEquals('Qualificato', l.Status);
    System.assertEquals(null, l.ConversionMessage__c);
    System.assertEquals(a.Id, l.AccountCollegato__c);
    System.assertEquals(l.Phone, a.Phone);
    System.assertEquals(l.Email, a.Email__c);
    System.assertEquals(l.TipoAccount__c, a.TipologiaCliente__c);
    System.assertEquals(l.State, a.ShippingState);
    System.assertEquals(l.PostalCode, a.ShippingPostalCode);
    System.assertEquals(l.Tag__c, a.Tag__c);

    // e' un lead web
    System.assertEquals(BtcConvertLead.CANALE_WEB, a.CanaleSviluppo__c);
    System.assertEquals(l.IFISForm__c, a.Sottocanale__c);
    System.assertEquals(l.IFISIdUnicoForm__c, a.IFISIdUnicoForm__c);
  }

  @isTest
  public static void testInitProspectFromLead() {
    TestUtils.creaLead('Stato che non fa scattare il trigger', 'ifisIdUnicoForm', 'firstName', 'lastName', 'ragioneSociale', '3393537383', 'email@email.com', '02168580120', '1', 'state', 'postalCode', 'tag', 'codiceOwner', 'messaggio', '1', 3);
    BtcConvertLead bcl = new BtcConvertLead();

    Lead l = [SELECT Status, ConversionMessage__c, AccountCollegato__c, RagioneSociale__c, Phone, Email, PIVA__c, TipoAccount__c, State, PostalCode, Tag__c, IFISForm__c, IFISIdUnicoForm__c
              FROM Lead];

    Test.startTest();
    Account a = bcl.initProspectFromLead(l);
    Test.stopTest();

    System.assertEquals(l.RagioneSociale__c, a.Name);
    System.assertEquals(UtenzeDefault__c.getInstance().IdUtente__c, a.OwnerId);
    System.assertEquals(l.Phone, a.Phone);
    System.assertEquals(l.Email, a.Email__c);
    System.assertEquals(l.PIVA__c, a.PIVA__c);
    System.assertEquals(l.PIVA__c, a.CF__c);
    System.assertEquals(l.TipoAccount__c, a.TipologiaCliente__c);
    System.assertEquals(l.State, a.ShippingState);
    System.assertEquals(l.PostalCode, a.ShippingPostalCode);
    System.assertEquals(l.State, a.BillingState);
    System.assertEquals(l.PostalCode, a.BillingPostalCode);
    System.assertEquals(l.Tag__c, a.Tag__c);
  }

  @isTest
  public static void testGetOwner() {
    Lead l = TestUtils.creaLead('Stato che non fa scattare il trigger', 'ifisIdUnicoForm', 'firstName', 'lastName', 'ragioneSociale', '3393537383', 'email@email.com', '02168580120', '1', 'state', 'postalCode', 'tag', 'ls', 'messaggio', '1', 3);

    Test.startTest();
    BtcConvertLead bcl = new BtcConvertLead();
    Id result = bcl.getOwner(l, null);
    Test.stopTest();

    System.assertEquals([SELECT Id FROM User WHERE LoginSiebel__c = 'ls'][0].Id, result);

    // wip: completare i casi di test
  }
}
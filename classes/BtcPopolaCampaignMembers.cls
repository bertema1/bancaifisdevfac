/**
* Progetto:         Banca IFIS
* Sviluppata il:    20/12/2016
* Developer:        Michele Triaca
* Description: classe per migrazione dati. Sugli account migrati è presente un campo IdSiebelCampagna che dice che
* quell'account è stato utilizzato in quella campagna su siebel. Il sistema collega il referente campagna di quell'account
* alla relativa campagna creando un campaign member
*/
global class BtcPopolaCampaignMembers implements Database.Batchable<SObject> {

  String query;

  global BtcPopolaCampaignMembers() {
    query = 'SELECT Id, Account.IdSiebelCampagna__c FROM Contact WHERE IsReferenteCampagna__c = TRUE AND Account.IdSiebelCampagna__c != NULL AND Id NOT IN (SELECT ContactId FROM CampaignMember)';
  }

  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator(query);
  }

  global void execute(Database.BatchableContext BC, List<Contact> scope) {
    Map<String, SObject> cMap = U.keyBy([
      SELECT Id,
      IdSiebel__c
      FROM Campaign
      WHERE IdSiebel__c IN :U.getSet(scope, 'Account.IdSiebelCampagna__c')
    ], 'IdSiebel__c');

    CampaignMember[] cmL = new CampaignMember[]{};

    for(Contact c : scope) {
      String idSiebel = c.Account.IdSiebelCampagna__c;
      if(cMap.containsKey(idSiebel)) {
        cmL.add(new CampaignMember(
          ContactId = c.Id,
          CampaignId = cMap.get(idSiebel).Id,
          Status = 'Target'
        ));
      }
    }

    TrgCampagna.disableTrigger = true;
    if(!cmL.isEmpty()) insert cmL;
  }

  global void finish(Database.BatchableContext BC) {}

}
public without sharing class TrgLead {
  /**
   * Al cambio di stato in 'Da qualificare', resetto il contatore dei tentativi di conversione,
   * in modo che se fallisce la chiamata al cerved il batch ci riprovi di nuovo n volte invece che una sola
   * Quando: before insert/update
   * D: Michele Triaca
   */
  public static void resetConversionCounter(T tu) {
    for(Lead l : (Lead[]) tu.filter('Status', 'Da qualificare').getChanged('Status')) {
      l.NumeroTentativiConversione__c = 0;
    }
  }

  /**
   * Al cambio di stato in 'Da qualificare' faccio partire il batch di conversione lead
   * Quando: after insert/update
   * D: Michele Triaca
   */
  public static void startConversionBatch(T tu) {
    Lead[] lL = tu.filter('Status', 'Da qualificare').getChanged('Status');
    if(!lL.isEmpty()) {
      BtcConvertLead b = new BtcConvertLead(U.getIdSet(lL, 'Id'));
      Database.executeBatch(b, 1);
    }
  }
}
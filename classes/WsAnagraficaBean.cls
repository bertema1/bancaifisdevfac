public with sharing class WsAnagraficaBean {

  public Decimal codiceIstituto3N;

  //NDG
  public String ndg;

  //Natura giuridica
  public String naturaGiuridica;

  //Attività economica
  public Decimal sae;
  public Decimal rae;
  public String ateco;
  public String codiceTAE;

  //Intestazione
  public String cognome;
  public String nome;
  public String attivita;

  //Intestazione x variazione. WORKAROUND BUG CEDACRI SU MAPPATURA INTESTAZIONE
  public String cognomeV;
  public String nomeV;
  public String attivitaV;

  //CCIAA
  public Date dataIscrizioneLegale;
  public Decimal rea;

  //Documento contact
  public String tipoDoc;
  public String numeroDoc;
  public Date dataEmissioneDoc;
  public String enteDoc;
  public Date dataScadenzaDocumento;

  //Indirizzi
  public String billingCity;
  public String billingStreetName;
  public String billingStreetType;
  public String billingStreetNumber;
  public Decimal billingPostalCode;
  public String billingCountry;
  public String billingState;

  public String shippingCity;
  public String shippingStreetName;
  public String shippingStreetType;
  public String shippingStreetNumber;
  public Decimal shippingPostalCode;
  public String shippingCountry;
  public String shippingState;

  //Dati contact
  public String sesso;
  public Date dataDiNascita;
  public String luogoDiNascita;
  public String provinciaDiNascita;

  //Piva-CF
  public String codiceFiscale;
  public String partitaIVA;

  //Dati di contatto
  public String email;
  public String numeroCellulare;
  public String prefissoTelefonoFisso;
  public String numeroTelefonoFisso;
  public String numeroFax;

  //Privacy
  public String consensoProfilazione;
  public String consensoProdMkt;
  public String consensoProdTerzi;
  public String consensoPromozMkt;
  public String consensoPromozTerzi;
  public String consensoModTradiz;
  public Datetime dataConsensi;

  // Dati aggiuntivi
  public Boolean partecipazioneSocietaFiduciarie;

  public static final String[] REQUIRED_ACCOUNT_FIELDS = new String[]{
    'Ateco__c',
    'BillingCity',
    'BillingCountry',
    'BillingPostalCode',
    'BillingState',
    'BillingStreetName__c',
    'BillingStreetNumber__c',
    'BillingStreetType__c',
    'BU__c',
    'CF__c',
    'ComportamentoRilevato__c',
    'ConsensoAllaProfilazione__c',
    'ConsensoAttivitaPromozionaleTerzi__c',
    'ConsensoAttivitaPromRicercheMercato__c',
    'ConsensoProdottiBancaRicercheMercato__c',
    'ConsensoProdottiSocietaTerze__c',
    'ConsensoSoloModalitaTradizionali__c',
    'DataFatturato__c',
    'DataInserimentoConsensi__c',
    'DataIscrizioneCCIAA__c',
    'DettaglioCensimento__c',
    'Email__c',
    'Fatturato__c',
    'Fax',
    'GestoreCliente__c',
    'GestoreDebitore__c',
    'Id',
    'Name',
    'NaturaGiuridica__c',
    'NDGGruppo__c',
    'Owner.NDGGruppo__c',
    'OwnerId',
    'PaeseSvolgimentoAttivitaPrevalente__c',
    'PIVA__c',
    'ProvinciaAttivitaPrevalente__c',
    'ProvinciaCCIAA__c',
    'QualificaCliente__c',
    'RAE__c',
    'REA__c',
    'SAE__c',
    'ScopoRapporto__c',
    'ShippingCity',
    'ShippingCountry',
    'ShippingPostalCode',
    'ShippingState',
    'ShippingStreetName__c',
    'ShippingStreetNumber__c',
    'ShippingStreetType__c',
    'StatoCensimento__c',
    'TAECode__c',
    'TelefonoNumero__c',
    'TelefonoPrefisso__c'
  };

  public static final String[] REQUIRED_CONTACT_FIELDS = new String[]{
    'Birthdate',
    'CF__c',
    'CodCatastaleLocNascita__c',
    'ConsensoAllaProfilazione__c',
    'ConsensoAttivitaPromozionaleTerzi__c',
    'ConsensoAttivitaPromRicercheMercato__c',
    'ConsensoProdottiBancaRicercheMercato__c',
    'ConsensoProdottiSocietaTerze__c',
    'ConsensoSoloModalitaTradizionali__c',
    'DataEmissioneDoc__c',
    'DataInserimentoConsensi__c',
    'DataScadenzaDoc__c',
    'DettaglioCensimento__c',
    'Email',
    'EnteEmitettenteDocumento__c',
    'Fax',
    'FirstName',
    'Id',
    'LastName',
    'LuogoEmissioneDoc__c',
    'LuogoNascita__c',
    'MailingCity',
    'MailingCountry',
    'MailingPostalCode',
    'MailingState',
    'MailingStreetName__c',
    'MailingStreetNumber__c',
    'MailingStreetType__c',
    'MobilePhone',
    'NDGGruppo__c',
    'NumeroDoc__c',
    'Phone',
    'ProvinciaNascita__c',
    'SAE__c',
    'Sesso__c',
    'StatoCensimento__c',
    'TAECode__c',
    'TelefonoNumero__c',
    'TelefonoPrefisso__c',
    'TipoDocumentoId__c'
  };
  public final static String NATURAGIURIDICA_PF = 'PF';
  public static final String ERR_DI_SENZA_CONTACT = 'La ditta individuale non ha un titolare su CRM. Inserire il titolare e riprovare la convalida';
  private Set<String> dateFields = new Set<String>{'dataIscrizioneLegale', 'dataEmissioneDoc', 'dataDiNascita', 'dataScadenzaDocumento'};
  private Set<String> datetimeFields = new Set<String>{'dataConsensi'};
  private Set<String> numericFields = new Set<String>{'codiceIstituto3N'};

  /**
   * SOQL per i campi obbligatori dell'account/contatto. Nel caso dell'account viene presa anche l'area.
   * @param  oId id dell'account/contatto
   * @return     account/contatto
   */
  public static SObject getEntity(Id oId) {
    String oName = oId.getSobjectType().getDescribe().getName();
    String[] accountFields = new List<String>(REQUIRED_ACCOUNT_FIELDS);
    accountFields.addAll(new String[]{'Area__c', 'PartecipazioneSocietaFiduciarie2__c'});
    String[] fields = oName == 'Account' ? accountFields : REQUIRED_CONTACT_FIELDS;
    SObject o = (Sobject) Database.query(QueryBuilder.newInstance(oName, fields).withId(oId).getQuery());
    return o;
  }

  private Contact getRelatedContact(String codiceFiscale, Id accId) {
    if(accId == null) throw new Ex.AnagrafeDiGruppoException(ERR_DI_SENZA_CONTACT);

    Contact[] cL = (Contact[]) Database.query(QueryBuilder.newInstance('Contact', REQUIRED_CONTACT_FIELDS)
      .beginFilter()
        .add('CF__c', QBOp.QEQUAL, codiceFiscale, true)
        .addUntyped('Id IN (SELECT ContactId FROM AccountContactRelation WHERE AccountId = \'' + accId + '\')')
      .endFilter()
      .getQuery()
    );

    if (cL.isEmpty()) throw new Ex.AnagrafeDiGruppoException(ERR_DI_SENZA_CONTACT);
    return cL[0];
  }

  private void fixDatiIntestazioneBugCedacri() {
    if(naturaGiuridica == 'DI' || naturaGiuridica == 'PF') {
      nomeV = nome;
      cognomeV = cognome;
      attivitaV = attivita;
    } else {
      if(attivita.length() <= 60) {
        cognomeV = attivita;
        nomeV = '${NULL}';
      } else {
        cognomeV = attivita.substring(0, 60);
        nomeV = attivita.substring(60);
      }
      attivitaV = '${NULL}';
    }
  }

  private void fillDatiServizio() {
    codiceIstituto3N = Decimal.valueOf(ImpostazioniServizi__c.getInstance().CodiceIstituto3N__c);
  }

  public void fillFrom(Id oId) {
    SObject o = getEntity(oId);
    if(o.getSObjectType() == Account.SObjectType) fillFrom((Account) o);
    else fillFrom((Contact) o);
  }

  public void fillFrom(Account a) {
    fillFrom(a, null);
  }

  public void fillFrom(Account a, Contact c) {
    if(a.NaturaGiuridica__c == 'DI') {
      fillFrom(c == null ? getRelatedContact(a.CF__c, a.Id) : c);
    }

    ndg = a.NDGGruppo__c;
    naturaGiuridica = a.NaturaGiuridica__c;
    attivita = a.Name.replace('"', '').toUpperCase(UserInfo.getLocale());
    sae = String.isBlank(a.SAE__c) ? 0 : Decimal.valueOf(a.SAE__c);
    rae = String.isBlank(a.RAE__c) ? 0 : Decimal.valueOf(a.RAE__c);
    fillAteco(a.Ateco__c);
    codiceTAE = a.TAECode__c;
    dataIscrizioneLegale = a.DataIscrizioneCCIAA__c;
    rea = String.isBlank(a.REA__c) ? 0 : Decimal.valueOf(a.REA__c);
    billingCity = a.BillingCity;
    billingStreetName = a.BillingStreetName__c;
    billingStreetType = a.BillingStreetType__c;
    billingStreetNumber = a.BillingStreetNumber__c;
    billingPostalCode = String.isBlank(a.BillingPostalCode) ? null : Decimal.valueOf(a.BillingPostalCode);
    billingCountry = a.BillingCountry;
    billingState = a.BillingState;
    shippingCity = a.ShippingCity;
    shippingStreetName = a.ShippingStreetName__c;
    shippingStreetType = a.ShippingStreetType__c;
    shippingStreetNumber = a.ShippingStreetNumber__c;
    shippingPostalCode = String.isBlank(a.ShippingPostalCode) ? null : Decimal.valueOf(a.ShippingPostalCode);
    shippingCountry = a.ShippingCountry;
    shippingState = a.ShippingState;
    codiceFiscale = a.CF__c;
    partitaIVA = a.PIVA__c;
    email = a.Email__c;
    prefissoTelefonoFisso = a.TelefonoPrefisso__c;
    numeroTelefonoFisso = a.TelefonoNumero__c;
    numeroFax = a.Fax;
    fillPrivacy(a);
    fillDatiAggiuntivi(a);

    fixDatiIntestazioneBugCedacri();
    fillDatiServizio();
  }

  public void fillFrom(Contact c) {
    ateco = '${NULL}';
    ndg = c.NDGGruppo__c;
    naturaGiuridica = NATURAGIURIDICA_PF;
    sae = String.isBlank(c.SAE__c) ? 0 : Decimal.valueOf(c.SAE__c);
    codiceTAE = c.TAECode__c;
    cognome = String.isBlank(c.LastName) ? null : c.LastName.toUpperCase(UserInfo.getLocale());
    nome = String.isBlank(c.FirstName) ? null : c.FirstName.toUpperCase(UserInfo.getLocale());
    billingCity = c.MailingCity;
    billingStreetName = c.MailingStreetName__c;
    billingStreetType = c.MailingStreetType__c;
    billingStreetNumber = c.MailingStreetNumber__c;
    billingPostalCode = String.isBlank(c.MailingPostalCode) ? null : Decimal.valueOf(c.MailingPostalCode);
    billingCountry = c.MailingCountry;
    billingState = c.MailingState;
    sesso = c.Sesso__c;
    dataDiNascita = c.Birthdate;
    luogoDiNascita = c.LuogoNascita__c;
    provinciaDiNascita = c.ProvinciaNascita__c;
    fillLocalitaDiNascita(c.CodCatastaleLocNascita__c);
    codiceFiscale = c.CF__c;
    email = c.Email;
    numeroCellulare = c.MobilePhone;
    numeroFax = c.Fax;
    prefissoTelefonoFisso = c.TelefonoPrefisso__c;
    numeroTelefonoFisso = c.TelefonoNumero__c;
    tipoDoc = c.TipoDocumentoId__c;
    numeroDoc = c.NumeroDoc__c;
    dataEmissioneDoc = c.DataEmissioneDoc__c;
    dataScadenzaDocumento = c.DataScadenzaDoc__c;
    enteDoc = c.EnteEmitettenteDocumento__c;
    fillPrivacy(c);

    fixDatiIntestazioneBugCedacri();
    fillDatiServizio();
  }

  private void fillPrivacy(SObject o) {
    consensoProfilazione = o.get('ConsensoAllaProfilazione__c') == true ? 'S' : 'N';
    consensoPromozTerzi = o.get('ConsensoAttivitaPromozionaleTerzi__c') == true ? 'S' : 'N';
    consensoPromozMkt = o.get('ConsensoAttivitaPromRicercheMercato__c') == true ? 'S' : 'N';
    consensoProdMkt = o.get('ConsensoProdottiBancaRicercheMercato__c') == true ? 'S' : 'N';
    consensoProdTerzi = o.get('ConsensoProdottiSocietaTerze__c') == true ? 'S' : 'N';
    consensoModTradiz = o.get('ConsensoSoloModalitaTradizionali__c') == true ? 'S' : 'N';
    dataConsensi = (Datetime) o.get('DataInserimentoConsensi__c');
  }

  private void fillDatiAggiuntivi(SObject o) {
    partecipazioneSocietaFiduciarie = o.get('PartecipazioneSocietaFiduciarie2__c') == true;
  }

  private void fillAteco(String ateco) {
    Ateco__c[] aL = [SELECT Id, Name, Cedacri__c FROM Ateco__c WHERE Name = :ateco OR Cedacri__c = :ateco];
    if(aL.isEmpty() || aL[0].Name == '0') this.ateco = null;
    else this.ateco = aL[0].Cedacri__c;
  }

  private void fillLocalitaDiNascita(String codCatastale) {
    if(String.isBlank(codCatastale)) return;
    Comune__c c = UtilAnagrafiche.getComune(codCatastale);
    if(c != null) {
      provinciaDiNascita = c.Provincia__c;
      luogoDiNascita = c.Name;
    }
  }

  public String toJSON(String templateName) {
    StaticResource doc = [SELECT Id, Body FROM StaticResource WHERE Name = :templateName];
    String template = doc.Body.toString();

    String j = JSON.serialize(this);
    Map<String, Object> objMap = (Map<String, Object>) JSON.deserializeUntyped(j);

    template = evaluateIf(template, objMap);

    for(String k : objMap.keySet()) {
      if (numericFields.contains(k)) {
        template = template.replace('%' + k + '%', convert(k, objMap.get(k)));
      } else {
        template = template.replace('"%' + k + '%"', convert(k, objMap.get(k)));
      }
    }

    System.debug('PAYLOAD GENERATO!! ' + (String.isBlank(template) ? '' : template.replaceAll('\n', ' ')));
    return template;
  }

  private String evaluateIf(String template, Map<String, Object> context) {
    String t = template;
    final String BEGIN_IF = '//if %';
    final String END_IF = '//endif\n';
    final Pattern ISNOTBLANK = Pattern.compile('ISNOTBLANK\\(([^\\)]+)\\)');

    Integer prevIdx = 0;
    do {
      prevIdx = t.indexOf(BEGIN_IF);
      if(prevIdx != -1) {
        Integer keyIdx = prevIdx + BEGIN_IF.length();
        Integer endIdx = t.indexOf(END_IF, keyIdx);
        Integer keyEndlineIdx = t.indexOf('\n', keyIdx) + 1;

        String key = t.substring(keyIdx, t.indexOf('%', keyIdx));
        Matcher m = ISNOTBLANK.matcher(key);
        Boolean checkNotBlank = false;
        if(m.matches()) {
          key = m.group(1);
          checkNotBlank = true;
        }

        Object v = context.get(key);
        t = t.substring(0, prevIdx) +
          (v == null || v == '' || (checkNotBlank && (v == '0' || v == 0)) ? '' : t.substring(keyEndlineIdx, endIdx)) +
          t.substring(endIdx + END_IF.length());
      }
    } while(prevIdx != -1);

    return t;
  }

  private String convert(String k, Object v) {
    if(v == null) {
      return 'null';
    } else if(dateFields.contains(k)) {
      Date d = (Date) JSON.deserialize('"' + v + '"', Date.class);
      return String.valueOf(UDate.getLongValue(d));
    } else if(datetimeFields.contains(k)) {
      Datetime d = (Datetime) JSON.deserialize('"' + v + '"', Datetime.class);
      return String.valueOf(d.getTime());
    } else if(v == 'true' || v == 'false') {
      return String.valueOf(v);
    } else {
      return '"' + String.valueOf(v) + '"';
    }
  }

  public void validate(Boolean fullValidation) {
    Set<String> types = new Set<String>();
    String type = naturaGiuridica == 'PF' ? 'PF' : (naturaGiuridica == 'DI' ? 'DI' : 'PG');
    types.add('light' + type);
    if(fullValidation) types.add('full' + type);

    CampoAnagrafe__mdt[] campiObbligatori = [SELECT
      Label
      FROM CampoAnagrafe__mdt
      WHERE Mandatory__c = TRUE
      AND Tipo__c IN :types
    ];

    String[] campiMancantiLabel = new String[]{}; // UtilAnagrafiche.getCampiMancanti(acc, campiObbligatori); TODO FINIRE
    if (!campiMancantiLabel.isEmpty()) {
      throw new Ex.AnagrafeDiGruppoException('Mancano i seguenti campi obbligatori per il censimento: ' + String.join(campiMancantiLabel, ', '));
    }
  }
}
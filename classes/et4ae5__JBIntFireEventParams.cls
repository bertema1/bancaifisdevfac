/*
Questo file è stato generato e non è il codice sorgente effettivo di questa
classe globale gestita.
Questo file di sola lettura mostra i costruttori, i metodi,
le variabili e le proprietà globali della classe.
Per abilitare il codice per la compilazione, tutti i metodi restituiscono null.
*/
global class JBIntFireEventParams {
    @InvocableVariable( required=true)
    global String ContactKey;
    @InvocableVariable( required=true)
    global String ContactPersonType;
    @InvocableVariable( required=true)
    global String EventDataConfig;
    @InvocableVariable( required=true)
    global String EventDefinitionKey;
    @InvocableVariable( required=false)
    global String EventType;
    @InvocableVariable( required=true)
    global String OwnerMID;
    @InvocableVariable( required=true)
    global String SalesforceObjectId;
    @InvocableVariable( required=true)
    global String SalesforceObjectName;
    @InvocableVariable( required=true)
    global String VersionNumber;
    global JBIntFireEventParams() {

    }
}

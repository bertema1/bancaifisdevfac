@isTest
private class TaCtrlDettaglioTest {

  private static Integer nCessioni = 10;
  private static Integer nFatture = 3;
  private static Integer nAnomalie = 3;

  @testSetup
  static void setupData () {
    User u = TestUtils.creaUtente('user1');
    Account a = TestUtils.creaAccount('AccountTest', u);
    insert new Prodotto__c(CodiceUnivoco__c = 'ATDTiAnticipo');
    Cessione__c[] cessioni = new Cessione__c[] {};
    for (Integer i = 0; i < nCessioni; i++) {
      cessioni.add(new Cessione__c(Name = 'test', Account__c = a.Id));
    }
    insert cessioni;

    Anomalia__c[] anomalie = new Anomalia__c[] {};
    for (Integer i = 0; i < nAnomalie; i++) {
      anomalie.add(
        new Anomalia__c(
          Cessione__c = cessioni[0].Id,
          Risolta__c = false,
          TipoAnomalia__c = 'DURC',
          SottoTipoAnomalia__c = 'Documento scaduto',
          DescrizioneAnomalia__c = 'Descrizione test'
        )
      );
    }
    insert anomalie;

  }

  @isTest
  static void testFetchCessioni() {
    // System.assert(TaCtrlDettaglio.fetchCessioni().size() == nCessioni); //todo test: sistemare
    Cessione__c c = [SELECT Id FROM Cessione__c LIMIT 1];
    System.assert(TaCtrlDettaglio.fetchCessione(c.Id).cessione.Id == c.Id);
  }

  @isTest
  static void testUpsertCessione() {
    Cessione__c c = [SELECT Id FROM Cessione__c LIMIT 1];
    TaCtrlDettaglio.upsertCessione(JSON.serialize(c));
  }

  @isTest
  static void testFetchAnomalie() {
    TaCtrlDettaglio.fetchAnomalie();
    // System.assert(TaCtrlDettaglio.fetchAnomalie().size() == nAnomalie); // todo test: sistemare
  }

  @isTest
  static void testSetReadedMessage() {
    Anomalia__c a = new Anomalia__c(Cessione__c = [SELECT Id FROM Cessione__c LIMIT 1].Id,
                                    Stato__c = 'Non Letto');
    insert a;

    TaCtrlDettaglio.setReadedMessage(a.Id);

    a = [SELECT Id, Stato__c FROM Anomalia__c WHERE Id = :a.Id];
    System.assert(a.Stato__c == 'Letto');
  }

  @isTest
  static void testCertificazioniFatture() {
    Account a = [SELECT Id FROM Account WHERE Name = 'AccountTest'];
    Cessione__c cessione = [SELECT Id FROM Cessione__c LIMIT 1];
    Certificazione__c certificazione = new Certificazione__c(Cessione__c = cessione.Id, TerminiPagamento__c = 30);
    insert certificazione;

    Test.startTest();
      TaCtrlDettaglio.fetchUploadCertificazioneInfo(cessione.Id);
      TaCtrlDettaglio.fetchCertificazioniFatture(cessione.Id);
      TaCtrlDettaglio.fetchFatture(cessione.Id);
      TaCtrlDettaglio.deleteCertificazione(certificazione.Id);
    Test.stopTest();
  }

  @isTest
  static void testUpdateFatture() {
    Account a = [SELECT Id FROM Account WHERE Name = 'AccountTest'];
    Cessione__c cessione = [SELECT Id FROM Cessione__c LIMIT 1];
    Certificazione__c cert = new Certificazione__c(Cessione__c = cessione.Id, TerminiPagamento__c = 30);
    insert cert;

    Fattura__c f = new Fattura__c(CertificazioneParent__c = cert.Id, DataFattura__c = Date.today());
    insert f;

    // TestUtils.creaCessioneCompleta(a);
    Test.startTest();
    TaCtrlDettaglio.updateFatture(JSON.serialize([SELECT Id FROM Fattura__c]));
    Test.stopTest();
  }

  @isTest
  static void testAssociaDebitoriCertificazioni() {
    // todo test: sistemare
    // User u = TestUtils.creaUtente('user2');
    // Account a = TestUtils.creaAccount('AccountTest', u);
    // Account debitore = TestUtils.creaAccount('Debitore', u);
    // debitore.CF__c = '12345678910';
    // debitore.SegmentoRischio__c = '1';
    // update debitore;
    // Cessione__c cessione = TestUtils.creaCessioneCompleta(a);

    // Certificazione__c[] certificazioni = [SELECT Id FROM Certificazione__c WHERE Cessione__c = :cessione.Id];
    // for (Certificazione__c c : certificazioni) {
    //   c.Debitore__c = debitore.id;
    //   c.ImportoCertificato__c = 10;
    //   c.DataPagamento__c = Date.newInstance(2020, 2, 17);
    // }
    // update certificazioni;

    // Test.startTest();
    // TaCtrlDettaglio.associaDebitoriCertificazioni(certificazioni);
    // Test.stopTest();
  }
}
@isTest
private class TaCtrlRegistrazioneUtentiTest {

  @testSetup
  public static void setupData() {
    insert new Prodotto__c(CodiceUnivoco__c = 'ATDTiAnticipo');
  }

  @isTest
  static void testFetchContacts() {
    try {
      TaCtrlRegistrazioneUtenti.fetchContacts();
    } catch (Exception e) {}
  }

  @isTest
  static void testAddContactAllegato () {
    Contact c = new Contact(FirstName = 'Paolo', LastName = 'Rossi');

    ContentVersion cv = new ContentVersion(Title = 'test', ContentURL = 'http://www.google.com/');
    insert cv;

    TaCtrlRegistrazioneUtenti.addContactAllegato(JSON.serialize(c), cv.Id);
  }

  @isTest
  static void testModifyContact () {
    Contact c = new Contact(FirstName = 'Paolo', LastName = 'Rossi');
    insert c;

    ContentVersion cv = new ContentVersion(Title = 'test', ContentURL = 'http://www.google.com/');
    insert cv;

    TaCtrlRegistrazioneUtenti.modifyContactAllegato(JSON.serialize(c), cv.Id, null);
    TaCtrlRegistrazioneUtenti.removeContact(c.Id);
  }


  @isTest
  static void testAggiungiAttore () {
    Account a = TestUtils.creaAccount('AccountTest');
    Cessione__c cessione = TestUtils.creaCessioneCompleta(a);

    Contact c = new Contact(FirstName = 'Paolo', LastName = 'Rossi');
    insert c;

    TaCtrlRegistrazioneUtenti.aggiungiAttore(cessione.Id, c.Id, 'Titolare effettivo adeguata verifica');
    TaCtrlRegistrazioneUtenti.fetchAttoriCessione(cessione.Id);
    TaCtrlRegistrazioneUtenti.rimuoviAttore(cessione.Id, c.Id, 'Titolare effettivo adeguata verifica');
  }
}
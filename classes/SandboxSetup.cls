global class SandboxSetup implements SandboxPostCopy {

  global void runApexClass(SandboxContext context) {
    System.debug('Org ID: ' + context.organizationId());
    System.debug('Sandbox ID: ' + context.sandboxId());
    System.debug('Sandbox Name: ' + context.sandboxName());

    String sandboxUrl = System.Url.getSalesforceBaseURL().getHost();
    String[] sandboxUrlSections = sandboxUrl.split('\\.');
    String sandboxName = sandboxUrlSections[0].substringAfter('--');
    String sandboxInstance = sandboxUrlSections[1];

    // EndpointServizi
    EndpointServizi__c[] endpointServizi = [SELECT Id FROM EndpointServizi__c];
    for (EndpointServizi__c es : endpointServizi) {
      es.LAGKeyHeroku__c = 'B3DAF392-AF8C-4687-8387-48261E9A63FD';
      es.LAGKey__c = 'B3DAF392-AF8C-4687-8387-48261E9A63FD';
      es.URLAllineaKnet__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/anagraficacompositeservice/setdatianagraficiifis';
      es.URLGetBilancioSinteticoRibes__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/bilanciosinteticocompositeservice/bilanciosintetico';
      es.URLGetEventiNegativiRibes__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/eventinegativicompositeservice/eventinegativi';
      es.URLGetInformazioniPGRibes__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/getprodottopgcompositeservice/getprodottopg';
      es.URLGetListaAziendeRibes__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/getlistaaziendecompositeservice/getlistaaziende';
      es.URLModificaDatiAggiuntiviQQ__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/modificadatiaggiuntiviqqcompositeservice/modificadatiaggiuntiviqq';
      es.UrlAggiornamentoStatoCartella__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/nvaggiornamentostatocartellacompositeservice/nvaggiornamentostatocartella';
      es.UrlAggiungiOggetti__c = 'https://sulsfmw01.bancaifis.it:8040/services/rest/InviaDoc';
      es.UrlCancellaOggetti__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/documentmanager/filenetaggiornamento/cancellaoggetti';
      es.UrlCheckStatoPef__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/checkstatopefcompositeservice/checkstatopef';
      es.UrlDatiBilancio__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/anagraficacompositeservice/datibilancio';
      es.UrlDettaglioOggetto__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/documentmanager/dettagliooggettocompositeservice/dettagliooggetto';
      es.UrlEsistePef__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/checkesistenzapefcompositeservice/checkesistenzapef';
      es.UrlFileNet__c = 'http://filenetp8.collaudo.ced.it';
      es.UrlGestioneCollNdg__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/gestionecollndgcompositeservice/gestionecollndg';
      es.UrlGetDatiAnagraficiCedacri__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/anagraficacompositeservice/getdatianagraficicedacri';
      es.UrlGetDocumentThroughTalend__c = 'https://sulsfmw01.bancaifis.it:8040/services/rest/GetDoc';
      es.UrlInfoCr__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/posizionerischio/primainformazionecrcompositeservice/primainformazionecr';
      es.UrlInterrogazioneBilanci__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/bilanci/interrogazionebilanci';
      es.UrlNvAggiornamentoInnescaVendita__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/nvaggiornamentoinnescavenditacompositeservice/nvaggiornamentoinnescavendita';
      es.UrlNvInquiryStatoCartella__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/nvinquirystatocartellacompositeservice/nvinquirystatocartella';
      es.UrlRenderPdf__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/pratica/adobecompositeservice/renderpdf';
      es.UrlRicercaAnagraficaCedacri__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/ricercaanagraficacedacricompositeservice/ricercaanagraficacedacri';
      es.UrlSearchDocument__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/documentmanager/ricercadocumenti/searchdocument';
      es.UrlSetAnagraficaCedacri__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/setanagraficacedacricompositeservice/setanagraficacedacri';
      es.UrlSetAnagraficaLight__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/setanagraficalightcompositeservice/setanagraficalight';
      es.UrlSetAnagraficaVariazione__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/anagrafevariazionecompositeservice/setanagraficavariazione';
      es.UrlSetRetianagraficheIfis__c = 'https://uatservices.bancaifis.it/arch/lag/proxy/anagrafica/anagraficacompositeservice/setretianagraficheifis';
      es.UserIdOverride__c = 'ext.crm';
      es.UrlLogger__c = 'https://' + sandboxUrl;
      es.UrlCertificazioneParser__c = 'https://pdf-ifis-test-' + sandboxName + '.herokuapp.com/tianticipo';
      es.UrlDocumentiTaStatici__c = 'https://pdf-ifis-test-' + sandboxName + '.herokuapp.com';
    }
    update endpointServizi;

    // Funzionalità
    Funzionalita__c[] funzionalita = [SELECT Id FROM Funzionalita__c];
    for (Funzionalita__c f : funzionalita) {
      f.DisabilitaInvioEmail__c = true;
    }
    update funzionalita;

    // Parametri TiAnticipo
    ParametriTiAnticipo__c[] parametriTiAnticipo = [SELECT Id FROM ParametriTiAnticipo__c];
    for (ParametriTiAnticipo__c pTa : parametriTiAnticipo) {
      pTa.UsernameSuffix__c = 'ta.' + sandboxName;
    }
    update parametriTiAnticipo;
  }
}
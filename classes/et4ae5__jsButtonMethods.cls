/*
Questo file è stato generato e non è il codice sorgente effettivo di questa
classe globale gestita.
Questo file di sola lettura mostra i costruttori, i metodi,
le variabili e le proprietà globali della classe.
Per abilitare il codice per la compilazione, tutti i metodi restituiscono null.
*/
global class jsButtonMethods {
    global jsButtonMethods() {

    }
    webService static String activateTriggeredSend(String trigSendId) {
        return null;
    }
    webService static String getPACId(String acctId) {
        return null;
    }
    webService static Boolean isETIntegrated() {
        return null;
    }
    webService static String namespace() {
        return null;
    }
    webService static String pauseTriggeredSend(String trigSendId) {
        return null;
    }
    webService static String performResub(String subKey, String objectType) {
        return null;
    }
    webService static String performUnsub(String subKey, String objectType) {
        return null;
    }
    webService static void resetConfiguration() {

    }
    webService static String saveRemember(et4ae5.jsButtonMethods.rememberParams params) {
        return null;
    }
global class rememberParams {
    @WebService
    webService String isChecked;
    @WebService
    webService String url;
    @WebService
    webService String userId;
    global rememberParams() {

    }
}
}

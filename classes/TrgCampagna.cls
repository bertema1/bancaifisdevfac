/**
* Progetto:         Banca IFIS
* Descrizione:      Classe handler del trigger Campagna.trigger
* Sviluppata il:    21/10/2016
* Developer:        Zerbinati Francesco, Michele Triaca
*/

public without sharing class TrgCampagna {
  private TrgCampagna() {}

  public static Boolean disableTrigger = false;

  /**
  * Metodo che crea un task sul contatto dell'account in membri campagna
  **/
  public static void creaTaskContatto(T tu) {
    Campaign[] camps = tu.filter('IsActive', true).getChanged('IsActive');
    if(camps.isEmpty()) return;

    creaTaskDaCampaignMember([SELECT
      Id,
      AccountOwner__c,
      AccountId__c,
      AccountOpFiloDiretto__c,
      Cap__c,
      Comune__c,
      Provincia__c,
      CampaignId,
      NomeCampagna__c,
      DataContattoCampagna__c,
      DataUltimaImportazioneDaWeb__c,
      ContactId,
      TipologiaCliente__c,
      MessaggioFormWeb__c,
      CanaleCampagna__c
      FROM CampaignMember
      WHERE CampaignID IN :camps
    ]);
  }

  /**
  * Metodo di appoggio usato da creaTaskContatto e anche dal trigger dei campaign member
  **/
  public static void creaTaskDaCampaignMember(CampaignMember[] cmL) {
    if(cmL.isEmpty()) return;

    Set<Id> setUtenti = U.getIdSet(cmL, 'AccountOwner__c');
    setUtenti.addAll(U.getIdSet(cmL, 'AccountOpFiloDiretto__c'));

    Map<Id,User> utenti = new Map<Id,User>([SELECT Id, IsActive FROM User WHERE Id IN :setUtenti]);
    CU.addToCache(utenti.keySet());

    CampaignMember[] cmLStandard = U.filter(cmL, 'CanaleCampagna__c', 'Sviluppo Diretto');
    CampaignMember[] cmLFD = U.filter(cmL, 'CanaleCampagna__c', 'Filo Diretto');

    Map<String, Map<String, Id>> tipoCampagna2ownerCapMap = new Map<String, Map<String, Id>>{
      'fd' => UtilAnagrafiche.getOwners(U.getSet(cmLFD, 'Cap__c'), U.getSet(cmLFD, 'Comune__c'), U.getSet(cmLFD, 'Provincia__c'), true),
      'altri' => UtilAnagrafiche.getOwners(U.getSet(cmLStandard, 'Cap__c'), U.getSet(cmLStandard, 'Comune__c'), U.getSet(cmLStandard, 'Provincia__c'), false)
    };

    System.debug('MAPPA OWNERS!!! ' + tipoCampagna2ownerCapMap);

    Map<Id, Account> accsToUpdate = new Map<Id, Account>();
    Map<Id, Account> accsToReassign = new Map<Id, Account>();
    Map<Id, Account> accs = new Map<Id, Account>([SELECT Id, CanaleSviluppo__c, Sottocanale__c, OwnerId FROM Account WHERE Id IN :U.getIdSet(cmL, 'AccountId__c')]);

    for(CampaignMember cm : cmL) {
      String canale = cm.CanaleCampagna__c == 'Filo Diretto' ? 'fd' : 'altri';
      Map<String, Id> ownerCap = tipoCampagna2ownerCapMap.get(canale);
      Id ownerId = ownerCap.get('0000');
      if(String.isNotBlank(cm.Cap__c) && ownerCap.containsKey(cm.Cap__c)) ownerId = ownerCap.get(cm.Cap__c);
      else if(String.isNotBlank(cm.Comune__c) && ownerCap.containsKey(cm.Comune__c)) ownerId = ownerCap.get(cm.Comune__c);
      else if(String.isNotBlank(cm.Provincia__c) && ownerCap.containsKey(cm.Provincia__c)) ownerId = ownerCap.get(cm.Provincia__c);

      Account a = accs.get(cm.AccountId__c);

      if(a.CanaleSviluppo__c != '5' && a.CanaleSviluppo__c != '4') {
        a.CanaleSviluppo__c = '2';
        if(String.isBlank(a.Sottocanale__c)) a.Sottocanale__c = cm.NomeCampagna__c;
        accsToUpdate.put(a.Id, a);
      }

      if(!utenti.get(cm.AccountOwner__c).IsActive || UtenzeDefault__c.getInstance(cm.AccountOwner__c).AssegnatarioDiDefault__c) {
        a.OwnerId = ownerId;
        accsToUpdate.put(cm.AccountId__c, a);
        accsToReassign.put(cm.AccountId__c, a);
      }
    }

    update accsToUpdate.values();


    Task[] res = new Task[]{};
    for(CampaignMember cm : (CampaignMember[]) U.filter(cmL, 'TipologiaCliente__c', '3', false)) {
      String canale = cm.CanaleCampagna__c == 'Filo Diretto' ? 'fd' : 'altri';
      Map<String, Id> ownerCap = tipoCampagna2ownerCapMap.get(canale);

      Id ownerId = ownerCap.get('0000');
      if(CU.isFiloDiretto(cm.AccountOpFiloDiretto__c) && utenti.get(cm.AccountOpFiloDiretto__c).IsActive && cm.CampaignId == BtcConvertLead.ID_CAMPAGNA_WEB) ownerId = cm.AccountOpFiloDiretto__c;
      else if(CU.isFiloDiretto(cm.AccountOwner__c) && utenti.get(cm.AccountOwner__c).IsActive && cm.CanaleCampagna__c == 'Filo Diretto') ownerId = cm.AccountOwner__c;
      else if(CU.isCommerciale(cm.AccountOwner__c) && utenti.get(cm.AccountOwner__c).IsActive && cm.CanaleCampagna__c != 'Filo Diretto') ownerId = cm.AccountOwner__c;
      else if(String.isNotBlank(cm.Cap__c) && ownerCap.containsKey(cm.Cap__c)) ownerId = ownerCap.get(cm.Cap__c);
      else if(String.isNotBlank(cm.Comune__c) && ownerCap.containsKey(cm.Comune__c)) ownerId = ownerCap.get(cm.Comune__c);
      else if(String.isNotBlank(cm.Provincia__c) && ownerCap.containsKey(cm.Provincia__c)) ownerId = ownerCap.get(cm.Provincia__c);

      Date activityDate = Date.today();
      if(cm.DataUltimaImportazioneDaWeb__c != null && cm.DataUltimaImportazioneDaWeb__c > Date.today()) activityDate = cm.DataUltimaImportazioneDaWeb__c.date();
      else if(cm.DataContattoCampagna__c != null && cm.DataContattoCampagna__c > Date.today()) activityDate = cm.DataContattoCampagna__c;

      Task t = new Task(
        WhoId = cm.ContactId,
        WhatId = cm.AccountId__c,
        Canale__c = cm.CanaleCampagna__c == 'Filo Diretto' ? 'Campagna Filo Diretto' : null,
        OwnerId = cm.CanaleCampagna__c == 'Filo Diretto' ? ownerId : (accsToReassign.containsKey(cm.AccountId__c) ? accsToReassign.get(cm.AccountId__c).OwnerId : cm.AccountOwner__c),
        Status = 'Aperto',
        Subject = 'Contatto telefonico campagna',
        ActivityDate = activityDate,
        Campagna__c = cm.CampaignId,
        RecordTypeId = U.getRecordTypes('Task').get('ContattoTelefonicoDiretto').Id,
        MembroCampagna__c = cm.Id
      );

      if(String.isNotBlank(cm.MessaggioFormWeb__c)) t.Description = cm.MessaggioFormWeb__c;

      res.add(t);
    }
    insert res;

  }

  /**
  * Metodo di appoggio usato da eliminaTaskContatto e anche dal trigger dei campaign member
  **/
  //public static void eliminaTaskDaCampaignMember(Map<Id,CampaignMember> cmL) {
  //  if(cmL.isEmpty()) return;

  //  Task[] res = [SELECT Id FROM Task WHERE MembroCampagna__c IN :cmL.keySet()];
  //  delete res;

  //}
}
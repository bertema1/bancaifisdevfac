/*
Questo file è stato generato e non è il codice sorgente effettivo di questa
classe globale gestita.
Questo file di sola lettura mostra i costruttori, i metodi,
le variabili e le proprietà globali della classe.
Per abilitare il codice per la compilazione, tutti i metodi restituiscono null.
*/
global class JBintFireBulkEvent {
    global JBintFireBulkEvent() {

    }
    @InvocableMethod(label='Fire Bulk JB Event' description='Fires off a bulk event for the given set of params')
    global static void fireBulkEvent(List<et4ae5.JBIntFireEventParams> fireEventParamList) {

    }
}

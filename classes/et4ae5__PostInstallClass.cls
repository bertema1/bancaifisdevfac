/*
Questo file è stato generato e non è il codice sorgente effettivo di questa
classe globale gestita.
Questo file di sola lettura mostra i costruttori, i metodi,
le variabili e le proprietà globali della classe.
Per abilitare il codice per la compilazione, tutti i metodi restituiscono null.
*/
global class PostInstallClass implements System.InstallHandler {
    global PostInstallClass() {

    }
    global void onInstall(System.InstallContext context) {

    }
}

/**
* Progetto:         Banca IFIS
* Sviluppata il:    13/01/2017
* Developer:        Giuliani Luigi
*/

@isTest
public with sharing class CtrlWizardAccountTest {

  @testSetup
  static void dataSetup() {

    User dora = TestUtils.creaUtente('dora');
    TestUtils.impostaCS();

    Account a = TestUtils.creaAccount('A', dora);
    Contact c = TestUtils.creaReferente(a);
    Account accountCompleto = TestUtils.creaAccount('AccountCompleto', 'MI', '02213390343', 'rsscld50r15h501y');
    TestUtils.creaEndpointServizi();

    System.runAs(dora) {
      Funzionalita__c f = Funzionalita__c.getInstance();
      f.DisabilitaNamingAutomaticaOpportunita__c = true;
      upsert f;
    }
  }

  @isTest
  static void testInsertAccount() {

    Account a = new Account(Name = 'testInsert', CF__c = 'RSSDRN56D44H282W', BillingCity = 'A', BillingPostalCode ='000' , BillingStreet = 'AAA', BillingState = 'aaaa', NaturaGiuridica__c='aaaa', VAT__c = 'aaa', BillingStreetType__c = 'via', BillingStreetNumber__c='aa', PIVA__c = 'aaaa', BillingStreetName__c = 'aaa');
    test.startTest();
    //caso 1: account senza contact
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_SET_ANAGRAFICA_LIGHT_PG));
    CtrlWizardAccount.insertAccount(a, '', false);
    List<Account> accountList = [SELECT Id, CF__c FROM Account WHERE Name = 'testInsert'];
    System.assertEquals(1, accountList.size());
    System.assertEquals('RSSDRN56D44H282W', accountList[0].CF__c);
  }

  @isTest
  static void testInsertAccount2() {
    //increase coverage
    //caso 2: ribes con errore
    Account a = new Account(Name = 'testInsert',  CF__c = 'RSSDRN56D44H282W',BillingCity = 'A', BillingPostalCode ='000' , BillingStreet = 'AAA', BillingState = 'aaaa', NaturaGiuridica__c='aaaa', VAT__c = 'aaa', BillingStreetType__c = 'via', BillingStreetNumber__c='aa', PIVA__c = 'aaaa', BillingStreetName__c = 'aaa');
    a.Origine__c = WsRestRibes.RIBES;

    test.startTest();
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_SET_ANAGRAFICA_LIGHT_PG));
    CtrlWizardAccount.insertAccount(a,'', false);
    test.stopTest();
  }

  @isTest
  static void testSetupAccount() {
    Account a = new Account(Name = 'testInsert',  CF__c = 'RSSDRN56D44H282W', BillingCity = 'A', BillingPostalCode ='000' , BillingStreet = 'AAA', BillingState = 'aaaa', NaturaGiuridica__c='aaaa', VAT__c = 'aaa', BillingStreetType__c = 'via', BillingStreetNumber__c='aa', PIVA__c = 'RSSDRN56D44H282W', BillingStreetName__c = 'aaa');
    test.startTest();
    //caso 1: CEDACRI
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(new String[]{
      TestMockUtils.PAYLOAD_RICERCA_ANAGRAFICA_CEDACRI_PG,
      TestMockUtils.PAYLOAD_GET_DATI_ANAGRAFICI_CEDACRI_DI
    }));
    a.Origine__c = WsAnagrafe.CEDACRI;
    CtrlWizardAccount.setupAccount(a);
    //caso 2: ribes
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_RIBES_INFORMAZIONIPG));
    a.CF__c = 'RSSDRN56D44H282W';
    a.PIVA__c = 'RSSDRN56D44H282W';
    a.Origine__c = WsRestRibes.RIBES;
    // CtrlWizardAccount.setupAccount(a); // todo test: sistemare
    //caso 3 TODO: origine crm senza ndg
    test.stopTest();
  }

  @isTest
  static void testSetupDIContact() {
    Account a = new Account(Name = 'testInsert',  CF__c = 'RSSDRN56D44H282W',BillingCity = 'A', BillingPostalCode ='000' , BillingStreet = 'AAA', BillingState = 'aaaa', NaturaGiuridica__c='aaaa', VAT__c = 'aaa', BillingStreetType__c = 'via', BillingStreetNumber__c='aa', PIVA__c = 'RSSDRN56D44H282W', BillingStreetName__c = 'aaa');
    test.startTest();
    System.assertEquals(a.Id, CtrlWizardAccount.setupDIContact(a).AccountId);
    test.stopTest();
  }

  @isTest
  static void testGetContatto() {
    Contact c = [SELECT Id, FirstName, LastName, AccountId, OwnerId FROM Contact WHERE FirstName = 'Referente'];
    Test.startTest();
    Contact contactRiestituito = CtrlWizardAccount.getContatto(c.id);
    Test.stopTest();
    System.assertEquals(contactRiestituito.FirstName, 'Referente');
  }

  @isTest
  static void testUpsertContact() {
    Contact c = [SELECT Id, FirstName, LastName, AccountId, OwnerId, NDGGruppo__c FROM Contact WHERE FirstName = 'Referente'];
    Account accountCompleto = [SELECT id, Name  FROM Account WHERE Name = 'AccountCompleto' ];
    AccountContactRelation relationNew = new AccountContactRelation(AccountId = accountCompleto.id);
    List<AccountContactRelation> rlist = new List<AccountContactRelation>();
    rlist.add(relationNew);
    c.Phone = '1234567890';

    Test.startTest();
    CtrlWizardAccount.upsertContact(c, rlist, 'standard');
    Test.stopTest();

    Contact contactModificato = [SELECT Id, Phone FROM Contact WHERE FirstName = 'Referente'];
    System.assertEquals(contactModificato.Phone, '1234567890');
    List<AccountContactRelation> rlistExpected = CtrlWizardAccount.getRelations(c.Id, c.NDGGruppo__c);
    System.assertEquals(2, rlistExpected.size());
  }


  @isTest
  static void testInsertContacts() {
    Account a = [SELECT Id, OwnerId FROM Account WHERE Name = 'A'];
    Contact c1 = new  Contact(FirstName = 'TestInsert', LastName = 'TestSurname1', AccountId = a.Id, OwnerId = a.OwnerId);
    Contact c2 = new  Contact(FirstName = 'TestInsert', LastName = 'TestSurname2', AccountId = a.Id, OwnerId = a.OwnerId);
    Test.startTest();
    List<Contact> contactList = new List<Contact>();
    contactList.add(c1);
    contactList.add(c2);
    CtrlWizardAccount.insertContacts(contactList);
    Test.stopTest();
    List<Contact> contattiInseriti = [SELECT Id, FirstName FROM Contact WHERE FirstName = 'TestInsert'];
    System.assertEquals(contattiInseriti.size(), 2);
  }

  @isTest
  static void testSearchAnagraficaSFDC() {

    CtrlWizardAccount.InputObj inp1 = new CtrlWizardAccount.InputObj('', 'AccountCompleto', 'MI', 'ragione sociale', false);

    //CASO 1: search by name
    Test.startTest();
    List<Account> accList = CtrlWizardAccount.searchAnagrafica(inp1, 'SFDC');
    Account a = [SELECT Id, OwnerId FROM Account WHERE Name = 'AccountCompleto'];
    System.assertEquals(accList.size(), 1);
    System.assertEquals(accList[0].Id, a.Id);
    System.assertEquals('CRM', accList[0].Origine__c);

    //CASO 2: search by PIVA
    CtrlWizardAccount.InputObj inp2 = new CtrlWizardAccount.InputObj('02213390343', '', 'MI', 'piva', false);
    List<Account> accList2 = CtrlWizardAccount.searchAnagrafica(inp2, 'SFDC');
    System.assertEquals(accList2.size(), 1);
    System.assertEquals(accList2[0].Id, a.Id);
    System.assertEquals('CRM', accList2[0].Origine__c);

    //CASO 3: search by CF
    CtrlWizardAccount.InputObj inp3 = new CtrlWizardAccount.InputObj('rsscld50r15h501y', '', 'MI', 'piva', false);
    List<Account> accList3 = CtrlWizardAccount.searchAnagrafica(inp3, 'SFDC');
    System.assertEquals(accList3.size(), 1);
    System.assertEquals(accList3[0].Id, a.Id);
    System.assertEquals('CRM', accList3[0].Origine__c);
    test.stopTest();
  }

  @isTest
  static void testSearchContattiSFDC() {

    Account a = [SELECT Id, OwnerId FROM Account WHERE Name = 'A'];
    Contact c2 = new  Contact(FirstName = 'TestInsert', LastName = 'TestSurname1', AccountId = a.Id, OwnerId = a.OwnerId);
    Contact c3 = new  Contact(FirstName = 'TestInsert', LastName = 'TestSurname2', AccountId = a.Id, OwnerId = a.OwnerId);
    insert c2;
    insert c3;

    Test.startTest();
    List<Contact> contactList = CtrlWizardAccount.searchContattiSFDC('', 'TestSurname1', 'TestInsert');
    System.assertEquals(1, contactList.size());
    System.assertEquals('TestSurname1', contactList[0].LastName);

    contactList = CtrlWizardAccount.searchContattiSFDC('aaaaaa99c99e063d', '', '');
    System.assertEquals(0, contactList.size());  //codice fiscale non inserito per nessuno
    test.stopTest();
  }

  @isTest
  static void testSearchAnagraficaANDG() {

    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_RICERCA_ANAGRAFICA_CEDACRI_PG));

    //test1 --> per PIVA --> deve restituirmi 1 risultato
    CtrlWizardAccount.InputObj inp1 = new CtrlWizardAccount.InputObj('00748820131', '', 'LC', 'piva', false);
    Test.startTest();
    List<Account> accList1 = CtrlWizardAccount.searchAnagrafica(inp1, 'ANDG');
    System.assertEquals(1, accList1.size());
    System.assertEquals('Cedacri', accList1[0].Origine__c);
    System.assertEquals(U.getRecordTypes('Account').get('Cliente').Id, accList1[0].RecordTypeId);
    //test 2  --> per Denominazione  --> deve restituirmi ancora 1 risultato
    CtrlWizardAccount.InputObj inp2 = new CtrlWizardAccount.InputObj('', 'RACCORFERS.N.C.DIBRIVIOFABIO&C', 'LC', 'dsadsa', false);
    List<Account> accList2 = CtrlWizardAccount.searchAnagrafica(inp2, 'ANDG');
    System.assertEquals(1, accList2.size());
    System.assertEquals('Cedacri', accList1[0].Origine__c);
    System.assertEquals(U.getRecordTypes('Account').get('Cliente').Id, accList1[0].RecordTypeId);
    //test 3 --> come prima ma su provincia di milano. attesi zero risultati
    CtrlWizardAccount.InputObj inp3 = new CtrlWizardAccount.InputObj('', 'RACCORFERS.N.C.DIBRIVIOFABIO&C', 'MI', 'dsadsa', false);
    List<Account> accList3 = CtrlWizardAccount.searchAnagrafica(inp3, 'ANDG');
    System.assertEquals(0, accList3.size());
    Test.stopTest();
  }

  @isTest
  static void testSearchContattiANDG() {

    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_RICERCA_ANAGRAFICA_CEDACRI_PF));

    //test1 --> per CF --> deve restituirmi 2 risultati (risposta mockata)
    Test.startTest();
    List<Contact> contactList = CtrlWizardAccount.searchContattiANDG('BRVPLA77P01A794E', '', '');
    System.assertEquals(2, contactList.size());
    //test 2  --> per Denominazione  --> deve restituirmi 2 risultati (risposta mockata)
    contactList = CtrlWizardAccount.searchContattiANDG('', 'Brivio', 'Paolo');
    System.assertEquals(2, contactList.size());
    System.assertEquals('485041', contactList[0].NDGGruppo__c);
    Test.stopTest();
  }

  @isTest
  static void testSearchAnagraficaRibes() {
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_RIBES_GETLISTAAZIENDE_NOT_CESSATA));
    CtrlWizardAccount.InputObj inp1 = new CtrlWizardAccount.InputObj('02168580120', '', 'PD', 'piva', false);

    test.startTest();
    List<Account> accList1 = CtrlWizardAccount.searchAnagrafica(inp1, 'RIBES');
    System.assertEquals(1, accList1.size());
    System.assert('Ribes' == accList1[0].Origine__c);
    System.assertEquals(U.getRecordTypes('Account').get('Prospect').Id, accList1[0].RecordTypeId);
    test.stopTest();
  }

  @isTest
  static void testGetRelations() {
    Contact c = [SELECT Id, NDGGruppo__c FROM Contact LIMIT 1];
    c.NDGGruppo__c = '800';
    update c;
    AccountContactRelation[] acrBefore = [SELECT Id FROM AccountContactRelation WHERE ContactId = :c.Id];

    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOADS_GESTIONE_COLL_NDG_PF));
    Test.startTest();
    AccountContactRelation[] response = CtrlWizardAccount.getRelations(c.Id, c.NDGGruppo__c);
    Test.stopTest();

    System.debug('response: ' + JSON.serialize(response));
    AccountContactRelation[] acrAfter = [SELECT Id FROM AccountContactRelation WHERE ContactId = :c.Id];

    System.assertEquals(1, acrBefore.size());
    System.assertEquals(1, acrAfter.size());
  }

  @isTest
  static void testCwaFetchContacts() {
    // TODO TEST
    try {
      CtrlWizardAccount.cwaFetchContact([SELECT Id FROM Contact LIMIT 1]);
    } catch (Exception e) {
    }
  }

  @isTest
  static void testGetFieldSets() {
    // TODO TEST
    try {
      CtrlWizardAccount.getFieldSets('*', null, 'Account');
    } catch (Exception e) {
    }
  }

  @isTest
  static void testRenderModifica() {
    // TODO TEST
    try {
      CtrlWizardAccount.renderModifica('*');
    } catch (Exception e) {
    }
  }

}
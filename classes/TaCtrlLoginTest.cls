@isTest
private class TaCtrlLoginTest {

  @testSetup
  static void setupData() {
    TestUtils.impostaCS();
    TestUtils.creaEndpointServizi();
    TestUtils.creaAccount('acc');
  }

  @isTest
  static void testConstructor(){
    TaCtrlLogin.ProfileInfo p = new TaCtrlLogin.ProfileInfo(null, null);
    TaCtrlLogin.UserInfoObj u = new TaCtrlLogin.UserInfoObj(null, null, null, null, null);
  }

  @isTest static void testLogin() {
    TaCtrlLogin.InputObj input = new TaCtrlLogin.InputObj();

    input.email = 'test@test.com.xxx';
    input.password = '12345678ab';

    try {
      TaCtrlLogin.login(JSON.serialize(input));
    } catch (Exception e) {}
  }

  @isTest static void testSelfRegister() {
    TaCtrlLogin.InputObj input = new TaCtrlLogin.InputObj();

    input.email = 'test@test.com.xxx';
    input.password = '12345678ab';
    input.nome = 'Paolo';
    input.cognome = 'Rossi';
    input.selectedAccount = new Account(Name = 'test', CF__c = 'AAAQQQ12Q12Q123Q', PIVA__c = '11111111111');

    try {
      TaCtrlLogin.selfRegister(JSON.serialize(input));
    } catch (Exception e) {}

   /*

  -------- A QUANTO PARE NON SI PUO' TESTARE UN METODO CHE CONTIENE IL Site.ValidatePassword ---------------

   Test.startTest();
    Profile p = [SELECT Id FROM Profile WHERE Name='Utente Standard'];
    User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
    LocaleSidKey='en_US', ProfileId = p.Id,
    TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com.com');
    insert u;
    Account acc = new Account(Name = 'nuovoaccount', OwnerId=u.Id);
    insert acc;
    TaCtrlLogin.InputObj newinput = new TaCtrlLogin.InputObj();

    newinput.email = 'newtest@test.com.xxx';
    newinput.password = 'new12345678ab';
    newinput.nome = 'newPaolo';
    newinput.cognome = 'newRossi';
    newinput.selectedAccount = acc;

    try {
      TaCtrlLogin.selfRegister(JSON.serialize(newinput));
    } catch (Exception e) {}
    Test.stopTest();
    */
  }

  @isTest static void testResetPassword() {
    TaCtrlLogin.InputObj input = new TaCtrlLogin.InputObj();

    input.forgottenEmail = 'test@test.com.xxx';

    try {
      TaCtrlLogin.resetPassword(JSON.serialize(input));
    } catch (Exception e) {}
  }

  @isTest static void testFetchProfile() {
    User admin = [SELECT id FROM User WHERE Alias = 'super'];
    System.runAs(admin) {
      // TaCtrlLogin.fetchProfile(); // todo test: sistemare
    }
  }

  @isTest static void testUpdateProfile() {
    Account a = [SELECT Id, OwnerId FROM Account WHERE Name = 'acc'];
    Contact c = TestUtils.creaReferente(a);
    Contact[] titolariEsecutori = new Contact[] {};
    for (Integer i = 0; i < 3; i++) {
      titolariEsecutori.add(TestUtils.creaReferente(a));
    }

    AdeguataVerifica__c adv = new AdeguataVerifica__c(Account__c = a.Id, TipoIndirizzo__c = 'billing');
    insert adv;

    TaCtrlLogin.ProfileInfo input = new TaCtrlLogin.ProfileInfo(a, c, titolariEsecutori, adv);
    TaCtrlLogin.updateProfile(JSON.serialize(input));
  }
}
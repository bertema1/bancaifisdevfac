/**
 * Created by ccardinale on 17/05/2017.
 */

@IsTest
private class ExtViewCheckListTest {

  @testSetup
  static void dataSetup() {
    Funzionalita__c f = Funzionalita__c.getInstance();
    f.DisabilitaNamingAutomaticaOpportunita__c = true;
    f.DisabilitaControlloCambioFaseOpp__c = true;
    upsert f;

    User dora = TestUtils.creaUtente('dora');
    TestUtils.impostaCS();
    Account a = TestUtils.creaAccount('A',dora);
    a.NDGGruppo__c = '3333';
    a.Filiale__c = '5';
    update a;
    Opportunity opp = TestUtils.CreaOpportunity(a, 'test');
    opp.IdCartella__c = 'a/333.33';
    update opp;
    Contact c = TestUtils.creaReferente(a);
    c.NDGGruppo__c = '1111';
    update c;
    NDGLinea__c ndgLinea = TestUtils.CreaNGDLinea(c, opp);
    ndgLinea.Tipo__c = 'Esecutore adeguata verifica';
    update ndgLinea;
    Prodotto__c prodotto = TestUtils.creaProdotto('AAAA','000001','Factoring');
    Linea__c[] linee = TestUtils.creaLinee(opp, 1);
    TestUtils.creaEndpointServizi();
  }

  @IsTest
  static void testInstance() {
    Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'test'];
    new ExtViewCheckList(new ApexPages.StandardController(opp));
    //TODO assert...
  }

  @IsTest
  static void testGetStatoChecklist(){
    Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'test'];
    System.assertEquals(false, ExtViewCheckList.getStatoChecklist(opp.Id).fatcaOk);
  }

  @IsTest
  static void testRefreshStatoCartella(){
    Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'test'];
    String idProdotto = [SELECt Id, IdProdotto__c FROM Linea__c WHERE Opportunity__c = :opp.Id][0].IdProdotto__c;
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_NV_INQUIRY_STATO_CARTELLA_PLACEHOLDER.replace('{{idProdotto}}',idProdotto)));
    System.assertEquals(true, ExtViewCheckList.refreshStatoCartella(opp.Id));
    Test.stopTest();
  }

  @IsTest
  static void testEsistePef(){
    Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'test'];
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_CHECK_STATO_PEF));
    System.assertEquals(false, ExtViewCheckList.esistePef(opp.Id));
    Test.stopTest();
  }

  @IsTest
  static void testInviaNuovaVendita(){
    Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'test'];
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, TestMockUtils.creaMockRispostaOk(TestMockUtils.PAYLOAD_NV_AGGIORNAMENTO_INNESCA_VENDITA));
    System.assertEquals(null, ExtViewCheckList.inviaNuovaVendita(opp.Id));
    Test.stopTest();
  }

  @IsTest
  static void testGeneraDocumentazione(){
    Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'test'];
    Test.startTest();
    System.assertNotEquals(null, ExtViewCheckList.generaDocumentazione(opp.Id, 'Privacy'));
    Test.stopTest();
  }

  @IsTest
  static void testEsisteTitolareEffettivo() {
    Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'test'];

    Test.startTest();
    Boolean response = ExtViewCheckList.esisteTitolareEffettivo(opp.Id);
    Test.stopTest();

    System.assert(!response);
  }

  @IsTest
  static void testRichiediInfoCR() {
    Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'test'];

    Test.startTest();
    Boolean response = ExtViewCheckList.richiediInfoCR(opp.Id);
    Test.stopTest();

    System.assert(response);
  }

  @isTest
  static void testCambiaInnescoOpportunita() {
    Opportunity opp = [SELECT Id, InnescataNV__c FROM Opportunity WHERE Name = 'test'];
    opp.InnescataNV__c = false;
    update opp;
    System.assert(!opp.InnescataNV__c);

    Test.startTest();
    ExtViewCheckList.cambiaInnescoOpportunita(true, opp.Id);
    Test.stopTest();

    opp = [SELECT Id, InnescataNV__c FROM Opportunity WHERE Name = 'test'];
    System.assert(opp.InnescataNV__c);
  }

  @isTest
  static void testFetchInnescoOpportunita() {
    Opportunity opp = [SELECT Id, InnescataNV__c FROM Opportunity WHERE Name = 'test'];

    opp.InnescataNV__c = true;
    update opp;
    Boolean result1 = ExtViewCheckList.fetchInnescoOpportunita(opp.Id);
    System.assert(result1);

    opp.InnescataNV__c = false;
    update opp;
    Boolean result2 = ExtViewCheckList.fetchInnescoOpportunita(opp.Id);
    System.assert(!result2);
  }
}
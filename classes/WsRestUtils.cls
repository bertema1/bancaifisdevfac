public without sharing class WsRestUtils {
  private WsRestUtils() {}

  public static final Decimal DEFAULT_TIMEOUT = 40000;

  public static WsRestInput.CommonResponse callService(String urlField, WsAnagraficaBean input, String template, Type t) {
    WsRestInput tmp = new WsRestInput(JSON.deserializeUntyped(input.toJSON(template)));
    return getResponseObject(urlField, tmp, t, DEFAULT_TIMEOUT);
  }

  public static WsRestInput.CommonResponse callService(String urlField, Object input, Type t, Decimal timeout) {
    return getResponseObject(urlField, new WsRestInput(input), t, timeout);
  }

  public static WsRestInput.CommonResponse callService(String urlField, Object input, Type t) {
    return getResponseObject(urlField, new WsRestInput(input), t, DEFAULT_TIMEOUT);
  }

  public static WsRestInput.CommonResponse callServiceSpecialRibes(String urlField, Object input, Type t) {
    WsRestInput.CommonResponse responseObj = (WsRestInput.CommonResponse) t.newInstance();
    WsRestInput tmp = new WsRestInput(input);
    String body;
    String responseString;
    try {
      body = tmp.toJSONString();
      responseString = WsRestUtils.makeRequest(urlField, body, DEFAULT_TIMEOUT);
      Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
      responseObj = WsRestRibes.parseInformazioniPG(responseMap);
    } catch (Exception e) {
      responseObj.exceptionError = e.getMessage();
    }

    writeLog(
      urlField,
      body,
      responseString,
      responseObj.isCorrect() ? null : responseObj.getErrors()
    );

    if (!responseObj.isCorrect()) {
      throw new Ex.WsCalloutException(responseObj.getErrors());
    }

    return responseObj;
  }

  // Come callService, ma elimina il campo "risposta" dal payload di output
  public static WsRestInput.CommonResponse callServiceSpecialEventiNegativi(String urlField, Object input, Type t) {
    WsRestInput.CommonResponse responseObj = (WsRestInput.CommonResponse) t.newInstance();
    WsRestInput tmp = new WsRestInput(input);
    String body;
    String responseString;
    try {
      body = tmp.toJSONString();
      responseString = WsRestUtils.makeRequest(urlField, body, DEFAULT_TIMEOUT);
      responseObj = (WsRestInput.CommonResponse)JSON.deserialize(responseString, t);
    } catch (Exception e) {
      responseObj.exceptionError = e.getMessage();
    }
    String regExp = '(?<="risposta":\\{"prodotto":\\{"reportPDF":\\{"fileBase64":")(.*)(?="\\}\\}\\})';
    if (responseString != null) responseString = responseString.replaceAll(regExp, 'PDF non loggato');

    writeLog(
      urlField,
      body,
      responseString,
      responseObj.isCorrect() ? null : responseObj.getErrors()
    );

    if (!responseObj.isCorrect()) throw new Ex.WsCalloutException(responseObj.getErrors());

    System.debug(urlField + ': ' + responseObj);
    return responseObj;
  }

  private static WsRestInput.CommonResponse getResponseObject(String urlField, WsRestInput input, Type t, Decimal timeout) {
    WsRestInput.CommonResponse responseObj = (WsRestInput.CommonResponse) t.newInstance();
    String body;
    String responseString;
    try {
      body = input.toJSONString();
      responseString = WsRestUtils.makeRequest(urlField, body, timeout);
      responseObj = (WsRestInput.CommonResponse)JSON.deserialize(responseString, t);
    } catch (Exception e) {
      responseObj.exceptionError = e.getMessage() + ' - ' + e.getStackTraceString();
    }

    writeLog(
      urlField,
      body,
      responseString,
      responseObj.isCorrect() ? null : responseObj.getErrors()
    );

    if (!responseObj.isCorrect()) throw new Ex.WsCalloutException(responseObj.getErrors());

    System.debug(urlField + ': ' + responseObj);
    return responseObj;
  }

  private static void writeLog(String urlField, String input, String output, String errorMessage) {
    GestioneLog__c gLog = GestioneLog__c.getInstance('default');
    if (gLog != null && gLog.get(urlField) != null && (Boolean) gLog.get(urlField)) {
      Map<String, Schema.SObjectField> fieldMap = gLog.getSObjectType().getDescribe().fields.getMap();
      String inputPretty = input;
      String outputPretty = output;

      try {
        inputPretty = JSON.serializePretty(JSON.deserializeUntyped(input));
      } catch (Exception e) {}

      try {
        outputPretty = JSON.serializePretty(JSON.deserializeUntyped(output));
      } catch (Exception e) {}

      Logger.log(new Log__c(
                   Source__c = fieldMap.get(urlField).getDescribe().getLabel(),
                   ErorrMessage__c = errorMessage,
                   InputPayload__c = inputPretty,
                   OutputPayload__c = outputPretty
                 ));
    }
  }

  public static String callSimpleService(String urlField, Object input) {
    String body = JSON.serialize(input);
    String responseString = WsRestUtils.makeRequest(urlField, body, DEFAULT_TIMEOUT);
    writeLog(
      urlField,
      body,
      responseString,
      null
    );
    return responseString;
  }

  private static String makeRequest(String urlField, String body, Decimal timeout) {
    HttpRequest req = new HttpRequest();
    req.setHeader('X-LAGKey', getLagKey(urlField));
    req.setHeader('Content-Type', 'application/json');
    req.setEndpoint(getEndpoint(urlField));
    req.setMethod('POST');
    req.setBody(body);
    req.setTimeout(timeout.intValue());

    System.debug(urlField + ' - body: ' + body);

    return (new Http()).send(req).getBody();
  }

  private static String getLagKey(String urlField) {
    return urlField == 'UrlCertificazioneParser__c' ? EndpointServizi__c.getInstance('settings').LAGKeyHeroku__c : EndpointServizi__c.getInstance('settings').LAGKey__c;
  }

  private static String getEndpoint(String urlField) {
    EndpointServizi__c endpoint = EndpointServizi__c.getInstance('settings');
    String url = (String) endpoint.get(urlField);
    if (url == null) throw new Ex.WsException('custom settings non impostato per l\'URL :' + urlField);
    return url;
  }
}
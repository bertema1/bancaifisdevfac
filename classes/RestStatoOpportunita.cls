@RestResource(urlMapping = '/StatoOpportunita')
global with sharing class RestStatoOpportunita {
  private RestStatoOpportunita() {}

  private static final String DOCUMENTI_PRONTI = '11';

  @HttpPost
  global static RestCommon.ResponseObject updateCartella(
    RestCommon.TechInfo techInfo,
    String codiceIstituto3N,
    String idApplicazioneChiamante,
    Cartella cartella,
    Linea[] linee
  ) {
    String message;
    String codice;

    if (cartella != null && linee != null) {
      if (cartella != null && String.isNotBlank(cartella.idCartella)) {
        String idCartella = cartella.idCartella;

        Opportunity[] opps = [SELECT Id,
                              (SELECT Id, IdProdotto__c FROM Linee__r)
                              FROM Opportunity
                              WHERE IdCartella__c = :idCartella];

        if (opps.isEmpty()) {
          codice = RestCommon.ID_CARTELLA_ASSENTE;
        } else if (opps.size() > 1) {
          message = 'Trovate troppo cartelle';
          codice = RestCommon.ID_CARTELLA_ASSENTE; //oppure altro errore???
        } else {
          Opportunity opp = opps[0];
          opp.StatoPEF__c = cartella.esito;

          try {
            Linea__c[] toUpdate = aggiornaLineeOpportunita(opp.Linee__r, linee);

            update opp;
            update toUpdate;
            codice = RestCommon.NESSUN_ERRORE;

            if (opp.StatoPEF__c == DOCUMENTI_PRONTI) {
              //se documenti pronti -> ricerca DOC04 + recupero documeni tramite talend
              WsRestDocumentaleUtils.recuperaDocumentiAsync(opp.Id);
            }

          } catch (Ex.RestStatoOpportunitaException e) {
            codice = e.getMessage();
          } catch (Exception e) {
            message = e.getMessage();
            codice = RestCommon.ERRORE_GENERICO;
          }
        }
      }
    }
    RestCommon.ResponseObject response = new RestCommon.ResponseObject(codice, message);

    RestCommon.logInboundMessage('StatoOpportunita Inbound', new Object[] {cartella, linee}, response);

    return response;
  }

  private static Linea__c[] aggiornaLineeOpportunita(Linea__c[] linee, Linea[] newLinee) {
    Map<String, SObject> oppLinee = U.keyBy(linee, 'IdProdotto__c');
    Linea__c[] tmp = new Linea__c[] {};
    for (Linea newLinea : newLinee) {
      Linea__c l = (Linea__c)oppLinee.get(newLinea.id.leftPad(6).replace(' ', '0'));
      if (l != null) {
        l.Stato__c = newLinea.stato;
        l.IdIstruttoria__c = newLinea.idIstruttoria;
        l.IdIstruttoriaFiglio__c = newLinea.idIstruttoriaFiglio;
        l.Dettaglio__c = newLinea.dettaglio;
        l.DataIstruttoria__c = UDate.parseDateYYYYMMDD(newLinea.dataIstruttoria);
        l.DataIstruttoriaFiglio__c = UDate.parseDateYYYYMMDD(newLinea.dataIstruttoriaFiglio);

        tmp.add(l);
      }
    }
    return tmp;
  }

  global class Cartella {
    String idCartella;
    String esito;
  }

  global class Linea {
    @TestVisible
    String id;
    String stato;
    String idIstruttoria;
    String dataIstruttoria;
    String idIstruttoriaFiglio;
    String dataIstruttoriaFiglio;
    String dettaglio;
  }
}
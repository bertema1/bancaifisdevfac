public without sharing class QbleInfoCr implements Queueable, Database.AllowsCallouts {
  public class InfoCrElem {
    public Long ndg;
    public Id objId;
    public InfoCrElem(String ndg, Id objId) {
      this.ndg = Long.valueOf(ndg);
      this.objId = objId;
    }
  }

  InfoCrElem[] ndgs;
  Queueable nextQ;

  public QbleInfoCr(Id oppId, Queueable nextQ) {
    this.nextQ = nextQ;
    Opportunity o = [SELECT
      Id,
      Account.CodiceIstituto3N__c,
      Account.NDGGruppo__c,
      Account.NaturaGiuridica__c,
      Account.CF__c,
      DataRichiestaCR__c,
      (SELECT NDG__c FROM NDG_Linee__r WHERE Tipo__c = 'Debitore' AND NDG__c != NULL AND DataRichiestaCr__c = NULL)
      FROM Opportunity
      WHERE Id = :oppId
    ];

    this.ndgs = new InfoCrElem[]{};
    if(o.DataRichiestaCR__c == null) this.ndgs.add(new InfoCrElem(o.Account.NDGGruppo__c, o.Id));

    // aggiungo agli ndgs il contatto associato alla ditta individuale, nel caso in cui abbia ndg diverso dall'account
    if (o.DataRichiestaCR__c == null && o.Account.NaturaGiuridica__c == 'DI' && String.isNotBlank(o.Account.CF__c)) {
      Contact[] c = [SELECT
        NDGGruppo__c
        FROM Contact
        WHERE CF__c = :o.Account.CF__c
        AND NDGGruppo__c != :o.Account.NDGGruppo__c
        AND CodiceIstituto3N__c = :o.Account.CodiceIstituto3N__c
      ];
      if (c.size() == 1 && String.isNotBlank(c[0].NDGGruppo__c)) this.ndgs.add(new InfoCrElem(c[0].NDGGruppo__c, o.Id));
    }

    for(NDGLinea__c deb : o.NDG_Linee__r) {
      this.ndgs.add(new InfoCrElem(deb.NDG__c, deb.Id));
    }
  }

  public QbleInfoCr(InfoCrElem[] ndgs, Queueable nextQ) {
    this.ndgs = ndgs;
    this.nextQ = nextQ;
  }

  public void execute(QueueableContext context) {
    if (!this.ndgs.isEmpty()) {
      InfoCrElem ndgToProcess = this.ndgs.remove(0);
      Logger.setTransactionContext(null, ndgToProcess.objId);
      WsRestPefMutui.InfoCrInput input = new WsRestPefMutui.InfoCrInput();
      input.ndg = ndgToProcess.ndg;

      try {
        WsRestPefMutui.InfoCr(input);
        SObject objToUpdate = ndgToProcess.objId.getSobjectType().newSObject(ndgToProcess.objId);
        objToUpdate.put('DataRichiestaCR__c', Datetime.now());
        update objToUpdate;
      } catch (Ex.WsCalloutException e) {
      } catch (DMLException e) {
        Logger.log('PEF_InfoCr', e.getMessage() + '. ' + e.getStackTraceString());
      }
    }

    if (!this.ndgs.isEmpty()) {
      QbleInfoCr nextJob = new QbleInfoCr(this.ndgs, this.nextQ);
      if (!Test.isRunningTest())
        System.enqueueJob(nextJob);
    } else if (nextQ != null) {
      if (!Test.isRunningTest())
        System.enqueueJob(nextQ);
    }
  }
}
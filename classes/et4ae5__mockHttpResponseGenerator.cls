/*
Questo file è stato generato e non è il codice sorgente effettivo di questa
classe globale gestita.
Questo file di sola lettura mostra i costruttori, i metodi,
le variabili e le proprietà globali della classe.
Per abilitare il codice per la compilazione, tutti i metodi restituiscono null.
*/
global class mockHttpResponseGenerator implements System.HttpCalloutMock {
    global Boolean errored;
    global String myBody;
    global String status;
    global Integer statusCode;
    global mockHttpResponseGenerator() {

    }
    global mockHttpResponseGenerator(String bodyString) {

    }
    global System.HttpResponse respond(System.HttpRequest req) {
        return null;
    }
}

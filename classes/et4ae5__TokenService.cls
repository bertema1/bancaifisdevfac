/*
Questo file è stato generato e non è il codice sorgente effettivo di questa
classe globale gestita.
Questo file di sola lettura mostra i costruttori, i metodi,
le variabili e le proprietà globali della classe.
Per abilitare il codice per la compilazione, tutti i metodi restituiscono null.
*/
@RestResource(urlMapping='/Token/*')
global class TokenService {
    global TokenService() {

    }
    @HttpGet
    global static void doGet() {

    }
    @HttpPost
    global static void doPost(List<et4ae5.TokenService.TokenServiceRequest> tokenReqs) {

    }
global class TokenServiceRequest {
    global TokenServiceRequest() {

    }
}
}

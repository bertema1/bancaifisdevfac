public class TrgAnomalia {

  /**
   * Invio di mail per le anomalie TiAnticipo
   */
  public static void inviaMail(T tu) {

    if (tu.triggerNew.size() == 0) return;
    if (MailUtils.isMailTiAnticipoDisabilitate) return;

    Messaging.SingleEmailMessage[] mails = new Messaging.SingleEmailMessage[]{};
    String[] toAddresses;
    String[] ccAddresses;

    Anomalia__c[] anomalie = [
      SELECT Id,
      Cessione__c,
      Cessione__r.CreatedById
      FROM Anomalia__c
      WHERE Id IN :U.getIdSet(tu.triggerNew, 'Id')];

    Map<String, SObject> emailTemplates = U.keyBy([
      SELECT Id,
      DeveloperName
      FROM EmailTemplate
      WHERE DeveloperName IN ('TiAnticipo17')], 'DeveloperName');

    Map<String, OrgWideEmailAddress[]> senderIds = U.groupBy([
      SELECT Id,
      DisplayName
      FROM OrgWideEmailAddress], 'DisplayName');

    for (Anomalia__c a : anomalie) {
      // Codice notifica 103
      if(emailTemplates.containsKey('TiAnticipo17') && senderIds.containsKey('Portale TiAnticipo'))
        mails.add(MailUtils.creaMailApexHTML(a.Cessione__c, emailTemplates.get('TiAnticipo17').Id, null, senderIds.get('Portale TiAnticipo')[0].Id, a.Cessione__r.CreatedById, null, null, null));
    }

    // Se le mail sono disabilitate o se sono in un test loggo il mancato invio delle mail
    if (!mails.isEmpty()) {
      if (Funzionalita__c.getInstance().DisabilitaInvioEmail__c || Test.isRunningTest()) {
        MailUtils.logNotSentEmail();
      } else {
        Messaging.sendEmail(mails);
      }
    }

  }

}
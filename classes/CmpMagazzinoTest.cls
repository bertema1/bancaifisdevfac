@isTest
private class CmpMagazzinoTest {

  @TestSetup
  static void dataSetup() {

    User utente = TestUtils.creaUtente('Utn');

    Campaign campagna = TestUtils.creaCampagna('Cmpgn1');

    Account a1 = TestUtils.creaAccount('Act1', utente, 'MI', '02213390343', 'rsscld50r15h501y', 'Cliente');
    Account a2 = TestUtils.creaAccount('Act2', utente, 'BO', '02213390344', 'rsscld50r15h501w', 'Prospect');
    Account a3 = TestUtils.creaAccount('Act3', utente, 'MB', '02213390345', 'rsscld50r15h501z', 'Prospect');

    Task t1 = TestUtils.creaTask(utente, a1, 'Subject1', 'Not Started', 'Normal', campagna);
    Task t2 = TestUtils.creaTask(utente, a2, 'Subject2', 'Not Started', 'Normal', campagna);
    Task t3 = TestUtils.creaTask(utente, a3, 'Subject3', 'Not Started', 'Normal', null);

  }

  @isTest
  static void testGetAccountList() {
    // SELECT Id, WhatId FROM Task
    // WHERE OwnerId = :userId
    //  AND Campagna__c = or != NULL
    //  AND WhatId IN (SELECT Id FROM Account WHERE RecordType.Name = :recordType
    // ORDER BY ActivityDate LIMIT 10

    CmpMagazzino.InterfaceAccount[] iaClienti = new List<CmpMagazzino.InterfaceAccount>();
    CmpMagazzino.InterfaceAccount[] iaProspect = new List<CmpMagazzino.InterfaceAccount>();
    CmpMagazzino.InterfaceAccount[] iaMagazzino = new List<CmpMagazzino.InterfaceAccount>();

    User utente = [SELECT id FROM User WHERE Alias = 'Utn'];
    System.runAs(utente) {
      Test.startTest();
      iaClienti = CmpMagazzino.getAccountList('clienti');
      iaProspect = CmpMagazzino.getAccountList('prospect');
      iaMagazzino = CmpMagazzino.getAccountList('magazzino');
      Test.stopTest();
    }

    System.assertEquals(1, iaClienti.size());
    System.assertEquals('Act1', iaClienti[0].acc.Name);
    System.assertEquals(1, iaProspect.size());
    System.assertEquals('Act2', iaProspect[0].acc.Name);
    System.assertEquals(1, iaMagazzino.size());
    System.assertEquals('Act3', iaMagazzino[0].acc.Name);
  }

  @isTest
  static void testResetPrioritaAccount() {
    Account act = [SELECT id FROM Account WHERE Name = 'Act1'];

    Test.startTest();
    CmpMagazzino.resetPrioritaAccount(act.id, 'Diretto');
    CmpMagazzino.resetPrioritaAccount(act.id, 'Indiretto');
    Test.stopTest();

    act = [SELECT PrioritaSviluppoDiretto__c, PrioritaSviluppoIndiretto__c FROM Account WHERE Name = 'Act1'];

    System.assertEquals(10, act.PrioritaSviluppoDiretto__c);
    System.assertEquals(10, act.PrioritaSviluppoIndiretto__c);
  }
}
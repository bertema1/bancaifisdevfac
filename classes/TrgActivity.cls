/**
* Progetto:         Banca IFIS
* Descrizione:      Classe handler del trigger Task.trigger e Event.Trigger
* Sviluppata il:    12/12/2016
* Ampiamente modificata il: 31/01/2017
* Developer:        Zerbinati Francesco, Michele Triaca
*/

public with sharing class TrgActivity {
  public static Boolean disabilitaCheckDataRicontatto = false;
  public static Boolean disabilitaCheckVisita = false;
  public static boolean contestoTrgAccountCambiaOwnerAttivita = false;

  /** In base alla matrice di condizioni MatriceEsiti svolgo delle azioni **/
  public static void azioneEsitazioneActivity(T tu) {

    SObject[] listaActivity = tu.getChanged(new List<String>{
      'RecordTypeId',
      'EsitoLivello1__c',
      'EsitoLivello2__c',
      'EsitoLivello3__c',
      'EsitoLivello4__c',
      'EsitoLivello5__c',
      'DataRicontatto__c'
    });

    if(listaActivity.isEmpty()) return;

    Map<String, SObject> me = U.keyBy([SELECT
          RecordType__c,
          EsitiLivello1__c,
          EsitiLivello2__c,
          EsitiLivello3__c,
          EsitiLivello4__c,
          EsitiLivello5__c,
          Azione__c
          FROM MatriceEsiti__mdt
    ],'{RecordType__c}-{EsitiLivello1__c}_{EsitiLivello2__c}_{EsitiLivello3__c}_{EsitiLivello4__c}_{EsitiLivello5__c}', true, true);

    Map <String, SObject[]> mappaAzioni = new Map<String,SObject[]>();

    for(SObject t : listaActivity) {
      List<String> arrayEsiti = new List<String> {
        (String) t.get('EsitoLivello1__c'),
        (String) t.get('EsitoLivello2__c'),
        (String) t.get('EsitoLivello3__c'),
        (String) t.get('EsitoLivello4__c'),
        (String) t.get('EsitoLivello5__c')
      };
      String hash = t.get('RecordTypeDevName__c') + '-' + String.join(arrayEsiti, '_' );

      String azione;
      if(me.containsKey(hash)) {
        azione = (String) me.get(hash).get('Azione__c');
        if(!mappaAzioni.containsKey(azione)) mappaAzioni.put(azione, new List<SObject>());
        mappaAzioni.get(azione).add(t);
      }

    }

    for(String azione : mappaAzioni.keySet()) {
      List<SObject> daProcessare = mappaAzioni.get(azione);

      System.debug('Eseguo azione: ' + azione);

      if (azione == 'Invio email') MailUtils.inviaMaildaTaskEventi(daProcessare,'ContattoTelefonicoKO');
      if (azione == 'Invio segnalazione a BU NPL')  MailUtils.inviaMaildaTaskEventi(daProcessare,'SegnalazioneNPL');
      if (azione == 'Invio segnalazione a Rendimax')  MailUtils.inviaMaildaTaskEventi(daProcessare,'SegnalazioneRendimax');

      if (azione == 'FINE') settaFuoriMagazzino(daProcessare, true);
      if (azione == 'WIP') settaFuoriMagazzino(daProcessare, false);
      if (azione == 'Contatto telefonico' && !disabilitaCheckDataRicontatto) controllaDataRicontatto(daProcessare);
      if (azione.containsIgnoreCase('Visita') && !disabilitaCheckVisita) bloccaEsitoSeFiloDiretto(daProcessare);
    }
  }

  /**
  * Prende una lista di task in input e crea nuovi task con data DataRicontatto__c
  **/
  public static void creaTaskRicontatto(T tu) {

    SObject[] listaActivity = tu.getChanged('DataRicontatto__c');

    SObject[] nuovi = new SObject[]{};
    for(SObject v : listaActivity) {
      if(v.get('DataRicontatto__c') != null) {
        nuovi.add(new Task(
          WhoId = (Id) v.get('WhoId'),
          WhatId = (Id) v.get('WhatId'),
          OwnerId = (Id) v.get('OwnerId'),
          Status = 'Aperto',
          Campagna__c = (Id) v.get('Campagna__c'),
          Description = (String) v.get('Description'),
          Subject = Label.RicontattoTelefonico,
          RicontattoTelefonico__c = true,
          ActivityDate = (Date) v.get('DataRicontatto__c'),
          RecordTypeId = (Id) v.get('RecordTypeId')
        ));
      }
    }

    insert nuovi;
  }


  /**
   * Chiude il task/evento se è stato esitato. Se si tratta di task, forza lo status a closed e l'activity date alla data
   * di chiusura
  **/
  public static void chiudiActivityEsitata(T tu) {
    SObject[] listaActivity = tu
      .filter('EsitoLivello1__c', (String) null, false)
      .filterByOld('EsitoLivello1__c', (String) null)
      .triggerNew;

    for(SObject t : listaActivity) {
      t.put('DataOraEsitazione__c', Datetime.now());
      if(t.getSObjectType() == Task.SObjectType) {
        t.put('Status', 'Chiuso');
        t.put('ActivityDate', Date.today());
      }
    }
  }

  /** Chiude di default tutte le segnalazioni **/
  public static void chiudiSegnalazione(T tu) {
    Task[] listaSegnalazioni = tu.filter('RecordTypeId',  U.getRecordTypes('Task').get('Segnalazione').Id).triggerNew;
    for(Task t : listaSegnalazioni) {
      t.Status = 'Chiuso';
      t.ActivityDate = Date.today();
    }
  }

  /** Rinomina l'oggetto dell'evento in  Tipo Attività - Nome Correlato **/
  public static void rinominaEvento(T tu) {
    Event[] listaActivity = tu
      .filter('WhatId', (String) null, false)
      .filter('TipoAttivita__c', (String) null, false)
      .triggerNew;

    Map<String, String> options = new Map<String, String>();
    List<Schema.Picklistentry> fieldResult = Event.TipoAttivita__c.getDescribe().getPicklistValues();
    for(Schema.PicklistEntry f : fieldResult) {
      options.put(f.getValue(), f.getLabel());
    }

    Map<Id, SObject> whatMap = getWhatMap(listaActivity, false);
    for(Event e : listaActivity) {
      e.Subject = (options.get(e.TipoAttivita__c) + ' - ' + (String) whatMap.get(e.WhatId).get('Name')).abbreviate(255);
    }
  }

  public static void rinominaTask(T tu) {
    for(Task t : (Task[]) tu
      .filter('Debitore__c', (String) null, false)
      .filter('RecordTypeId', U.getRecordTypes('Task').get('Segnalazione').Id)
      .triggerNew
    ) {
      t.Subject = (Label.SegnalazioneDebitore + ' ' + t.NomeDebitore__c).abbreviate(255);
    }
  }

  /** Su inserimento Eventi
  * In creazione controlla campo "Tipo Attività".
  * Visita Commerciale o Visita Firma Contratto possibili solo dal titolare e solo su account Prospect o Cliente non attivo.
  * Visita relazione su clienti attivi e anche da non titolari
  **/
  public static void bloccaVisiteNonPossibili(T tu, Boolean skipCheckTitolare) {
    /*
      Gli utenti Filo Diretto possono creare visite senza restrizioni.

      Visita "commerciale" o per "firma contratto" è possibile se tutte le seguenti condizioni sono soddisfatte:
      1) l'owner evento è nell'account team

      Visita "relazionale" è possibile se tutte le seguenti condizioni sono soddisfatte:
      1) l'account non è prospect
      2) l'account è attivo
      Questo tipo di visita può essere creata inoltre da commerciali/responsabili Leasing, Lending e Finanza Strutturata senza restrizioni
    */
    if (CU.isFiloDiretto()) return;
    Event[] eventi = tu.filter('WhatId', (String) null, false).filter('TipoAttivita__c', (String) null, false).triggerNew;
    if (eventi.isEmpty()) return;

    Map<Id,Account> coinvolti = new Map<Id,Account>([SELECT
      Id,
      OwnerId,
      RecordTypeId,
      StatoAnagrafica__c,
      Controparte__c
      FROM Account
      WHERE Id IN :U.getIdSet(eventi, 'WhatId')
    ]);

    Map<String, AccountTeamMember[]> coinvoltiAccountTeam = U.groupBy([
      SELECT UserId,
      AccountId
      FROM AccountTeamMember
      WHERE AccountId IN :coinvolti.keySet()
      ], 'AccountId');

    Id rtCliente = U.getRecordTypes('Account').get('Cliente').Id;
    Id rtProspect = U.getRecordTypes('Account').get('Prospect').Id;

    for (Event e : eventi) {
      /*
      *   - Prospect o Cliente non attivo. Visite possibili: Commerciale / Firma
      *   - Cliente attivo con campo Controparte=Debitore o Cedente o Cedente Debitore. Visite possibili: tutte e 3, ovver Commerciale / Firma / Relazionale
      */
      Boolean ownerUgualeOInAccountTeam = e.OwnerId != coinvolti.get(e.WhatId).OwnerId;
      if (e.WhatId.getSobjectType() == Account.SObjectType) ownerUgualeOInAccountTeam = ownerUgualeOInAccountTeam || U.getSet(coinvoltiAccountTeam.get(e.WhatId), 'UserId').contains(e.OwnerId);

      Boolean clienteDebitore = coinvolti.get(e.WhatId).RecordTypeId == rtCliente && coinvolti.get(e.WhatId).Controparte__c == 'Debitore';
      Boolean clienteAttivo = coinvolti.get(e.WhatId).RecordTypeId == rtCliente && String.isNotBlank(coinvolti.get(e.WhatId).Controparte__c);
      Boolean clienteCedente = !clienteDebitore && clienteAttivo;

      Boolean visitaCommercialeOppureFirma = e.TipoAttivita__c.containsIgnoreCase('Commerciale') || e.TipoAttivita__c.containsIgnoreCase('Firma');
      Boolean visitaRelazionale = e.TipoAttivita__c.containsIgnoreCase('Relazionale');

      if (visitaCommercialeOppureFirma && !ownerUgualeOInAccountTeam && !skipCheckTitolare) e.addError(Label.ERR_visitaTitolare);
      if (!CU.isLeasing(e.OwnerId) && !CU.isLending(e.OwnerId) && !CU.isFinanzaStrutturata(e.OwnerId)) {
        if (visitaRelazionale && !clienteAttivo) e.addError(Label.ERR_visitaRelazionale);
      }
    }
  }


  /**
  * Metodo che impedisce di cancellare un task o un evento, se è già stato chiuso o se l'utente è un commerciale
  * o un gestore debitori e l'attività non è assegnata a lui.
  **/
  public static void controlloCancellazioneActivity(T tu) {

    SObject[] daCancellare = tu.oldMap.values();

    for(SObject t : daCancellare) {

      //impedisci di cancellare task già chiuso
      if( t.getSobjectType() == Task.getSObjectType() && t.get('Status') == 'Chiuso') {
        t.addError(Label.ERR_cancellaTask);
      }

      //impedisci di cancellare evento già chiuso (considero chiuso se è presente un esito di livello 1)
      if( t.getSobjectType() == Event.getSObjectType() && t.get('EsitoLivello1__c') != null) {
        t.addError(Label.ERR_cancellaEvento);
      }

      //se l'activity non è mia e sono un commerciale o un gestore debitori, non la posso cancellare
      if(t.get('OwnerId') != UserInfo.getUserId() && (CU.isCommerciale() || CU.isGestoreDebitori())) {
        t.addError(Label.ERR_cancellaAttivita);
      }

      //se l'activity non è stata creata da me e sono un commerciale, non la posso cancellare
      // skippo per utente integrazione
      UtenzeDefault__c u = UtenzeDefault__c.getInstance((Id) t.get('CreatedById'));
      if(t.get('CreatedById') != UserInfo.getUserId() && !u.AssegnatarioDiDefault__c && CU.isCommerciale()) {
        t.addError(Label.ERR_cancellaAttivitaCreata);
      }

    }
  }


  /*
  * Cambio ownership per operatori Filo Diretto
  * Esitazione di un task non mio di tipo contatto telefonico DIRETTO con associato un account
  * il task apparteneva ad un altro operatore di filo diretto
  */
  public static void gestisciOwnershipTaskFiloDiretto(T tu) {

    if (!CU.isFiloDiretto()) return;
    //prendo i task da controllare e i corrispondenti owners (inclusi i loro ruoli)
    List<Task> daControllare = (List<Task>) tu
      .filter('RecordTypeId', U.getRecordTypes('Task').get('ContattoTelefonicoDiretto').Id)
      .triggerNew;

    Set<Id> ownersId = U.getIdSet(daControllare, 'OwnerId');
    Map<Id, User> prevOwnersDiFD = new Map<Id, User>([SELECT Id
      FROM User
      WHERE UserRole.DeveloperName LIKE '%FiloDiretto%'
      AND Id IN :ownersId]);

    for(Task t : daControllare) {
      // se il precedente owner era di filo diretto e lo stato è chiuso (ovvero è esitato)
      if(prevOwnersDiFD.containsKey(t.OwnerId) && t.Status == 'Chiuso') {
        t.OwnerId = UserInfo.getUserId();
      }
    }

  }

  /**
  * Cambio owner dell'account collegato al task secondo le condizioni:
  * Se GestoreDebitori o International crea un task di tipo Segnalazione legato ad un account, lui diventa il presentatore di quell’account
  * La Segnalazione genera un Task CT assegnato ad un operatore FD (in base CAP) o a un operatore International e associato ad una campagna
  **/
  public static void gestisciSegnalazione(T tu) {
    // scremo solo per le segnalazioni
    if(!(CU.isGestoreDebitori() || CU.isInternational())) return;
    Task[] listaSegnalazioni = tu.filter('RecordTypeId',  U.getRecordTypes('Task').get('Segnalazione').Id).triggerNew;
    if(listaSegnalazioni.isEmpty()) return;

    // ok, sono segnalazioni
    // procedo con cambio ownership dell'account (TODO check se anche altri oggetti)
    Map<Id, Account> accs = new Map<Id, Account>([SELECT
      Id,
      ShippingPostalCode,
      ShippingState,
      ShippingCity,
      BillingPostalCode,
      BillingState,
      BillingCity
      FROM Account
      WHERE Id IN :U.getIdSet(listaSegnalazioni, 'WhatId')
    ]);

    for(Account a : accs.values()) {
      a.Presentatore__c = UserInfo.getUserId();
    }
    update accs.values();

    Map<String, Id> cap2owner;
    if (CU.isGestoreDebitori()) {
      Set<String> caps = U.getSet(accs.values(), 'ShippingPostalCode');
      Set<String> comuni = U.getSet(accs.values(), 'ShippingCity');
      Set<String> provincie = U.getSet(accs.values(), 'ShippingState');
      caps.addAll(U.getSet(accs.values(), 'BillingPostalCode'));
      comuni.addAll(U.getSet(accs.values(), 'BillingCity'));
      provincie.addAll(U.getSet(accs.values(), 'BillingState'));

      cap2owner = UtilAnagrafiche.getOwners(caps, comuni, provincie, true);
    }

    // ora genero il ContattoTelefonico
    SObject[] nuovi = new SObject[]{};
    for(Task t : listaSegnalazioni) {
      Id ownerId;
      // Se il gestore che ha creato la segnalazione è un operatore di Filo Diretto, assegno il task Contatto Telefonico ad un operatore FiloDiretto in base CAP..
      if (CU.isGestoreDebitori()) {
        ownerId = cap2owner.get('0000');
        if(String.isNotBlank(t.WhatId) && accs.containsKey(t.WhatId)) {
          Account a = accs.get(t.WhatId);
          if(String.isNotBlank(a.BillingPostalCode) && cap2owner.containsKey(a.BillingPostalCode)) ownerId = cap2owner.get(a.BillingPostalCode);
          else if(String.isNotBlank(a.ShippingPostalCode) && cap2owner.containsKey(a.ShippingPostalCode)) ownerId = cap2owner.get(a.ShippingPostalCode);
          else if(String.isNotBlank(a.BillingCity) && cap2owner.containsKey(a.BillingCity)) ownerId = cap2owner.get(a.BillingCity);
          else if(String.isNotBlank(a.ShippingCity) && cap2owner.containsKey(a.ShippingCity)) ownerId = cap2owner.get(a.ShippingCity);
          else if(String.isNotBlank(a.BillingState) && cap2owner.containsKey(a.BillingState)) ownerId = cap2owner.get(a.BillingState);
          else if(String.isNotBlank(a.ShippingState) && cap2owner.containsKey(a.ShippingState)) ownerId = cap2owner.get(a.ShippingState);
        }
      // ..altrimenti, se il gestore che ha creato la segnalazione è un International, assegno il task a un utente International di default
      } else if (CU.isInternational()) {
        ownerId = Impostazioni__c.getInstance().IdOwnerCampagnaSviluppoDebInternational__c;
      }

      // il task sarà a sua volta associato alla campagna default di filo diretto o di international
      nuovi.add(new Task(
        WhatId = t.WhatId, // account della segnalazione
        Status = 'Aperto',
        OwnerId = ownerId,
        Campagna__c = CU.isGestoreDebitori() ? Impostazioni__c.getInstance().IdCampagnaSviluppoDebitori__c : Impostazioni__c.getInstance().IdCampagnaSviluppoDebitoriInternational__c, // campagna default
        Subject = 'Contatto telefonico',
        RicontattoTelefonico__c = false,
        ActivityDate = Date.today(),
        RecordTypeId = U.getRecordTypes('Task').get('ContattoTelefonicoDiretto').Id
      ));
    }
    insert nuovi;

  }

  public static Map<Id, SObject> getWhatMap(SObject[] activities, Boolean onlyWhatWithOwner) {
    Map<Id, SObject> res = new Map<Id, SObject>();
    Map<String, SObject> eventOrTaskMap = U.keyBy(activities, 'WhatId');

    Map<SObjectType, Set<Id>> whatMap = new Map<SObjectType, Set<Id>>();
    for(SObject o : U.filter(activities, 'WhatId', (String) null, false)) {
      SObjectType objType = ((Id) o.get('WhatId')).getSObjectType();
      if(!whatMap.containsKey(objType)) whatMap.put(objType, new Set<Id>());
      whatMap.get(objType).add((Id) o.get('WhatId'));
    }

    //QUERY NEL FOR!!! MA LE CHIAVI DELLA MAPPA SARANNO 2 O 3 AL MASSIMO. BY MT
    for(SObjectType objType : whatMap.keySet()) {
      Schema.DescribeSObjectResult dsor = objType.getDescribe();
      String[] fields = new String[]{'Id', 'Name'};
      if(dsor.fields.getMap().containsKey('OwnerId')) fields.add('OwnerId');
      else if(onlyWhatWithOwner) continue;

      String q = QueryBuilder.newInstance(dsor.getName(), fields)
        .beginFilter()
          .add('Id', QBOp.QIN, whatMap.get(objType))
        .endFilter()
        .getQuery();

      SObject[] objL = Database.query(q);
      res.putAll(objL);
    }

    return res;
  }

  /**
   * Blocca l'inserimento/update/cancellazione di un task/evento se questo è collegato ad un oggetto il cui owner non è l'utente stesso.
   * Il controllo non viene fatto per utenze che non siano commerciali factoring o che siano responsabili filiale
   * Il controllo non viene fatto per task/eventi di tipo "Visita debitore", "Visita Relazionale", e nel caso di
   * international per il contatto telefonico creato da una segnalazione debitori da parte di un operatore international
   **/
  public static void bloccaAzioniActivityNonOwner(T tu) {
    if(!CU.isCommerciale() || CU.isResponsabileFiliale()) return;

    SObject[] tuList = tu.triggerNew;
    if(tu.mode == T.TMode.xDELETE) tuList = tu.oldMap.values();
    // non faccio il controllo per quelle che sono visita debitori (requisito GPD)
    tuList = U.filter(tuList, 'TipoAttivita__c', 'Visita debitore', false);
    // non lo faccio neanche per Visita Relazionale
    tuList = U.filter(tuList, 'TipoAttivita__c', 'Visita Relazionale', false);
    // INTERNATIONAL: non faccio il controllo il contatto telefonico creato da una segnalazione debitori da parte di un operatore international
    if(CU.isInternational()) {
      tuList = U.filter(tuList, 'Campagna__c', Impostazioni__c.getInstance().IdCampagnaSviluppoDebitoriInternational__c, false);
      tuList = U.filter(tuList, 'RecordTypeDevName__c', 'Segnalazione', false);
    }

    tuList = U.filter(tuList, 'TipoAttivita__c', (String) null, false);

    tuList = U.filter(tuList, 'WhatId', (String) null, false);

    Map<Id, SObject> whatMap = getWhatMap(tuList, true);

    Map<String, AccountTeamMember[]> coinvoltiAccountTeam = U.groupBy([
      SELECT UserId,
      AccountId
      FROM AccountTeamMember
      WHERE AccountId IN :U.getSet(tuList, 'WhatId')
      ], 'AccountId');

    Boolean isUpdate = tu.mode == T.TMode.xUPDATE;
    for(SObject activity : tuList) {
      Id whatId = (Id) activity.get('WhatId');
      if (whatMap.containsKey(whatId)) {
        // whatId = Account: se l'owner dell'activity non è nè nell'Account Team nè è l'owner del whatId lancio errore
        if (whatMap.get(whatId).getSobjectType() == Account.SObjectType) {
          if(!trgActivity.contestoTrgAccountCambiaOwnerAttivita && isUpdate && activity.getSObjectType().getDescribe().getName() == 'Task' && UserInfo.getUserId() != activity.get('OwnerId')){
            activity.addError(Label.ERR_modificaTaskProprietario);
          }
          if((!U.getSet(coinvoltiAccountTeam.get(whatId), 'UserId').contains(String.valueOf(activity.get('OwnerId'))) && whatMap.get(whatId).get('OwnerId') != activity.get('OwnerId'))){
            activity.addError(Label.ERR_richiediAssegnazioneAnagrafica);
          }
        // whatId != Account: se l'owner dell'activity non è l'owner del whatId lancio errore
        } else {
          if(whatMap.get(whatId).get('OwnerId') != activity.get('OwnerId')) {
              activity.addError(Label.ERR_richiediAssegnazioneAnagrafica);
          }
        }
      }
    }
  }

  /**
   * Leasing.
   * Alla chiusura di un'attività di tipo "Visita.." o "Contatto Telefonico", se l'attività è associata a un Account e
   * l'owner dell'attività è un commercialeEF o TR di quell'Account, aggiorno la corrispettiva data di ultimo contatto.
   */
  public static void popolaDataUltimoContatto(T tu) {
    SObject[] activities = tu.filter('EsitoLivello1__c', null, false).getChanged('EsitoLivello1__c');
    Map<Id,Account> accsToUpdate = new Map<Id,Account>();
    Map<Id,Account> accountCoinvolti = new Map<Id,Account>([
      SELECT Id,
      CommercialeEF__c,
      CommercialeTR__c,
      UltimoContattoEF__c,
      UltimoContattoTR__c
      FROM Account
      WHERE Id IN :U.getIdSet(activities, 'WhatId')
    ]);

    for(SObject activity : activities) {
      String tipoAttivita = (String) activity.get('TipoAttivita__c');
      Id whatId = (Id) activity.get('WhatId');
      Id ownerId = (Id) activity.get('OwnerId');
      Date activityDate = (Date) activity.get('ActivityDate');
      if ((tipoAttivita.containsIgnoreCase('Contatto Telefonico') || tipoAttivita.containsIgnoreCase('Visita')) && whatId != null && whatId.getSobjectType() == Account.SObjectType) {
        Account a = accsToUpdate.containsKey(whatId) ? accsToUpdate.get(whatId) : accountCoinvolti.get(whatId);
        if (ownerId == a.CommercialeEF__c && (activityDate >= a.UltimoContattoEF__c || a.UltimoContattoEF__c == null)) a.UltimoContattoEF__c = activityDate;
        if (ownerId == a.CommercialeTR__c && (activityDate >= a.UltimoContattoTR__c || a.UltimoContattoTR__c == null)) a.UltimoContattoTR__c = activityDate;
        accsToUpdate.put(whatId, a);
      }
    }
    update accsToUpdate.values();
  }

  /**
  * Se un utente filo diretto esita un task senza flag "gia cliente" e senza "non sovrascrivere"
  * l'utente FiloDiretto diventa owner e presentatore (e diventa anche OpFiloDiretto__c dell'account via trigger)
  */
  public static void gestisciOwnershipAccountFiloDiretto(T tu) {
    if (!CU.isFiloDiretto()) return;

    // filtro task per non gia cliente, escludendo i promemoria
    List<Task> task = tu.filter('GiaCliente__c', true, false).filter('NonSovrascrivere__c', true, false).filter('Status', 'Chiuso').filter('RecordTypeId', U.getRecordTypes('Task').get('Promemoria').Id, false).getChanged(new String[]{'Status'});

    Set<Id> accountIds = U.getIdSet(task, 'WhatId');

    Account[] accs = new Account[]{};
    for(Id aId : accountIds) {
      accs.add(new Account(
        Id = aId,
        OwnerId = UserInfo.getUserId(),
        Presentatore__c = UserInfo.getUserId()
      ));
    }

    update accs;
  }

  /*************************************************
  * Metodi privati di appoggio
  *************************************************/

  /**
  * Metodo che controlla che ci siano tutte le date di ricontatto quando servono
  */
  private static void controllaDataRicontatto(SObject[] lista) {
    for(SObject dp: lista) {
      if(dp.get('DataRicontatto__c') == null) dp.addError(Label.ERR_esitoTaskRecall);
    }
  }


  /**
  * Metodo che blocca l'esitazione se il task è di filo diretto ed è una visita (necessita dunque di creazione evento)
  **/
  @TestVisible
  private static void bloccaEsitoSeFiloDiretto(SObject[] lista) {
    for(SObject dp : lista) {
      if(dp.get('DiFiloDiretto__c') == true && dp.getSObjectType() == Task.SObjectType && !Funzionalita__c.getInstance().EditTaskFD__c) dp.addError(Label.ERR_esitoTaskFD);
    }
  }

  /**
  * Metodo che setta FuoriMagazzino a true per gli account id passati ( o per i task)
  **/
  public static void settaFuoriMagazzino(List<SObject> lista, Boolean fuori) {
    Set<Id> accsIds = U.getIdSet(lista, 'WhatId');
    settaFuoriMagazzino(accsIds, fuori);
  }

  public static void settaFuoriMagazzino(Set<Id> accsIds, Boolean fuori) {

    Account[] accs = new Account[]{};
    for(Id aId : accsIds) {
      accs.add(new Account(Id = aId, FuoriMagazzino__c = fuori));
    }

    update accs;
  }

}
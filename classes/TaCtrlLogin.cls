/**
* Progetto:         Banca IFIS - Ti Anticipo
* Sviluppata il:    14/06/2016
* Developer:        Stefano Brivio
*/

public class TaCtrlLogin {

  private static String CANALE_TA = '5';
  private static String SOTTOCANALE_TA = '5';

  public TaCtrlLogin() {}

  public class InputObj {
    public String nome, cognome, email, password, ragioneSociale, piva, selectedAccountKey, forgottenEmail, telefono;
    public Account selectedAccount;
    public Boolean privacy;
  }

  public class ProfileInfo {
    @AuraEnabled public Account account;
    @AuraEnabled public Contact contact;
    @AuraEnabled public List<Contact> titolariEsecutori;
    @AuraEnabled public AdeguataVerifica__c adegVerifica;

    public ProfileInfo(Account account, Contact contact) {
      this.account = account;
      this.contact = contact;
      this.titolariEsecutori = new List<Contact>();
      this.adegVerifica = new AdeguataVerifica__c();
    }

    public ProfileInfo(Account account, Contact contact, List<Contact> titolariEsecutori) {
      this.account = account;
      this.contact = contact;
      this.titolariEsecutori = titolariEsecutori;
      this.adegVerifica = new AdeguataVerifica__c();
    }

    public ProfileInfo(Account account, Contact contact, List<Contact> titolariEsecutori, AdeguataVerifica__c adegVerifica) {
      this.account = account;
      this.contact = contact;
      this.titolariEsecutori = titolariEsecutori;
      this.adegVerifica = adegVerifica;
    }
  }

  public class UserInfoObj {
    @AuraEnabled public String firstName, lastName, fullName, username;
    @AuraEnabled public Boolean accessoEffettuato;

    public UserInfoObj(String firstName, String lastName, String fullName, String username, Boolean accessoEffettuato) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.fullName = fullName;
      this.username = username;
      this.accessoEffettuato = accessoEffettuato;
    }
  }

  public static String login(String input) {
    String errorMessage;
    InputObj obj = (InputObj)JSON.deserialize(input, InputObj.class);
    ApexPages.PageReference pRef;
    try {
      // includere /s/ per gestire il redirect lato client
      pRef = Site.login(TiAnticipoUtils.getCommunityUsername(obj.email), obj.password, '/s/home');
      if (pRef != null) {
      } else {
        errorMessage = 'Credenziali non valide';
      }
    } catch (Exception e) {
      errorMessage = e.getMessage();
    }
    if (String.isNotBlank(errorMessage)) {
      throw new Ex.GenericException(errorMessage);
    }
    return pRef.getUrl();
  }

  public static String selfRegister(String input) {
    InputObj obj = (InputObj)JSON.deserialize(input, InputObj.class);
    validateRegistration(obj);

    Account acc = obj.selectedAccount;
    Boolean fixDati = false;
    if (String.isBlank(acc.Id)) {
      fixDati = true;
    }
    acc = CtrlWizardAccount.setupAccount(acc);
    CtrlWizardAccount.insertAccount(acc, acc.Origine__c, true);
    if (fixDati) {
      acc.CanaleSviluppo__c = CANALE_TA;
      acc.Sottocanale__c = SOTTOCANALE_TA;
      acc.ClienteTiAnticipo__c = true;
      acc.OwnerId = UtenzeDefault__c.getInstance().IdUtenteDefaultS__c;
      update acc;
    }

    String username = TiAnticipoUtils.creaCommunityUsername(obj.email);
    Id userId = createUser(username, obj.email, obj.nome, obj.cognome, obj.password, acc.Id);

    User u = [SELECT Id, ContactId, Username FROM User WHERE Id = :userId];
    update new Contact(
      Id = u.ContactId,
      DataIscrizioneTiAnticipo__c = Datetime.now(),
      PrivacyRegistrazioneTiAnticipo__c = obj.privacy,
      Phone = obj.telefono
    );

    return login(JSON.serialize(obj));
  }

  public static Boolean resetPassword(String input) {
    String errorMessage;
    InputObj obj = (InputObj)JSON.deserialize(input, InputObj.class);
    String username = TiAnticipoUtils.getCommunityUsername(obj.forgottenEmail);
    if (!Site.isValidUsername(username)) {
      errorMessage = 'Lo username inserito non è valido';
    } else {
      Boolean success = Site.forgotPassword(username);
      if (!success) throw new Ex.GenericException('Non è stato possibile resettare la password per l\'email indicata');
    }
    return true;
  }

  private static void validateRegistration(InputObj obj) {
    String passError;
    String errorMessage = '';
    String[] errors = new String[] {};

    if (!UtilAnagrafiche.isOK(obj.nome)) errors.add('Nome');
    if (!UtilAnagrafiche.isOK(obj.cognome)) errors.add('Cognome');
    if (!UtilAnagrafiche.isOK(obj.email)) errors.add('Email');
    if (!UtilAnagrafiche.isOK(obj.ragioneSociale)) errors.add('Ragione sociale');
    if (!UtilAnagrafiche.isOK(obj.piva)) errors.add('Partita IVA');
    if (String.isBlank(obj.password)) {
      passError = 'Password non valida';
    }

    if (!errors.isEmpty()) {
      errorMessage = 'I seguenti campi sono obbligatori: ' + String.join(errors, ', ') + ' ';
    }
    if (String.isNotBlank(passError)) {
      errorMessage += passError;
    }

    if (String.isNotBlank(errorMessage)) new Ex.GenericException(errorMessage);
  }

  private static Id createUser(String username, String email, String nome, String cognome, String password, Id accountId) {
    User u = new User();
    u.Username = username;
    u.Email = email;
    u.FirstName = nome;
    u.LastName = cognome;
    u.TimeZoneSidKey = 'Europe/Rome';
    u.CommunityNickname = u.Username.left(40);

    Site.validatePassword(u, password, password);

    return Site.createPortalUser(
             u,
             accountId,
             password,
             true
           );
  }

  public static ProfileInfo fetchProfile() {
    if (UserInfo.getUserType() == 'Guest') return null;

    Id contactId = TiAnticipoUtils.getUserContactId();
    Contact userContact = [SELECT Id,
                           AccountId,
                           Name,
                           FirstName,
                           LastName,
                           CF__c,
                           Ruolo__c,
                           Birthdate,
                           LuogoNascita__c,
                           Phone,
                           MobilePhone,
                           Email
                           FROM Contact
                           WHERE Id = :contactId];

    Account userAccount = [SELECT Id,
                           Name,
                           PIVA__c,
                           BillingCity,
                           BillingCountry,
                           BillingPostalCode,
                           BillingState,
                           BillingStreet,
                           BillingStreetType__c,
                           BillingStreetNumber__c,
                           BillingStreetName__c,
                           ShippingCity,
                           ShippingCountry,
                           ShippingPostalCode,
                           ShippingState,
                           ShippingStreet,
                           ShippingStreetType__c,
                           ShippingStreetNumber__c,
                           ShippingStreetName__c,
                           FirmaDigitale__c,
                           REA__c,
                           SAE__c,
                           RAE__c,
                           Ateco__c,
                           EmailPEC__c,
                           Email__c,
                           TelefonoPrefisso__c,
                           TelefonoNumero__c,
                           Fax,
                           NaturaGiuridica__c,
                           NDGGruppo__c,
                           ProvinciaCerved__c,
                           IBAN__c,
                           PresaVisionePrivacy__c,
                           CCIAA__c,
                           SIA__c,
                           PaeseSvolgimentoAttivitaPrevalente__c,
                           ProvinciaAttivitaPrevalente__c,
                           QualificaCliente__c
                           FROM Account
                           WHERE Id = :userContact.AccountId];

    AdeguataVerifica__c adv;
    AdeguataVerifica__c[] advs = [SELECT Id,
                                  Account__c,
                                  CorrispondenzaStreetType__c,
                                  CorrispondenzaStreetName__c,
                                  CorrispondenzaStreetNumber__c,
                                  CorrispondenzaCity__c,
                                  CorrispondenzaCAP__c,
                                  CorrispondenzaState__c,
                                  CorrispondenzaCountry__c,
                                  TipoIndirizzo__c
                                  FROM AdeguataVerifica__c
                                  WHERE Account__c = :userAccount.Id];
    adv = (advs.size() != 0) ? advs[0] : new AdeguataVerifica__c(
      Account__c = userAccount.Id,
      TipoIndirizzo__c = 'billing',
      CorrispondenzaCity__c = userAccount.BillingCity,
      CorrispondenzaCountry__c = userAccount.BillingCountry,
      CorrispondenzaCAP__c = userAccount.BillingPostalCode,
      CorrispondenzaState__c = userAccount.BillingState,
      CorrispondenzaStreetType__c = userAccount.BillingStreetType__c,
      CorrispondenzaStreetNumber__c = userAccount.BillingStreetNumber__c,
      CorrispondenzaStreetName__c = userAccount.BillingStreetName__c
      );
    if (adv.Id == null) insert adv;

    List<Contact> titolariEsecutori = TaCtrlRegistrazioneUtenti.fetchContacts();

    return new ProfileInfo(userAccount, userContact, titolariEsecutori, adv);
  }

  public static Boolean updateProfile(String input) {
    ProfileInfo profile = (ProfileInfo)JSON.deserialize(input, ProfileInfo.class);
    try {
      Account acc = profile.account;
      update acc;

      Contact con = profile.contact;
      update con;

      AdeguataVerifica__c adv = profile.adegVerifica;
      update adv;

      update profile.titolariEsecutori;

    } catch (Exception e) {
      throw new Ex.GenericException(e.getMessage());
    }
    return true;
  }

  public static UserInfoObj getUserInfo() {
    Contact userContact = [SELECT Id,
      AccessoEffettuatoTiAnticipo__c
      FROM Contact
      WHERE Id = :TiAnticipoUtils.getUserContactId()];

    return new UserInfoObj(
      UserInfo.getFirstName(),
      UserInfo.getLastName(),
      UserInfo.getName(),
      UserInfo.getUserName(),
      userContact.AccessoEffettuatoTiAnticipo__c
    );
  }

  public static Boolean setAccessoEffettuato(Boolean value) {
    update new Contact(Id = TiAnticipoUtils.getUserContactId(), AccessoEffettuatoTiAnticipo__c = value);
    return true;
  }
}
public without sharing class TiAnticipoUtils {

  //metodi allineamento Cessione -> Opportunity per TiAnticipo

  /**
   * Allineamento Cessione -> pConfigurati per TiAnticipo:
   * Da alcuni valori relativi a una cessione vengono popolati alcuni pConfigurati
   * @param  cessioni
   */
  public static void syncParametri(Cessione__c[] cessioni) {
    PConfigurato__c[] pConfigurati = [SELECT Id,
                                      Codice__c,
                                      Valore__c,
                                      Linea__r.Opportunity__r.Cessione__c
                                      FROM   PConfigurato__c
                                      WHERE  Linea__r.Opportunity__r.Cessione__c IN :U.getSet(cessioni, 'Id')
                                      AND    Linea__r.Opportunity__r.StageName = 'In Lavorazione'];
    Map<String, PConfigurato__c[]> pConfiguratiPerCessione = U.groupBy(pConfigurati, '{Linea__r.Opportunity__r.Cessione__c}_{Codice__c}', true);
    for (Cessione__c cessione : cessioni) {
      if (pConfiguratiPerCessione.containsKey((cessione.Id + '_C150')) && cessione.SpeseIstruttoriaDebitore__c != null && cessione.SpeseIstruttoriaCedente__c != null)
        pConfiguratiPerCessione.get(cessione.Id + '_C150')[0].Valore__c = String.valueOf(cessione.SpeseIstruttoriaDebitore__c + cessione.SpeseIstruttoriaCedente__c);
      if (pConfiguratiPerCessione.containsKey((cessione.Id + '_SF5')))
        pConfiguratiPerCessione.get(cessione.Id + '_SF5')[0].Valore__c = String.valueOf(cessione.ImportoTotaleCertificazioni__c);
      if (pConfiguratiPerCessione.containsKey((cessione.Id + '_SF6')))
        pConfiguratiPerCessione.get(cessione.Id + '_SF6')[0].Valore__c = String.valueOf(cessione.ImportoTotaleCertificazioni__c);
    }
    upsert pConfigurati;
  }

  public static Id getUserAccountId() {
    User u = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
    return String.isNotBlank(u.Contact.AccountId) ? u.Contact.AccountId : UtenzeDefault__c.getInstance().IdAccountDiDefault__c;
  }

  public static Id getUserContactId() {
    User u = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
    String result = String.isNotBlank(u.ContactId) ? u.ContactId : UtenzeDefault__c.getInstance().IdReferenteDiDefault__c;
    return result;
  }

  public static Decimal calcoloCostoPreventivoPubblico(String segmentoRischio, Decimal importoFattura, Integer deltaGiorni) {
    Decimal costo;

    CommissioneATD__mdt[] commissioniAtd = [SELECT Id,
                                            CommissioneATDMese__c,
                                            SegmentoRischio__c,
                                            EstremoInferiore__c,
                                            EstremoSuperiore__c
                                            FROM CommissioneATD__mdt
                                            WHERE SegmentoRischio__c = :segmentoRischio];

    SpesaIstruttoria__mdt[] spese = [SELECT Id,
                                     SpeseCedente__c,
                                     SpeseDebitore__c,
                                     EstremoInferiore__c,
                                     EstremoSuperiore__c
                                     FROM SpesaIstruttoria__mdt];

    CommissioneATD__mdt commissioneAtd = getCommissioneAtd(commissioniAtd, segmentoRischio, importoFattura);
    SpesaIstruttoria__mdt spesa = getSpesaIstruttoria(spese, importoFattura);
    if (commissioneAtd != null && spesa != null) {
      Decimal commissione = commissioneAtd.CommissioneATDMese__c / 100;
      costo = importoFattura * commissione / 30 * deltaGiorni + spesa.SpeseCedente__c + spesa.SpeseDebitore__c;
    }
    return costo;
  }

  public static void calcoloPreventivo(Set<Id> idCessioni) {
    Cessione__c[] cessioni = [SELECT Id,
                              (SELECT Id,
                               Cessione__c,
                               CertificazioneBocciata__c,
                               CertificazioneValida__c,
                               Debitore__c,
                               DeltaGiorni__c,
                               ImportoCertificato__c,
                               ImportoCertificatoCalcolo__c
                               FROM Certificazioni__r
                               WHERE CertificazioneValida__c = true)
                              FROM Cessione__c
                              WHERE Id IN :idCessioni];

    CommissioneATD__mdt[] commissioniAtd = [SELECT Id,
                                            CommissioneATDMese__c,
                                            SegmentoRischio__c,
                                            EstremoInferiore__c,
                                            EstremoSuperiore__c
                                            FROM CommissioneATD__mdt];

    Set<Id> debSet = new Set<Id>();
    for (Cessione__c cessione : cessioni) {
      debSet.addAll(U.getIdSet(cessione.Certificazioni__r, 'Debitore__c'));
    }
    Map<Id, Account> debitoriMap = new Map<Id, Account>(
      [SELECT Id,
       SegmentoRischio__c
       FROM Account WHERE Id IN :debSet]
    );

    for (Cessione__c cessione : cessioni) {
      Decimal costoComplessivoC138 = 0;

      Map<String, Certificazione__c[]> certificazioniPerDebitoreMap = U.groupBy(cessione.Certificazioni__r, 'Debitore__c');

      for (Id idDebitore : certificazioniPerDebitoreMap.keySet()) {
        Account debitore = debitoriMap.get(idDebitore);
        Certificazione__c[] certificazioniPerDebitore = certificazioniPerDebitoreMap.get(idDebitore);
        Decimal totaleOriginarioPerDebitore = U.sumBy(certificazioniPerDebitore, 'ImportoCertificato__c');
        CommissioneATD__mdt commissioneAtd = getCommissioneAtd(
                                               commissioniAtd,
                                               debitore.SegmentoRischio__c,
                                               totaleOriginarioPerDebitore
                                             );

        Decimal c138 = calcoloC138(certificazioniPerDebitore, debitore, commissioneAtd);

        Certificazione__c[] certificazioniNonBocciate = U.filter(certificazioniPerDebitore, 'CertificazioneBocciata__c', false);
        Decimal totaleCertificazioniNonBocciate = U.sumBy(certificazioniNonBocciate, 'ImportoCertificatoCalcolo__c');
        costoComplessivoC138 += totaleCertificazioniNonBocciate * c138;
      }
      cessione.CostoComplessivoC138__c = costoComplessivoC138;
    }
    update cessioni;
  }

  //il calcolo della c138 deve includere anche certificazioni bocciate e riviste
  public static Decimal calcoloC138(Certificazione__c[] certificazioniPerDebitore, Account debitore, CommissioneATD__mdt commissioneAtd) {
    Decimal costoPerDebitore = 0;
    Decimal totaleOriginarioPerDebitore = U.sumBy(certificazioniPerDebitore, 'ImportoCertificato__c');
    for (Certificazione__c c : certificazioniPerDebitore) {
      try {
        costoPerDebitore += c.ImportoCertificato__c * commissioneAtd.CommissioneATDMese__c / 100 / 30 * c.DeltaGiorni__c;
      } catch (Exception ex) {
        // Se viene lanciata l'eccezione è perchè non è accettata la certificazione in fase di upload. Verrà quindi
        // successivamente rieseguito questo calcolo, popolando correttamente i valori.
        costoPerDebitore = 0;
      }
    }
    Decimal c138 = costoPerDebitore / totaleOriginarioPerDebitore;
    return c138;
  }

  public static void calcolaSpeseIstruttoria(Set<Id> idCessioni) {
    //questo calcolo prende in considerazione ANCHE le certficiazioni bocciate, come da richiesta di Keoma
    //e quindi deve usare ImportoTotaleCertificazioniOriginario__c
    Cessione__c[] cessioni = [SELECT Id,
                              ImportoTotaleCertificazioniOriginario__c,
                              NumeroCertificazioni__c,
                              (SELECT Id,
                               Debitore__c
                               FROM Certificazioni__r
                               WHERE CertificazioneValida__c = true)
                              FROM Cessione__c
                              WHERE Id IN :idCessioni];

    SpesaIstruttoria__mdt[] spese = [SELECT Id,
                                     SpeseCedente__c,
                                     SpeseDebitore__c,
                                     EstremoInferiore__c,
                                     EstremoSuperiore__c
                                     FROM SpesaIstruttoria__mdt];

    for (Cessione__c c : cessioni) {
      Decimal speseCedente, speseDebitore;
      if (c.NumeroCertificazioni__c == 0) {
        speseCedente = 0;
        speseDebitore = 0;
      } else {
        Integer numeroDebitori = getNumeroDebitori(c.Certificazioni__r);
        Decimal mediaImportoCertificazioni = c.ImportoTotaleCertificazioniOriginario__c / numeroDebitori;

        SpesaIstruttoria__mdt spesa = getSpesaIstruttoria(spese, mediaImportoCertificazioni);
        if (spesa != null) {
          speseDebitore = spesa.SpeseDebitore__c * numeroDebitori;
        }
        spesa = getSpesaIstruttoria(spese, c.ImportoTotaleCertificazioniOriginario__c);
        if (spesa != null) {
          speseCedente = spesa.SpeseCedente__c;
        }
      }
      c.SpeseIstruttoriaCedente__c = speseCedente;
      c.SpeseIstruttoriaDebitore__c = speseDebitore;
    }

    update cessioni;
  }

  private static Integer getNumeroDebitori(Certificazione__c[] certificazioni) {
    return U.groupBy(certificazioni, 'Debitore__c')
           .keySet()
           .size();
  }

  public static CommissioneATD__mdt getCommissioneAtd(
    CommissioneATD__mdt[] commissioni,
    String segmento,
    Decimal importo) {
    for (CommissioneATD__mdt c : commissioni) {
      if (c.SegmentoRischio__c == segmento
          && c.EstremoInferiore__c < importo
          && c.EstremoSuperiore__c >= importo) {
        return c;
      }
    }
    return null;
  }

  private static SpesaIstruttoria__mdt getSpesaIstruttoria(SpesaIstruttoria__mdt[] spese, Decimal importo) {
    for (SpesaIstruttoria__mdt s : spese) {
      if (s.EstremoInferiore__c < importo
          && s.EstremoSuperiore__c >= importo) {
        return s;
      }
    }
    return null;
  }

  public static Boolean setUtenteTitolare() {
    if (canBeUtenteTitolare()) {
      Contact c = new Contact(Id = TiAnticipoUtils.getUserContactId(), TitolareTa__c = true);
      update c;
      return true;
    }
    return false;
  }

  public static Boolean isUtenteTitolare() {
    Id contactId = TiAnticipoUtils.getUserContactId();
    Id accId = TiAnticipoUtils.getUserAccountId();
    Contact contatto = [SELECT Id, TitolareTa__c
                        FROM Contact
                        WHERE AccountId = :accId
                                          AND Id = :contactId];

    return contatto.TitolareTa__c;
  }

  public static Boolean canBeUtenteTitolare() {
    Id contactId = TiAnticipoUtils.getUserContactId();
    Id accId = TiAnticipoUtils.getUserAccountId();
    Contact[] contatti = [SELECT Id, TitolareTa__c
                          FROM Contact
                          WHERE AccountId = :accId
                                            AND Id != :contactId
                                            AND TitolareTa__c = true];

    return contatti.size() == 0;
  }

  public static String getCommunityUsername(String email) {
    return email + '.' + ParametriTiAnticipo__c.getOrgDefaults().UsernameSuffix__c;
  }

  public static String creaCommunityUsername(String email) {
    String username = getCommunityUsername(email);
    testUniqueUsername(username);
    return username;
  }

  private static void testUniqueUsername(String username) {
    User[] existingUsers = [SELECT Id FROM User WHERE Username = :username];
    if (existingUsers.size() > 0) throw new Ex.GenericException('Esiste già un\'utenza associata a questo indirizzo email. Contatta Banca IFIS per maggiori informazioni.');
  }
}
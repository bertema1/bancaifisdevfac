@isTest
private class RestResourceTest {
  @TestSetup
  static void setupData() {
    Funzionalita__c f = Funzionalita__c.getInstance();
    f.DisabilitaControlloCambioFaseOpp__c = true;
    upsert f;

    insert new EndpointServizi__c(
      Name = 'settings',
      LAGKey__c = 'x',
      URLGetBilancioSinteticoRibes__c = 'https://a.com',
      UrlSearchDocument__c = 'https://b.com'
    );
  }

  @isTest
  static void testAnagrafeCrm() {
    Account[] accs = [SELECT Id FROM Account];
    System.assert(accs.isEmpty());

    RestAnagrafeCrm.DatiAnagraficiCedacriGlobal d = (RestAnagrafeCrm.DatiAnagraficiCedacriGlobal)JSON.deserialize(TestMockUtils.PAYLOAD_REST_ANAGRAFECRM, RestAnagrafeCrm.DatiAnagraficiCedacriGlobal.class);
    RestCommon.ResponseObject response = RestAnagrafeCrm.updateNdg(null, d);
    System.assert(response.returnCode == RestCommon.NESSUN_ERRORE);

    accs = [SELECT Id FROM Account];
    System.assert(accs.size() == 1);
  }

  @isTest
  static void testStatoOpportunita() {
    //TODO FIX IT
    // RestStatoOpportunita.Cartella c = (RestStatoOpportunita.Cartella)JSON.deserialize(TestMockUtils.PAYLOAD_REST_STATOOPPORTUNITA_CARTELLA, RestStatoOpportunita.Cartella.class);
    // RestStatoOpportunita.Linea[] l = (RestStatoOpportunita.Linea[])JSON.deserialize(TestMockUtils.PAYLOAD_REST_STATOOPPORTUNITA_LINEE, RestStatoOpportunita.Linea[].class);

    // RestCommon.ResponseObject response = RestStatoOpportunita.updateCartella(null, null, null, c, l);
    // System.assert(response.returnCode == RestCommon.ID_CARTELLA_ASSENTE);

    // Account a = TestUtils.creaAccount('test');
    // Opportunity opp = TestUtils.CreaOpportunity(a);
    // opp.IdCartella__c = '123456';
    // update opp;

    // response = RestStatoOpportunita.updateCartella(null, null, null, c, l);
    // System.assert(response.returnCode == RestCommon.ID_LINEA_ASSENTE);

    // TestUtils.creaLinee(opp, 3);
    // System.debug('IDDDDD'+[SELECT IdProdotto__c  FROM Linea__c WHERE Opportunity__c = :opp.Id][0].IdProdotto__c);
    // l[0].Id = [SELECT Id, IdProdotto__c  FROM Linea__c WHERE Opportunity__c = :opp.Id][0].IdProdotto__c;
    // l[1].Id = [SELECT Id, IdProdotto__c  FROM Linea__c WHERE Opportunity__c = :opp.Id][1].IdProdotto__c;
    // response = RestStatoOpportunita.updateCartella(null, null, null, c, l);
    // System.assertEquals(RestCommon.NESSUN_ERRORE, response.returnCode);
  }
}
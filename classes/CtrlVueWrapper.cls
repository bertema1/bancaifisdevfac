public with sharing class CtrlVueWrapper {
  @AuraEnabled
  public static Task[] fetchMyTasks() { return CtrlListeHp.fetchMyTasks(); }

  @AuraEnabled
  public static Event[] fetchMyEvents() { return CtrlListeHp.fetchMyEvents(); }

  @AuraEnabled
  public static SObject[] fetchActivities(String accountId) { return CtrlRecentActivities.fetchActivities(accountId); }

  // POSIZIONE ACCOUNT

  @AuraEnabled
  public static Rapporto__c[] fetchRapporti(String accountId) { return CtrlPosizioneAccount.fetchRapporti(accountId); }

  @AuraEnabled
  public static Rapporto__c fetchAssetDebitore(String accountId) { return CtrlPosizioneAccount.fetchAssetDebitore(accountId); }

  @AuraEnabled
  public static Relazione__c[] fetchRelazioneClienti(String accountId) { return CtrlPosizioneAccount.fetchRelazioneClienti(accountId); }

  @AuraEnabled
  public static String tipoAccount(String accountId) { return CtrlPosizioneAccount.tipoAccount(accountId); }

  @AuraEnabled
  public static List<Map<String, Object>> getRiepilogoOpportunita(String opportunityId) { return CtrlWizardOpportunity.getRiepilogoOpportunita(opportunityId); }

  @AuraEnabled
  public static List<Map<String, Object>> getRiepilogoInfoCr(String opportunityId) { return CtrlWizardOpportunity.getRiepilogoInfoCr(opportunityId); }

  @AuraEnabled
  public static List<Map<String, Object>> getRiepilogoAccountTeam(String accountId) { return CtrlWizardAccount.getRiepilogoAccountTeam(accountId); }

  @AuraEnabled
  public static Map<String, Map<String,Object>> getFieldSets(String fieldSetList, String sObjectId, String sObjectName) { return CtrlWizardAccount.getFieldSets(fieldSetList, sObjectId, sObjectName); }

  @AuraEnabled
  public static Id upsertObject(String obj, String objName) {
    try {
      return CtrlWizardAccount.upsertObject(obj, objName);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean renderModifica(String profileList) { return CtrlWizardAccount.renderModifica(profileList); }

  @AuraEnabled
  public static WSRestRibesUtils.EventiNegativiPlain getInformazioniPopupDebitore(String accountId) {
    try {
      return CtrlWizardOpportunity.getInformazioniPopupDebitore(accountId);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  public static Id getInformazioniPopupDebitorePDF(String accountId) {
    try {
      return CtrlWizardOpportunity.getInformazioniPopupDebitorePDF(accountId);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  public static void aggiornaNSA(Id oppId){ ExtViewCheckList.aggiornaNSA(oppId); }

  @AuraEnabled
  public static ExtViewCheckList.StatoCheckList getStatoChecklist(String oppId) { return ExtViewChecklist.getStatoChecklist(oppId); }

  @AuraEnabled
  public static Boolean refreshStatoCartella(String oppId) { return ExtViewChecklist.refreshStatoCartella(oppId); }

  @AuraEnabled
  public static String generaDocumentazione(Id oppId, String tipo) { return ExtViewChecklist.generaDocumentazione(oppId, tipo); }

  @AuraEnabled
  public static Opportunity getInformazioniOppChecklist(Id oppId) { return ExtViewCheckList.getInformazioniOppChecklist(oppId); }

  @AuraEnabled
  public static SObject inverseLookup(String fieldLabel, String fieldValue, String[] otherFields, String targetObj, String value) { return ExtSfdcLookup.inverseLookup(fieldLabel, fieldValue, otherFields, targetObj, value);}

  @AuraEnabled
  public static SObject[] searchRecent(String fieldLabel, String fieldValue, String[] otherFields, String targetObj, String[] valuesToExclude) { return ExtSfdcLookup.searchRecent(fieldLabel, fieldValue, otherFields, targetObj, valuesToExclude); }

  //COMMUNITY TI ANTICIPO
  @AuraEnabled
  public static String login(String input) {
    try {
      return TaCtrlLogin.login(input);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static String selfRegister(String input) {
    try {
      return TaCtrlLogin.selfRegister(input);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean resetPassword(String input) {
    try {
      return TaCtrlLogin.resetPassword(input);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean setAccessoEffettuato(Boolean value) {
    try {
      return TaCtrlLogin.setAccessoEffettuato(value);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static SObject[] search(String fieldLabel, String fieldValue, String[] otherFields, String targetObj, String value, String[] valuesToExclude) {
    try {
      return ExtSfdcLookup.search(fieldLabel, fieldValue, otherFields, targetObj, value, valuesToExclude);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static SObject[] searchComuni(String fieldLabel, String fieldValue, String[] otherFields, String targetObj, String value, String[] valuesToExclude) {
    try {
      return ExtSfdcLookup.searchComuni(fieldLabel, fieldValue, otherFields, targetObj, value, valuesToExclude);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static SObject[] searchFactoringUsers(String fieldLabel, String fieldValue, String[] otherFields, String targetObj, String value, String[] valuesToExclude) {
    try {
      return ExtSfdcLookup.searchFactoringUsers(fieldLabel, fieldValue, otherFields, targetObj, value, valuesToExclude);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Map<String, String> getPicklistMap(String fieldName, String objectName) {
    try {
      return ExtSfdcLookup.getPicklistMap(fieldName, objectName);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean addContactAllegato(String contact, String file) {
    try {
      return TaCtrlRegistrazioneUtenti.addContactAllegato(contact, file);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean aggiungiAttore(String idCessione, String idReferente, String tipo) {
    try {
      return TaCtrlRegistrazioneUtenti.aggiungiAttore(idCessione, idReferente, tipo);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean removeContact(String idContact) {
    try {
      return TaCtrlRegistrazioneUtenti.removeContact(idContact);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean rimuoviAttore(String idCessione, String idReferente, String tipo) {
    try {
      return TaCtrlRegistrazioneUtenti.rimuoviAttore(idCessione, idReferente, tipo);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static NDGLinea__c[] fetchAttoriCessione(String idCessione) {
    try {
      return TaCtrlRegistrazioneUtenti.fetchAttoriCessione(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean modifyContactAllegato(String contact, String file, String idCessione) {
    try {
      return TaCtrlRegistrazioneUtenti.modifyContactAllegato(contact, file, idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Cessione__c[] fetchCessioni() {
    try {
      return TaCtrlDettaglio.fetchCessioni();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Anomalia__c[] fetchAnomalie() {
    try {
      return TaCtrlDettaglio.fetchAnomalie();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean setReadedMessage(String idAnomalia) {
    try {
      return TaCtrlDettaglio.setReadedMessage(idAnomalia);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlDettaglio.DettaglioCessione fetchCessione(Id idCessione) {
    try {
      return TaCtrlDettaglio.fetchCessione(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static void popolaUltimoAccesso(Id idCessione) {
    try {
      TaCtrlDettaglio.popolaUltimoAccesso(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Contact[] fetchContacts() {
    try {
      return TaCtrlRegistrazioneUtenti.fetchContacts();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlLogin.ProfileInfo fetchProfile() {
    try {
      return TaCtrlLogin.fetchProfile();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean updateProfile(String input) {
    try {
      return TaCtrlLogin.updateProfile(input);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlDettaglio.DettaglioCessione upsertCessione(String cessione) {
    try {
      return TaCtrlDettaglio.upsertCessione(cessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static void documentiPrivacyCaricati(Id idCessione) {
    try {
      TaCtrlDocumentazione.documentiPrivacyCaricati(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlDocumentazione.InfoDocUpload getDocumentiUpload(Id idCessione) {
    try {
      return TaCtrlDocumentazione.getDocumentiUpload(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlDocumentazione.InfoDoc[] getDocumentiAnomali(Id idCessione) {
    try {
      return TaCtrlDocumentazione.getDocumentiAnomali(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean chiudiAnomalia(Id idAnomalia) {
    try {
      return TaCtrlDocumentazione.chiudiAnomalia(idAnomalia);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean deleteDocument(Id contentVersionId) {
    try {
      return TaCtrlDocumentazione.deleteDocument(contentVersionId);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static String getFoglioInformativoDownload() {
    try {
      return TaCtrlDocumentazione.getFoglioInformativoDownload();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlDocumentazione.InfoDoc[] getDocumentiDownload(Id cessioneId) {
    try {
      return TaCtrlDocumentazione.getDocumentiDownload(cessioneId);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlDocumentazione.InfoDoc[] getDocumentiDinamiciDownload(String [] codici) {
    try {
      return TaCtrlDocumentazione.getDocumentiDinamiciDownload(codici);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static String[] fetchStatiEsteri() {
    try {
      return ExtSfdcLookup.fetchStatiEsteri();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Comune__c[] fetchStatiEsteriFull() {
    try {
      return ExtSfdcLookup.fetchStatiEsteriFull();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Account[] taSearchAnagrafica(String piva, String platform) {
    try {
      CtrlWizardAccount.InputObj inputObj = new CtrlWizardAccount.InputObj(piva, null, null, 'piva', false);
      return CtrlWizardAccount.searchAnagrafica(inputObj, platform);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean salvaAllegatoCessione(String codiceDocumento, String famigliaDocumento, String idAttachment, String idCessione, String tipo) {
    try {
      return TaCtrlDocumentazione.salvaAllegatoCessione(codiceDocumento, famigliaDocumento, idAttachment, idCessione, tipo);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean salvaCertificazione(String idAttachment, String idCessione) {
    try {
      return TaCtrlDocumentazione.salvaCertificazione(idAttachment, idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlDettaglio.UploadCertificazioneInfo associaDebitoriCertificazioni(Certificazione__c[] certificazioni) {
    try {
      return TaCtrlDettaglio.associaDebitoriCertificazioni(certificazioni);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Fattura__c[] fetchFatture(String idCessione) {
    try {
      return TaCtrlDettaglio.fetchFatture(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean updateFatture(String input) {
    try {
      return TaCtrlDettaglio.updateFatture(input);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean deleteCertificazione(String idCertificazione) {
    try {
      return TaCtrlDettaglio.deleteCertificazione(idCertificazione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Certificazione__c[] fetchCertificazioniFatture(String idCessione) {
    try {
      return TaCtrlDettaglio.fetchCertificazioniFatture(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlDettaglio.UploadCertificazioneInfo fetchUploadCertificazioneInfo(String idCessione) {
    try {
      return TaCtrlDettaglio.fetchUploadCertificazioneInfo(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static String generaDocumentazioneTa(Id idOpportunity, String tipo) {
    try {
      return TaCtrlDocumentazione.generaDocumentazione(idOpportunity, tipo);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static String[] generaDocumentazioneStaticaTa() {
    try {
      return TaCtrlDocumentazione.generaDocumentazioneStatica();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TestiTa__c[] fetchFAQ() {
    try {
      return TaCtrlTesti.fetchFAQ();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static TaCtrlLogin.UserInfoObj getUserInfo() {
    try {
      return TaCtrlLogin.getUserInfo();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Ateco__c[] fetchAteco() {
    try {
      return CtrlWizardAccount.fetchAteco();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static NaturaGiuridicaSAE__mdt[] fetchRelazioneNgSae() {
    try {
      return CtrlWizardAccount.fetchRelazioneNgSae();
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean updatePrivacy(SObject obj, String oppCess) {
    try {
      return CtrlWizardOpportunity.updatePrivacy(obj, oppCess);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Opportunity fetchPrivacyCompilata(String idCessione) {
    try {
      return CtrlWizardOpportunity.fetchPrivacyCompilata(idCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Id fetchAttorePrivacy(String objType, Id oppId) {
    try {
      return CtrlWizardOpportunity.fetchAttorePrivacy(objType, oppId);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }

  @AuraEnabled
  public static Boolean editNomeCessione(Id idCessione, String nomeCessione) {
    try {
      return TaCtrlDettaglio.editNomeCessione(idCessione, nomeCessione);
    } catch (Exception e) {
      throw new AuraHandledException(Ex.getReadableMessage(e));
    }
  }
  //END COMMUNITY TI ANTICIPO


  @AuraEnabled
  public static String[] getSessionIdServerUrl() {
    String[] result = new String[]{};
    result.add((String) JSON.deserialize(Page.SessionId.getContent().toString(), String.class));
    String userType = UserInfo.getUserType();
    String serverUrl = (userType == 'Guest' || userType == 'CspLitePortal') ? URL.getSalesforceBaseUrl().toExternalForm() : 'https://' + [SELECT InstanceName FROM Organization LIMIT 1].InstanceName.toLowerCase() + '.salesforce.com';
    result.add(serverUrl);
    return result;
  }

  @RemoteAction
  @AuraEnabled
  public static Map<String, String> getLabels(String objName) {
    Map<String, String> ret = new Map<String, String>();
    Map<String, Schema.SObjectField> fields =  Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
    Schema.DescribeFieldResult tmp;
    for (String key : fields.keySet()) {
      tmp = fields.get(key).getDescribe();
      ret.put(tmp.getName(), tmp.getLabel());
    }
    return ret;
  }

  @AuraEnabled
  public static void updateBilancioSintetico(Id accountId) {
    System.debug('CtrlVueWrapper.updateBilancioSintetico.accountId: ' + JSON.serialize(accountId));
    WsRestRibesUtils.updateBilancioSintetico(accountId);
  }

  @AuraEnabled
  public static Account getInformazioniCambiaOwnerAccount(String accountId) { return TrgAccount.getInformazioniCambiaOwnerAccount(accountId); }

  @AuraEnabled
  public static TrgAccount.CambiaOwnerAccountResult cambiaOwnerAccount(String accountId, Boolean inviaMessaggioNotifica, String nuovoTitolareId, Boolean trasferisciOpportunitaAperte) {
    return TrgAccount.cambiaOwnerAccount(accountId, inviaMessaggioNotifica, nuovoTitolareId, trasferisciOpportunitaAperte);
  }

  @AuraEnabled
  public static Id getContentDocumentId(Id oppId, String tipo) {
    return CtrlGestioneAllegati.getContentDocumentId(oppId, tipo);
  }

  @AuraEnabled
  public static Boolean isPolandSearch() {
    return CtrlWizardAccount.isPolandSearch();
  }

}
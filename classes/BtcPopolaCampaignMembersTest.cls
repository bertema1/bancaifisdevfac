@isTest
private class BtcPopolaCampaignMembersTest {
  @testSetup
  static void setupData() {
    User owner = new User(Id = UserInfo.getUserId());

    Account a = TestUtils.creaAccount('acc1', owner);
    a.IdSiebelCampagna__c = 'provaCampagna';
    update a;

    Account a2 = TestUtils.creaAccount('acc2', owner);
    a2.IdSiebelCampagna__c = 'provaCampagna';
    update a2;

    Campaign c = TestUtils.creaCampagna('provaCampagna');
    c.IdSiebel__c = 'provaCampagna';
    update c;
  }

  @isTest
  static void testBtc() {
    Test.startTest();
    Database.executeBatch(new BtcPopolaCampaignMembers());
    Test.stopTest();

    CampaignMember[] cmL = [SELECT Id, ContactId, Contact.Account.Name FROM CampaignMember ORDER BY Contact.Account.Name];

    System.assertEquals(2, cmL.size());
    System.assertEquals('acc1', cmL[0].Contact.Account.Name);
    System.assertEquals('acc2', cmL[1].Contact.Account.Name);
  }
}
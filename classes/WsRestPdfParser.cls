public with sharing class WsRestPdfParser {

  public static ParseCertificazioneResponse parseCertificazione(Id contentVersionId) {
    if (String.isBlank(contentVersionId)) {
      throw new Ex.WsException('parseCertificazione input vuoto');
    }

    ParseCertificazioneInput input = new ParseCertificazioneInput(contentVersionId);

    String responseString = WsRestUtils.callSimpleService('UrlCertificazioneParser__c', input);
    return (ParseCertificazioneResponse)JSON.deserialize(responseString, ParseCertificazioneResponse.class);
  }

  public class ParseCertificazioneInput {
    public Id sfdcId;

    public ParseCertificazioneInput(Id sfdcId) {
      this.sfdcId = sfdcId;
    }
  }

  public class ParseCertificazioneResponse {
    private Payload payload;
    private String message;

    public Boolean isCorrect() {
      return message == 'OK. ';
    }

    public Payload getResult() {
      return payload;
    }
  }

  public class Payload {
    private String dataPagamento;
    private String naturaCorrenteImportoCredito;
    private String contoCapitaleImportoCredito;
    private String ammontareComplessivoCredito;
    public String codiceControllo;
    public String dataRicezione;
    public String debitore;
    public String numeroCertificazione;
    public String numIstanzaCertificazione;
    public String pivaOrCf;
    public Fattura[] fatture;

    public Date getDataPagamento() {
      return Date.parse(dataPagamento);
    }

    public Double getImportoCertificato() {
      return parseValuta(naturaCorrenteImportoCredito) + parseValuta(contoCapitaleImportoCredito);
    }

    public Double getAmmontareComplessivoCredito() {
      return parseValuta(ammontareComplessivoCredito);
    }
  }

  public class Fattura {
    private String data;
    private String importo;
    public String numero_fattura;
    private String riconosciuto;

    public Double getImporto() {
      return parseValuta(importo);
    }

    public Double getImportoRiconosciuto() {
      return parseValuta(riconosciuto);
    }

    public Date getData() {
      return Date.parse(data);
    }
  }

  //utils
  private static Double parseValuta(String valuta) {
    if (String.isBlank(valuta)) return null;
    return Double.valueOf(valuta.replace('€', '').replace('.', '').replace(',', '.'));
  }
}
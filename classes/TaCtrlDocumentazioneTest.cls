@isTest
private class TaCtrlDocumentazioneTest {

  private static Integer nCessioni = 10;

  @testSetup
  static void setupData() {
    Account a = TestUtils.creaAccount('AccountTest');
    insert new Prodotto__c(CodiceUnivoco__c = 'ATDTiAnticipo');
    Cessione__c[] cessioni = new Cessione__c[] {};
    for (Integer i = 0; i < nCessioni; i++) {
      cessioni.add(new Cessione__c(Name = 'test', Account__c = a.Id));
    }
    insert cessioni;
  }

  @isTest
  static void testGetDocumentiUpload() {
    Cessione__c c = [SELECT Stato__c FROM Cessione__c LIMIT 1];
    TaCtrlDocumentazione.getDocumentiUpload(c.Id);
  }

  @isTest
  static void testAllegatoCertificazione() {
    // TODO TEST
    try {
      TaCtrlDocumentazione.salvaAllegatoCertificazione(null, null, null, null, null);
    } catch (Exception e) {
    }
  }

  @isTest
  static void testSalvaAllegatoCessione() {
    Cessione__c c = [SELECT Stato__c FROM Cessione__c LIMIT 1];
    ContentVersion cv = new ContentVersion(Title = 'test', ContentURL = 'http://www.google.com/');
    insert cv;
    TaCtrlDocumentazione.salvaAllegatoCessione(
      'test',
      'Bilanci',
      cv.Id,
      c.Id,
      'test'
    );

    cv = new ContentVersion(Title = 'test', ContentURL = 'http://www.google.com/');
    insert cv;
    TaCtrlDocumentazione.salvaAllegatoCessione(
      'test',
      'Bilanci',
      cv.Id,
      c.Id,
      'test'
    );
  }
}
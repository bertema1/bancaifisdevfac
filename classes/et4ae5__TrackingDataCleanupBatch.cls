/*
Questo file è stato generato e non è il codice sorgente effettivo di questa
classe globale gestita.
Questo file di sola lettura mostra i costruttori, i metodi,
le variabili e le proprietà globali della classe.
Per abilitare il codice per la compilazione, tutti i metodi restituiscono null.
*/
global class TrackingDataCleanupBatch implements Database.Batchable<SObject>, Database.Stateful {
    global void execute(Database.BatchableContext bc, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}

/**
* Progetto:         Banca IFIS
* Descrizione:      Trigger su oggetto Evento
* Sviluppata il:    31/10/2016
* Developer:        Zerbinati Francesco
*/

trigger Event on Event (after insert, after update, before delete, before insert, before update) {
	  Funzionalita__c f = Funzionalita__c.getInstance();
    if (f.DisabilitaTriggers__c) return;
    T tu = T.getInstance();

    if(f.DisabilitaTriggerActivities__c) {
      if(T.isBeforeInsert() || T.isBeforeUpdate()) {
        TrgActivity.chiudiActivityEsitata(tu);
      }
      return;
    }

    if(T.isBeforeInsert()) {
      TrgActivity.chiudiActivityEsitata(tu);
      TrgActivity.bloccaVisiteNonPossibili(tu, f.DisabilitaCheckTitolareEventi__c);
      TrgActivity.bloccaAzioniActivityNonOwner(tu);
      TrgActivity.azioneEsitazioneActivity(tu);
      TrgActivity.rinominaEvento(tu);
    }

    if(T.isBeforeUpdate()) {
      TrgActivity.chiudiActivityEsitata(tu);
      TrgActivity.bloccaAzioniActivityNonOwner(tu);
    }

    if(T.isAfterInsert()) {
      TrgActivity.popolaDataUltimoContatto(tu);
    }

		if(T.isAfterUpdate()) {
			TrgActivity.azioneEsitazioneActivity(tu);
      TrgActivity.popolaDataUltimoContatto(tu);
		}

    if(T.isBeforeDelete()) {
      TrgActivity.controlloCancellazioneActivity(tu);
      TrgActivity.bloccaAzioniActivityNonOwner(tu);
    }

}
/**
* Progetto:         Banca IFIS
* Descrizione:      Trigger su oggetto Task
* Sviluppata il:    31/10/2016
* Ampiamente modificata il: 31/01/2017
* Developer:        Zerbinati Francesco
*/

trigger Task on Task (after insert, after update, before insert, before update, before delete) {
    Funzionalita__c f = Funzionalita__c.getInstance();
    if (f.DisabilitaTriggers__c) return;
    T tu = T.getInstance();

    if(f.DisabilitaTriggerActivities__c) {
      if(T.isBeforeInsert() || T.isBeforeUpdate()) {
        // chiude subito il task se è esitato
        TrgActivity.chiudiActivityEsitata(tu);
      }
      return;
    }

    if(T.isBeforeInsert()) {
      // chiude subito il task se è esitato
      TrgActivity.chiudiActivityEsitata(tu);
      // chiude subito tutte le segnalazioni
      TrgActivity.chiudiSegnalazione(tu);
      // un utente non può inserire attività per un account non suo
      TrgActivity.bloccaAzioniActivityNonOwner(tu);
      // rinomina i task di tipo segnalazione secondo una naming convention
      TrgActivity.rinominaTask(tu);
    }

    if(T.isBeforeUpdate()) {
      // chiude il task se esitato
      TrgActivity.chiudiActivityEsitata(tu);
      // gestisce l'ownerId dei task per gli utenti di filo diretto
      TrgActivity.gestisciOwnershipTaskFiloDiretto(tu);
      // un utente non può inserire attività per un account non suo
      TrgActivity.bloccaAzioniActivityNonOwner(tu);
      // rinomina i task di tipo segnalazione secondo una naming convention
      TrgActivity.rinominaTask(tu);
    }

    if(T.isAfterUpdate()) {
      // esegue un azione specificata nella matrice esiti in base ai livelli esito
      TrgActivity.azioneEsitazioneActivity(tu);
      // crea un task di ricontatto se cè una data di ricontatto (DataRicontatto__c)
      TrgActivity.creaTaskRicontatto(tu);
      // viene sistemato OperatoreFiloDiretto_c se il task viene esitato come "Gia cliente" o "gia contattato"
      TrgActivity.gestisciOwnershipAccountFiloDiretto(tu);

      TrgActivity.popolaDataUltimoContatto(tu);
    }

    if(T.isAfterInsert()) {
      // esegue un azione specificata nella matrice esiti in base ai livelli esito
      TrgActivity.azioneEsitazioneActivity(tu);
      // crea un task di ricontatto se cè una data di ricontatto (DataRicontatto__c)
      TrgActivity.creaTaskRicontatto(tu);

      // gestisce il task con record type "Segnalazione" (presentatore e assegnazione di un CT a Filo Diretto)
      TrgActivity.gestisciSegnalazione(tu);
      // viene sistemato OperatoreFiloDiretto_c se il task viene esitato come "Gia cliente" o "gia contattato"
      TrgActivity.gestisciOwnershipAccountFiloDiretto(tu);

      TrgActivity.popolaDataUltimoContatto(tu);
    }

    if(T.isBeforeDelete()) {
      // impedisce di cancellare un task se chiuso oppure l'attività non è dell'utente
      TrgActivity.controlloCancellazioneActivity(tu);
      TrgActivity.bloccaAzioniActivityNonOwner(tu);
    }

}
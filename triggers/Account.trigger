/**
* Progetto:         Banca IFIS
* Descrizione:      Trigger su oggetto Account
* Sviluppata il:    14/09/2016
* Developer:        Zerbinati Francesco, Michele Triaca
*/

trigger Account on Account (before insert, before update, after update, after insert) {
  Funzionalita__c f = Funzionalita__c.getInstance();
  if (f.DisabilitaTriggers__c) return;
  T tu = T.getInstance();

  if (T.isBeforeInsert()) {
    // all'inserimento popola settore dato codice ateco
    TrgAccount.popolaSettoreDaATECO(tu);
    // gestione Mutuo MCC
    TrgAccount.trgAssociazioneInBaseCAP(tu);
    // ottiene il cab del comune a partire dal nome
    TrgAccount.getCabComune(tu);
    // setto presentatore e fd
    TrgAccount.settaProprieta(tu);
    // inizializzazione e fix di alcuni dati
    TrgAccount.inizializzaDati(tu);
    // ricalcolo indirizzo completo
    TrgAccount.ricalcolaIndirizzoCompleto(tu);
    // un utente leasing superuser non può essere owner di un account
    TrgAccount.gestisciOwner(tu);
  }

  if (T.isBeforeUpdate()) {
    // aggiorna il settore dato un nuovo codice ateco
    TrgAccount.popolaSettoreDaATECO(tu);
    // controlla se si ha una richiesta di assegnazione (tremite campo StatoAssegnazione__c == 'Richiesta')
    TrgAccount.preparaProcessoAssegnazione(tu);
    // resetta lo stato di assegnazione se è approvata/rifiutata
    TrgAccount.resettaStatoAssegnazione(tu);
    // stabilisce priorità dalla matrice
    TrgAccount.stabilisciPriorita(tu);
    // ottiene il cab del comune a partire dal nome
    TrgAccount.getCabComune(tu);
    // setto presentatore e fd
    TrgAccount.settaProprieta(tu);
    // inizializzazione e fix di alcuni dati
    TrgAccount.inizializzaDati(tu);
    // ricalcolo indirizzo completo
    TrgAccount.ricalcolaIndirizzoCompleto(tu);
    // un utente leasing superuser non può essere owner di un account
    TrgAccount.gestisciOwner(tu);
  }

  if (T.isAfterUpdate()) {
    // se è cambiato il nome, aggiorna il nome del referente campagna
    TrgAccount.allineaReferenteCampagna(tu);
    TrgAccount.cambiaOwnerAttivita(tu);
    // cambia filiale
    TrgAccount.cambiaFiliale(tu);
    //Se è stato richiesto un cambio di assegnatario, questo metodo fa partire il processo approvativo
    TrgAccount.avviaProcessoAssegnazione(tu);

    TrgAccount.gestisciAccountTeam(tu);
  }

  if (T.isAfterInsert()) {
    //Creo un contact "referente campagna" associato all'account
    TrgAccount.creaReferenteStandard(tu);
    // creo un contatto telefonico per l'owner dell'account (solo se è un commerciale o un filo diretto)
    if (!f.DisabilitaCreazioneCTDopoInsertAcc__c && !TrgAccount.disabilitaCreazioneCTDopoInsertAccount) TrgAccount.creaContattoTelefonico(tu);
    //Quando si crea un nuovo prospect o cambia il titolare si deve aggiornare il campo filiale sull'account
    //con la filiale dello sviluppatore
    TrgAccount.cambiaFiliale(tu);

    TrgAccount.gestisciAccountTeam(tu);
  }
}
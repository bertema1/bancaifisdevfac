trigger Anomalia on Anomalia__c (after insert) {
  if (Funzionalita__c.getInstance().DisabilitaTriggers__c) return;
  T tu = T.getInstance();

  if (T.isAfterInsert()) {
    TrgAnomalia.inviaMail(tu);
  }

}
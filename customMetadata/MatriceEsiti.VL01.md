<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>VL01</label>
    <protected>false</protected>
    <values>
        <field>Azione__c</field>
        <value xsi:type="xsd:string">FINE</value>
    </values>
    <values>
        <field>EsitiLivello1__c</field>
        <value xsi:type="xsd:string">Lista cedenti ricevuta</value>
    </values>
    <values>
        <field>EsitiLivello2__c</field>
        <value xsi:type="xsd:string">Inserita lista cedenti</value>
    </values>
    <values>
        <field>EsitiLivello3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>EsitiLivello4__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>EsitiLivello5__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">FactoringIndirettoVisitaPerLista</value>
    </values>
</CustomMetadata>

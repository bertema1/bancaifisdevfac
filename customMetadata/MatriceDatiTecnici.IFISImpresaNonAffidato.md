<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>IFIS Impresa non affidato</label>
    <protected>false</protected>
    <values>
        <field>CATEV__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CATFA__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CAT__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Codice__c</field>
        <value xsi:type="xsd:string">222</value>
    </values>
    <values>
        <field>DILAZ__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FORMA__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FTEC__c</field>
        <value xsi:type="xsd:string">02401</value>
    </values>
    <values>
        <field>LINEA__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SOTCOD__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SOTEV__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SOTFA__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Acquisto a titolo definitivo</label>
    <protected>false</protected>
    <values>
        <field>Acquisto__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Icona__c</field>
        <value xsi:type="xsd:string">custom-custom41</value>
    </values>
    <values>
        <field>Ordinamento__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CO267</label>
    <protected>false</protected>
    <values>
        <field>AtecoRichiesto__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>RaeRichiesto__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SAE__c</field>
        <value xsi:type="xsd:string">476</value>
    </values>
    <values>
        <field>SedeLegale__c</field>
        <value xsi:type="xsd:string">IT</value>
    </values>
</CustomMetadata>

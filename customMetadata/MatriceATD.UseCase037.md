<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>UseCase037</label>
    <protected>false</protected>
    <values>
        <field>Alert__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CaricoMassivo__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>CessioneContinuativa__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>ConAnticipazione__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>DebitoreConProroga__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>FidoDiCoppiaATD__c</field>
        <value xsi:type="xsd:string">ICAR automatici</value>
    </values>
    <values>
        <field>ICARAutomatici__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>IFISImpresa__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>LineaDiAcquisto__c</field>
        <value xsi:type="xsd:string">751</value>
    </values>
    <values>
        <field>LineaDiCarico__c</field>
        <value xsi:type="xsd:string">561</value>
    </values>
    <values>
        <field>LineaSingola__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>OperazioniRotative__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>PerfezionamentoAcquisto__c</field>
        <value xsi:type="xsd:string">Riconoscimento/Certificazione</value>
    </values>
    <values>
        <field>PossibilitaNotifica__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>ProsolvendoProsoluto__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>TipologiaConto__c</field>
        <value xsi:type="xsd:string">Conto evidenza (se nuovo servizio su rapporto esistente, IFIS Impresa / Conto evidenza esistente)</value>
    </values>
</CustomMetadata>

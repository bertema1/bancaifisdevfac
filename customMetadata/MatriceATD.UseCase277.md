<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>UseCase277</label>
    <protected>false</protected>
    <values>
        <field>Alert__c</field>
        <value xsi:type="xsd:string">NC</value>
    </values>
    <values>
        <field>CaricoMassivo__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>CessioneContinuativa__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>ConAnticipazione__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>DebitoreConProroga__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>FidoDiCoppiaATD__c</field>
        <value xsi:type="xsd:string">ICAR automatici</value>
    </values>
    <values>
        <field>ICARAutomatici__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>IFISImpresa__c</field>
        <value xsi:type="xsd:string">SI</value>
    </values>
    <values>
        <field>LineaDiAcquisto__c</field>
        <value xsi:type="xsd:string">841</value>
    </values>
    <values>
        <field>LineaDiCarico__c</field>
        <value xsi:type="xsd:string">631</value>
    </values>
    <values>
        <field>LineaSingola__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>OperazioniRotative__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>PerfezionamentoAcquisto__c</field>
        <value xsi:type="xsd:string">Su richiesta</value>
    </values>
    <values>
        <field>PossibilitaNotifica__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>ProsolvendoProsoluto__c</field>
        <value xsi:type="xsd:string">NO</value>
    </values>
    <values>
        <field>TipologiaConto__c</field>
        <value xsi:type="xsd:string">IFIS Impresa</value>
    </values>
</CustomMetadata>

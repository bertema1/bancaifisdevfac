<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>STD05</label>
    <protected>false</protected>
    <values>
        <field>NomeSezione__c</field>
        <value xsi:type="xsd:string">Dettagli Operazione</value>
    </values>
    <values>
        <field>Ordine__c</field>
        <value xsi:type="xsd:double">18.0</value>
    </values>
</CustomMetadata>

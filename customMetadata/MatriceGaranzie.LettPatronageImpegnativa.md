<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lett. di patronage impegnativa</label>
    <protected>false</protected>
    <values>
        <field>CodiceCedacri__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>CodiceKNET__c</field>
        <value xsi:type="xsd:string">LPI</value>
    </values>
    <values>
        <field>Omnibus__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Specifica__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Tipologia__c</field>
        <value xsi:type="xsd:string">Personale</value>
    </values>
</CustomMetadata>

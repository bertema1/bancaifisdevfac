<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Tecniche di Comunicazione</label>
    <protected>false</protected>
    <values>
        <field>DescrizioneDownload__c</field>
        <value xsi:type="xsd:string">Scarica e Firma il modulo Tecniche di comunicazione</value>
    </values>
    <values>
        <field>DescrizioneUpload__c</field>
        <value xsi:type="xsd:string">Carica il modulo Tecniche di Comunicazione firmato</value>
    </values>
    <values>
        <field>Download__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>FamigliaDocumento__c</field>
        <value xsi:type="xsd:string">Identificazione cliente</value>
    </values>
    <values>
        <field>Help__c</field>
        <value xsi:type="xsd:string">Funzionale alla corretta determinazione delle modalità con cui poterti ricontattare e inviare i documenti contrattuali</value>
    </values>
    <values>
        <field>Ordine__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Tipo__c</field>
        <value xsi:type="xsd:string">ModuloTecnicheDiComunicazioneFirm</value>
    </values>
    <values>
        <field>Upload__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Visibile__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>

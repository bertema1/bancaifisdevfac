<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Spesa4</label>
    <protected>false</protected>
    <values>
        <field>EstremoInferiore__c</field>
        <value xsi:type="xsd:double">50000.0</value>
    </values>
    <values>
        <field>EstremoSuperiore__c</field>
        <value xsi:type="xsd:double">75000.0</value>
    </values>
    <values>
        <field>SpeseCedente__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>SpeseDebitore__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
</CustomMetadata>

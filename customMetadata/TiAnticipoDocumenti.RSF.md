<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Modulo di Richiesta Servizi Finanziari</label>
    <protected>false</protected>
    <values>
        <field>DescrizioneDownload__c</field>
        <value xsi:type="xsd:string">Scarica e Firma il modulo di Richiesta Servizi Finanziari (RSF)</value>
    </values>
    <values>
        <field>DescrizioneUpload__c</field>
        <value xsi:type="xsd:string">Carica il Modulo Richiesta Servizi Finanziari firmato</value>
    </values>
    <values>
        <field>Download__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>FamigliaDocumento__c</field>
        <value xsi:type="xsd:string">Identificazione cliente</value>
    </values>
    <values>
        <field>Help__c</field>
        <value xsi:type="xsd:string">Necessario per permetterci di recuperare le informazioni utili alla valutazione dell’operazione di finanziamento</value>
    </values>
    <values>
        <field>Ordine__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Tipo__c</field>
        <value xsi:type="xsd:string">RSFFirm</value>
    </values>
    <values>
        <field>Upload__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Visibile__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>

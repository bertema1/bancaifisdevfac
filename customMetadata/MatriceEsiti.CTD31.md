<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CTD31</label>
    <protected>false</protected>
    <values>
        <field>Azione__c</field>
        <value xsi:type="xsd:string">Contatto telefonico</value>
    </values>
    <values>
        <field>EsitiLivello1__c</field>
        <value xsi:type="xsd:string">Azienda in target</value>
    </values>
    <values>
        <field>EsitiLivello2__c</field>
        <value xsi:type="xsd:string">Esigenza fuori target</value>
    </values>
    <values>
        <field>EsitiLivello3__c</field>
        <value xsi:type="xsd:string">Tipologia credito</value>
    </values>
    <values>
        <field>EsitiLivello4__c</field>
        <value xsi:type="xsd:string">Crediti non continuativi / non rotativi</value>
    </values>
    <values>
        <field>EsitiLivello5__c</field>
        <value xsi:type="xsd:string">Da contattare</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">ContattoTelefonicoDiretto</value>
    </values>
</CustomMetadata>

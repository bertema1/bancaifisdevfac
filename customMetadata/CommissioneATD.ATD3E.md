<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ATD3E</label>
    <protected>false</protected>
    <values>
        <field>CommissioneATDMese__c</field>
        <value xsi:type="xsd:double">0.379</value>
    </values>
    <values>
        <field>EstremoInferiore__c</field>
        <value xsi:type="xsd:double">75000.0</value>
    </values>
    <values>
        <field>EstremoSuperiore__c</field>
        <value xsi:type="xsd:double">100000.0</value>
    </values>
    <values>
        <field>SegmentoRischio__c</field>
        <value xsi:type="xsd:string">3</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>VC01</label>
    <protected>false</protected>
    <values>
        <field>Azione__c</field>
        <value xsi:type="xsd:string">WIP</value>
    </values>
    <values>
        <field>EsitiLivello1__c</field>
        <value xsi:type="xsd:string">Azienda in target</value>
    </values>
    <values>
        <field>EsitiLivello2__c</field>
        <value xsi:type="xsd:string">Esigenza fuori target</value>
    </values>
    <values>
        <field>EsitiLivello3__c</field>
        <value xsi:type="xsd:string">Prodotti richiesti</value>
    </values>
    <values>
        <field>EsitiLivello4__c</field>
        <value xsi:type="xsd:string">Finan. A Fondo Perduto / Agevolato</value>
    </values>
    <values>
        <field>EsitiLivello5__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">VisitaCommerciale</value>
    </values>
</CustomMetadata>

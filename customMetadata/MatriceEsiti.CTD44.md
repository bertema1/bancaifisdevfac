<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CTD44</label>
    <protected>false</protected>
    <values>
        <field>Azione__c</field>
        <value xsi:type="xsd:string">Visita commerciale</value>
    </values>
    <values>
        <field>EsitiLivello1__c</field>
        <value xsi:type="xsd:string">Azienda in target</value>
    </values>
    <values>
        <field>EsitiLivello2__c</field>
        <value xsi:type="xsd:string">Azienda interessata fissato appuntamento</value>
    </values>
    <values>
        <field>EsitiLivello3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>EsitiLivello4__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>EsitiLivello5__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">ContattoTelefonicoDiretto</value>
    </values>
</CustomMetadata>

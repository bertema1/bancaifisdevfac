<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contratto Mutuo</label>
    <protected>false</protected>
    <values>
        <field>CodiceDoc__c</field>
        <value xsi:type="xsd:string">EX2000018</value>
    </values>
    <values>
        <field>CodiceSottoclasse__c</field>
        <value xsi:type="xsd:string">EX0020002</value>
    </values>
    <values>
        <field>RecuperareDaFilenet__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contratto d&apos;appalto</label>
    <protected>false</protected>
    <values>
        <field>DescrizioneDownload__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DescrizioneUpload__c</field>
        <value xsi:type="xsd:string">Contratto d&apos;appalto</value>
    </values>
    <values>
        <field>Download__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>FamigliaDocumento__c</field>
        <value xsi:type="xsd:string">Documenti credito</value>
    </values>
    <values>
        <field>Help__c</field>
        <value xsi:type="xsd:string">Documento Contrattuale che ha regolato lo svolgimento dei servizi erogati da parte dell’appaltatore nei confronti del committente/appaltante pubblico relativo alla certificazione caricata</value>
    </values>
    <values>
        <field>Ordine__c</field>
        <value xsi:type="xsd:double">8.0</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Tipo__c</field>
        <value xsi:type="xsd:string">Contratto2</value>
    </values>
    <values>
        <field>Upload__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Visibile__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Bergamo</label>
    <protected>false</protected>
    <values>
        <field>EmailGestore__c</field>
        <value xsi:type="xsd:string">luisa.restelli@bancaifis.it</value>
    </values>
    <values>
        <field>Email__c</field>
        <value xsi:type="xsd:string">bergamo@bancaifis.it</value>
    </values>
</CustomMetadata>

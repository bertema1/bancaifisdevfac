<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Documento poteri di firma</label>
    <protected>false</protected>
    <values>
        <field>DescrizioneDownload__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DescrizioneUpload__c</field>
        <value xsi:type="xsd:string">Carica documento poteri di firma</value>
    </values>
    <values>
        <field>Download__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>FamigliaDocumento__c</field>
        <value xsi:type="xsd:string">Documentazione societaria</value>
    </values>
    <values>
        <field>Help__c</field>
        <value xsi:type="xsd:string">Vi preghiamo di inoltrarci la documentazione societaria necessaria ad attestare che il soggetto che firmerà il contratto di factoring possiede i  relativi poteri di straordinaria amministrazione (es. procura autenticata e/o verbale assemblea soci e/o verbale consiglio di amministrazione), fermo restando che la banca si riserva fin d’ora la facoltà di richiederVi ogni altro documento ritenuto necessario al fine di permettere alla Banca di effettuare le verifiche necessarie ad attestare la sussistenza dei suddetti poteri.</value>
    </values>
    <values>
        <field>Ordine__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Tipo__c</field>
        <value xsi:type="xsd:string">Atto costituitivo</value>
    </values>
    <values>
        <field>Upload__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Visibile__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>

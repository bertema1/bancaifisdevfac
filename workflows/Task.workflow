<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TaskChiuso</fullName>
        <field>Status</field>
        <literalValue>Chiuso</literalValue>
        <name>Task chiuso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Esito Chiude Task</fullName>
        <actions>
            <name>TaskChiuso</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.EsitoLivello1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Quandi si esita un contatto telefonico, il task viene chiuso</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

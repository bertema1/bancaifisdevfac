<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SettaStatoRichiamata</fullName>
        <field>StatoAssegnazione__c</field>
        <literalValue>Richiamata</literalValue>
        <name>Setta stato richiamata</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Setta_stato_approvata</fullName>
        <field>StatoAssegnazione__c</field>
        <literalValue>Approvata</literalValue>
        <name>Setta stato approvata</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Setta_stato_rifiutata</fullName>
        <field>StatoAssegnazione__c</field>
        <literalValue>Rifiutata</literalValue>
        <name>Setta stato rifiutata</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PopolaQualifica</fullName>
        <description>Popola il campo Qualifica se lo Stato Fast Finance è &apos;Professionista&apos; o &apos;Giudice Delelgato&apos;</description>
        <field>Title</field>
        <formula>IF( ISPICKVAL(StatoFastFinance__c, &quot;N/A&quot;) , &quot;&quot;, TEXT(StatoFastFinance__c))</formula>
        <name>Popola Qualifica</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Popola Qualifica Fast Finance</fullName>
        <actions>
            <name>PopolaQualifica</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Popola il campo &apos;Qualifica&apos; se lo Stato Fast Finance è &apos;Professionista&apos; o &apos;Giudice Delegato&apos;</description>
        <formula>ISCHANGED(StatoFastFinance__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FastFinanceNotificaGiuliaSchieri_perOpportunitInValutazione</fullName>
        <description>Fast Finance - Notifica Giulia Schieri per Opportunità in Valutazione</description>
        <protected>false</protected>
        <recipients>
            <recipient>giulia.schieri@bancaifis.it</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FastFinanceOpportunitaInValutazione</template>
    </alerts>
    <fieldUpdates>
        <fullName>AggiornamentoDataDelibera</fullName>
        <field>DataDelibera__c</field>
        <formula>TODAY()</formula>
        <name>Aggiornamento data Delibera</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AggiornamentoFaseDiCaduta</fullName>
        <field>FaseDiCaduta__c</field>
        <formula>TEXT(PRIORVALUE(StageName))</formula>
        <name>Aggiornamento fase di caduta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DataDiPassaggioInValutazione</fullName>
        <field>DataPassaggioInValutazione__c</field>
        <formula>TODAY()</formula>
        <name>Data di passaggio &apos;In Valutazione&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DatadipassaggioInLavorazione</fullName>
        <field>DataPassaggioinLavorazione__c</field>
        <formula>TODAY()</formula>
        <name>Data di passaggio &apos;In Lavorazione&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Data di passaggio %27In Lavorazione%27</fullName>
        <actions>
            <name>DatadipassaggioInLavorazione</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>In Lavorazione</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Data di passaggio %27In Redazione Contratti%27</fullName>
        <actions>
            <name>AggiornamentoDataDelibera</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Redazione Contratti</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Data di passaggio %27In Valutazione%27</fullName>
        <actions>
            <name>DataDiPassaggioInValutazione</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>In Valutazione</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Popola FaseDiCaduta</fullName>
        <actions>
            <name>AggiornamentoFaseDiCaduta</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Persa</value>
        </criteriaItems>
        <description>Quando la fase dell&apos;Opportunità diventa &quot;Persa&quot;, popola il campo &quot;FaseDiCaduta&quot; con la fase in cui si trovava l&apos;Opportunità.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

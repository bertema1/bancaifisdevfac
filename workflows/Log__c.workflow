<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Invia_mail_in_caso_di_errore</fullName>
        <description>Invia mail in caso di errore</description>
        <protected>false</protected>
        <recipients>
            <recipient>ifis@telnext.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ErroreLog</template>
    </alerts>
    <rules>
        <fullName>Invia mail in caso di errore</fullName>
        <actions>
            <name>Invia_mail_in_caso_di_errore</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Log__c.ErorrMessage__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MailAllegati</fullName>
        <description>Mail Allegati</description>
        <protected>false</protected>
        <recipients>
            <field>To__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/InvioAllegati</template>
    </alerts>
    <alerts>
        <fullName>MailNPL</fullName>
        <description>Mail NPL</description>
        <protected>false</protected>
        <recipients>
            <field>Cc__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>To__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SegnalazioneNPL</template>
    </alerts>
    <alerts>
        <fullName>MailRendimax</fullName>
        <description>Mail Rendimax</description>
        <protected>false</protected>
        <recipients>
            <field>Cc__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>To__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SegnalazioneRendimax</template>
    </alerts>
    <rules>
        <fullName>Invia Mail Allegati</fullName>
        <actions>
            <name>MailAllegati</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Email__c.Template__c</field>
            <operation>equals</operation>
            <value>InvioAllegati</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Invia Mail NPL</fullName>
        <actions>
            <name>MailNPL</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Email__c.Template__c</field>
            <operation>equals</operation>
            <value>SegnalazioneNPL</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Invia Mail Rendimax</fullName>
        <actions>
            <name>MailRendimax</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Email__c.Template__c</field>
            <operation>equals</operation>
            <value>SegnalazioneRendimax</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
